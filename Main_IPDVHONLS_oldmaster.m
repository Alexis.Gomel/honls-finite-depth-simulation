%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% Variable depth NLS extended to 4th order
% See Djordjevic & Redekopp ZAMP 1978
% For the HO coefficients, see Sedletsky JETP 2003 (and same author works up to today)
%
% i B_X = -alpha U_TT + beta |U|^2U - i mu U + i (-alpha_3 U_TTT + beta_21 |U|^2
% U_T + beta_22 U^2 U^*_T) + alpha4 U_TTTT
% all coefficients depend on X in a cumbersome way, see References above 
% (Sedletsky writes the time evolution, here we employ the space evolution, coefficients change: at 4th order they involve 3rd order)
% The small parameter epsilon does NOT appear explicitely
%
% The reasonable normalization is to put g = 1, so that omega (FIXED!!!) is in unit of
% [m^-1/2] and set kh as a function of X (this IS the important parameter to discern focusing/defocusing regimes).
% Now omega^2 = k tanh kh = k sigma. We can derive h or k explicitely and
% find the dependence of cg, alpha, beta, mu. 
% omega = 1 could as well be chosen. Any different omega = pippo corresponds to a
% rescaling of time (multiply by pippo^-1) and lengths (divide by pippo^2)
%
% The solution is made by means of integrating factor or interaction
% picture. 
%
% The place of the shoaling term (mu U) is customary: the linear step is a better choice, since its integration is simpler (it is a log derivative!!!) 
% Only the 3O shoaling is included for now. The 4O is more cumbersome, but
% can be derived by simple energy arguments. 
%
% Andrea ARMAROLI, GAP University of Geneva, 26.02.2019
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all;
clear all;
%%
addpath(genpath(fullfile('functions','writers')));
addpath(genpath(fullfile('functions','Depth_parameters')));
addpath(genpath(fullfile('functions','import')));
addpath(genpath(fullfile('functions','plot')));
addpath(genpath(fullfile('functions','colormaps')));
addpath(genpath(fullfile('functions','khfunctions')));
addpath(genpath(fullfile('functions','Dialogs')));
addpath(genpath(fullfile('functions','sidebands')));
addpath(genpath(fullfile('functions','RK')));
addpath(genpath(fullfile('functions','jonswap')));
%%
g0 = 9.81;
crit= 1.363;
Normalized=1; %If =1 this makes the plots Normalized to B0. 
[s_color,e_color]=color_language_beforeafter();
[color_s,color_Ns,color_utr,color_main,color_sideband,color_2sideband]=color_language_3wave();
[dispersion_color,nonlinear_color]=color_language_NLScoefs();
Fsz=15;
	
%Equation coefficients ON/Off. 1 is ON, 2 is OFF
NLS_coef=onoff_dialog();
ON_OFF_coef=onoff_coef(NLS_coef);

DimensionOptions= {'Dimensional','Adimentional'}; 
defSelection = DimensionOptions{2};
Dimensionflag = bttnChoiseDialog(DimensionOptions, 'Input Style', defSelection,...
 'Input Style'); 
fprintf( 'User selection "%s"\n',DimensionOptions{Dimensionflag});

% selecting a function for kh 
switch Dimensionflag
     case 1 
        prompt = {'f [1/s]','L_\xi [m]','L_\tau (time window, periods/soliton length) [ad]',...
            'n_t (log_2)','Number of steps to store N_x','\epsilon (steepness = k_0 a_0) [ad]',...
            'Shoaling ON/OFF','FOD ON/OFF','Export initial function','W-M Factor','Disipation','Output time [s]'};
        dlg_title = 'Parameters'; options.Interpreter = 'tex'; num_lines = 1;
		if ~exist('button_dialog_temp_1.txt', 'dir') %check if dir already exists
			defaultans=importfile_buttondialog_case1('button_dialog_temp_1.txt');
			defaultans=cellstr(defaultans);
			params = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
		else
			defaultans = {'1.01','10','120','12','100','0.06','1','1','no','1.83','0.00','100'};
			params = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
		end

		% normalized frequency omega = omega[s^-1]/g^{1/2}
        freq_dim = str2double(params(1));
        Omega = freq_dim*(2*pi)/sqrt(g0);
        T0_dim=1/freq_dim;
        fprintf('Dimentional frequency: %.3f \n , Adimentional frequency: %.3f \n ', freq_dim, Omega)
        Lx = str2double(params(2));
        Nper = str2double(params(3));
        nt = 2.^str2double(params(4));
        % number of points to save
        Nx = str2double(params(5));
        esteep0 = str2double(params(6));
        shoalingflag = str2double(params(7));
        FODflag = str2double(params(8)); 
        Exportinit =  string(params(9));
        Factor=str2double(params(10));
        dis=str2double(params(11));
        Out_time=str2double(params(12));
		%update dialog options
		print_button_dialog_parameters_case1(freq_dim,Lx,Nper,str2double(params(4)),Nx,esteep0,shoalingflag,FODflag,Exportinit,Factor,dis,Out_time);

     case 2
        prompt = {'\Omega [m^{-1/2}]','L_\xi [m]','L_\tau (time window, periods/soliton length) [ad]',...
            'n_t (log_2)','Number of steps to store N_x','\epsilon (steepness = k_0 a_0) [ad]',...
            'Shoaling ON/OFF','FOD ON/OFF','Export initial function','W-M Factor','Disipation','Output time [s]'};
        dlg_title = 'Parameters';
        options.Interpreter = 'tex';
        num_lines = 1;
		if ~exist('button_dialog_temp_2.txt', 'dir') %check if dir already exists
			defaultans=importfile_buttondialog_case1('button_dialog_temp_2.txt');
			defaultans=cellstr(defaultans);
			params = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
		else
			defaultans = {'1','50','10','11','100','0.14','1','0','yes','1','0.0021072','100'};
			params = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
		end
        % normalized frequency omega = omega[s^-1]/g^{1/2}
        Omega = str2double(params(1));
        freq_dim = Omega*sqrt(g0)/(2*pi);
        T0_dim=1/freq_dim;
        fprintf('Dimentional frequency: %.3f \n , Adimentional frequency: %.3f \n ', freq_dim, Omega)
        Lx = str2double(params(2));
        Nper = str2double(params(3));
        nt = 2.^str2double(params(4));
        % number of points to save
        Nx = str2double(params(5));
        esteep0 = str2double(params(6));
        shoalingflag = str2double(params(7));
        FODflag = str2double(params(8));
        Exportinit =  string(params(9));
        Factor=str2double(params(10));
        dis=str2double(params(11));
        Out_time=str2double(params(12));
		%update dialog options
		print_button_dialog_parameters_case1(Omega,Lx,Nper,str2double(params(4)),Nx,esteep0,shoalingflag,FODflag,Exportinit,Factor,dis,Out_time);
end        

%% Bathymetry
% hk is expressed as a function hk = 1.363 is the focusing/defocusing
% turning poin
bathymetryOptions = {'Linear A->B','Erfc A->B','Linear A->B->A','Erfc A->B->A'}; 
defSelection = bathymetryOptions{2};
bathymetryflag = bttnChoiseDialog(bathymetryOptions, 'Bathymetry profile', defSelection,...
 'Bathymetry profile'); 
fprintf( 'User selection "%s"\n', bathymetryOptions{bathymetryflag});

% selecting a function for kh 
switch bathymetryflag
    case 1
        khfunsel =  @khfun4;
    case 2
        khfunsel =  @khfun1;
    case 3
        khfunsel =  @khfun3;
    case 4
        khfunsel =  @khfun2;
end

%%% bathymetry params
% hk = 1.363 is the focusing/defocusing turning point
prompt = {'hk_{0}','hk_{fin}','\xi_1','\xi_2','\xi_{slope}'};
dlg_title = 'Solver parameters'; options.Interpreter = 'tex'; num_lines = 1;
%Read existing file or use default values if it doesn't exist.
if ~exist('kh_dialog_temp.txt', 'dir') %check if dir already exists
	defaultans=importfile_buttondialog_kh('kh_dialog_temp.txt');
	defaultans=cellstr(defaultans);
	bathymetrypar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
else
	defaultans = {'24.146','24.146','13.5','15.5','20'};
	bathymetrypar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
end

kh0 = str2double(bathymetrypar(1)); khfin = str2double(bathymetrypar(2));
xvarstart = str2double(bathymetrypar(3)); xvarstop = str2double(bathymetrypar(4)); %[m]
xslope = str2double(bathymetrypar(5));

print_kh_dialog_parameters(kh0,khfin,xvarstart,xvarstop,xslope)
if bathymetryflag == 1, warning('Slope is not used!'); end

%% solver params;
prompt = {'initial step','minimum step','tolerance'};
dlg_title = 'Solver parameters'; options.Interpreter = 'tex'; num_lines = 1;
defaultans = {'0.01','1e-6','1e-9'};
solvpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
hx0 = str2double(solvpar(1)); hxmin = str2double(solvpar(2)); tol = str2double(solvpar(3));
% Lx = 1000; % [m] % number of points to save % Nx = 250; % hx0 = 0.01; % hxmin = 1e-6; % tol = 1e-10;

%% definition of X lattice
if hx0<Lx./Nx
    hx0 = Lx./Nx;
end
Hx = Lx./Nx;
X = 0:Hx:Lx;
% Nper = 20;

%% compute the NLS parameters 
% DO NOT DISPLACE IT, it is necessary to estimate MI, soliton, DSW params
[H,K,Sigma,Cg,Cp,Alpha,Beta,Alpha3,Alpha4,Beta_21,Beta_22,Mu,Muintexp,dythe_hilb,D_hilb_general] = depthdependent_HONLSparameters_onoff(Omega,khfunsel(X,kh0,khfin,xvarstart,xvarstop,xslope),FODflag,ON_OFF_coef);
Regime=Beta./Alpha;

ratio=20/9; r_fig=400, figure('position',[20 50 ratio*r_fig r_fig])
set_latex_plots(groot)
subplot(221);
plot(X,H,X,Mu,'LineWidth',1.5);
legend({'$h$[m]','$\mu$[$m^{-1}$]'},'FontSize',Fsz-1)
xlabel('$X$[m]','FontSize',Fsz); set(gca,'Fontsize',Fsz-1);
subplot(222);
plot(X,K,X,Cp./Cg,X(2:end),Muintexp(2:end)./Muintexp(1:end-1),'LineWidth',1.5);
legend({'$k$[$m^{-1}$]','$c_p/c_g$','$e^{\int \mu}$'},'FontSize',Fsz-1)
xlabel('$X$[m]','FontSize',Fsz); set(gca,'Fontsize',Fsz-1);
subplot(223);
plot(X,Regime,'LineWidth',1.5);
legend({'$\beta/ \alpha$'},'FontSize',Fsz-1)
xlabel('$X$[m]','FontSize',Fsz); set(gca,'Fontsize',Fsz-1);
subplot(224);
plot(X,Alpha3,X,Alpha4,X,Beta_21,X,Beta_22,'LineWidth',1.5);
legend({'$\alpha_3$ (TOD)','$\alpha_4$ (FOT)','$\beta_{21}$','$\beta_{22}$'},'FontSize',Fsz-1)
xlabel('$X$[m]','FontSize',Fsz); set(gca,'Fontsize',Fsz-1);

axesHandles = get(gcf,'children');
axesHandles = findall(0,'type','axes');
% set(axesHandles,xlim,[0,Lx])
axis(axesHandles,'tight')
% ...and their initial values
[h0,k0,sigma0,cg0,cp0,alpha0,beta0,alpha30,alpha40,beta_210,beta_220,mu0,muintexp0,dythe_hilb0,D_hilb_general0] = depthdependent_HONLSparameters_onoff(Omega,kh0,FODflag,ON_OFF_coef);

regime=beta0/alpha0;
% Reference amplitude
B0 = esteep0./k0;
% Normalization scales
Tnl = sqrt(abs(2*alpha0./beta0))./B0;
Lnl = 1./abs(beta0)/B0^2;

%% excitation type
inputOptions = {'Shot noise like perturbed plane wave','Harmonically perturbed plane wave','Akhmediev Breather',...
    'Peregrine Breather','Bright Soliton','Dark Soliton','Gaussian on top of a background',...
    'Sedletsky bright soliton','Steplike initial condition (dam break)','Amin noise','Benchmark Gaussian','3 wave gaussian','Jonswap','Benchmark soliton','Kuznetsov-Ma'}; 
defSelection = inputOptions{3};
excflag = bttnChoiseDialog(inputOptions, 'Type of initial conditions', defSelection,...
 'Type of initial conditions',bttn_display); 
fprintf( 'User selection "%s"\n', inputOptions{excflag});

% At each type of initial condition, different parameters are associated.
% Moveover, omega0 (on which the time window depends) and the plotting
% limit windowwidth are different.
IC_case=0;
switch excflag
    case 1
		IC_case=1;
        if regime> 0 ,  warning('Plane waves are stable'), end
        prompt = {sprintf('Noise fraction, B_0 = %g',B0)};
        dlg_title = 'Noisy Plane Wave';  options.Interpreter = 'tex'; num_lines = 1;
        defaultans = {'1e-4'};        
        NPWpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        save_params=NPWpar;
        etanoise =  str2double( NPWpar(1));
        eta0 = 1-etanoise;
        U0 = str2double( NPWpar(1));
        variance = etanoise/nt*2/pi;
        omega0 = 1/Tnl*sqrt(2);
        windowwidth = 10*Tnl;
        
    case 2
		IC_case=2;
	    if regime> 0 ,  warning('Plane waves are stable'), end
        prompt = {'Total initial energy','Sideband fraction','Sideband unbalance','Relative phase (\phi_1+\phi_{-1})/2-\phi_0','\Omega/\Omega_Max'};
        dlg_title = 'Harmonically perturbed Plane Wave'; options.Interpreter = 'tex';  num_lines = 1;
        defaultans = {'1','1e-2','0e-3','pi/2','1'};       
        PPWpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        save_params=PPWpar;
        U0 = str2double( PPWpar(1));
        eta0 = U0 - str2double(PPWpar(2));
        unb = str2double(PPWpar(3));
        psi0 = (eval(char(PPWpar(4))));
		omega_max=Tnl*sqrt(2);
        omega0 = 1/Tnl*sqrt(2)*str2double(PPWpar(5));
        windowwidth = 10*Tnl;
        eta1 = (U0-eta0+unb)/2;
        etam1 = (U0-eta0-unb)/2;       
        if eta1*etam1 < 0, error('The chosen unbalance is to big or negative intensity are chosen!'); end
        

    case 3
		IC_case=3;
		if regime> 0 ,  warning('Plane waves are stable'), end
        prompt = {'a','Initial point \xi_0'};
        dlg_title = 'Akhmediev Breather'; options.Interpreter = 'tex'; num_lines = 1;
        defaultans = {'0.25','-500'};        
        ABpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        save_params=ABpar;
        aAB = str2double( ABpar(1));
        X0 = str2double( ABpar(2));
        bAB = sqrt(8*aAB*(1-2*aAB));
        omegaAB = 2*sqrt(1-2*aAB);
        omega0 = omegaAB/Tnl;
        windowwidth = 10*Tnl;
        
    case 4
		IC_case=4;
		if regime> 0 ,  warning('Plane waves are stable'), end
        prompt = {'Initial point \xi_0'};
        dlg_title = 'Peregrine Breather'; options.Interpreter = 'tex'; num_lines = 1;
        defaultans = {'-500'};        
        Perpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);          
        save_params=Perpar;
        X0 = str2double( Perpar(1));        
        omega0 = 2*pi/Tnl;
        windowwidth = 20*Tnl;
        if Nper<20, warning('The time window is too narrow!'), end  
        
    case 5
		IC_case=5;
	    if regime> 0 ,  warning('Bright soliton is not a solution'), end
        prompt = {'Soliton width','Soliton number'};
        dlg_title = 'N-soliton';  options.Interpreter = 'tex'; num_lines = 1;
        defaultans = {'1','1'};    
        Solpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        save_params=Solpar;
        tau0 = str2double(Solpar(1));
        Nsol = str2double( Solpar(2));      
        omega0 = 2*pi/tau0/Tnl;
        windowwidth = 20*Tnl;
        if Nper<20, warning('The time window is too narrow!'), end

     case 6
		IC_case=6;
	    if regime< 0 ,  warning('Dark soliton is not a solution'), end
        prompt = {'Soliton width','Soliton number'};
        dlg_title = 'N-soliton';  options.Interpreter = 'tex'; num_lines = 1;
        defaultans = {'1','1'};    
        Solpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        save_params=Solpar;
        tau0 = str2double(Solpar(1));        
        Nsol = str2double(Solpar(2));     
        omega0 = 2*pi/tau0/Tnl;
        windowwidth = 20*Tnl;      
        if Nper<50 , warning('The time window is too narrow!'),end
		
    case 7
		IC_case=7;
	    if regime< 0 ,  warning('The dynamics maybe unstable in the defocusing regime!'), end
        if Nper<50, warning('The time window is too narrow!'),  end
        prompt = {'Perturbation width (in Tnl units)','Background rel. amplitude','Perturbation rel. amplitude','Supergaussian number (=1 Gaussian)'};
        dlg_title = 'Gaussian on a background'; options.Interpreter = 'tex'; num_lines = 1;
        defaultans = {'1','0.5','1','1'};   
        Pertpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        save_params=Pertpar;
        tau0 = str2double(Pertpar(1));           
        Bback = str2double( Pertpar(2));
        Bpert = str2double( Pertpar(3));
        MSG = str2double( Pertpar(4));     
        omega0 = 2*pi/tau0/Tnl;
        windowwidth = (19+tau0)*Tnl; %this is for the plot.
        
    case 8
		IC_case=8;
	    if regime< 0 ,  warning('This is a special bright soliton'), end
        prompt = {'t_0'};
        dlg_title = 'Sedletsky 1-soliton'; options.Interpreter = 'tex'; num_lines = 1;
        defaultans = {'-200'};   
        Solpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
                save_params=Solpar;
        t0 = str2double(Solpar(1));             
        omega0 = 2*pi/Tnl;
        windowwidth = 60*Tnl;      
        if Nper<50, warning('The time window is too narrow!'), end
        
      case 9
        IC_case=9;
        % Steplike profile (dam break)
	    if regime< 0 ,  warning('The dynamics maybe unstable in the defocusing regime!'), end
        if Nper<50, warning('The time window is too narrow!'), end
        prompt = {'Perturbation offset (in Tnl units)','Background rel. amplitude (in units of \varepsilon)','Perturbation rel. amplitude','Supergaussian number (=1 Gaussian)'};
        dlg_title = 'Steplike for dam break';  options.Interpreter = 'tex'; num_lines = 1;
        defaultans = {'0','1','5','100'};      
        Pertpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        save_params=Pertpar;
        toff = str2double(Pertpar(1));           
        Bback = str2double( Pertpar(2));
        Bpert = str2double( Pertpar(3));
        MSG = str2double( Pertpar(4));   
        omega0 = 2*pi/Tnl;
        windowwidth = 50*Tnl;
		
		
   case 10
	    IC_case=10;
        % Real noise pert plane wave
	    if regime< 0 ,  warning('The dynamics maybe unstable in the defocusing regime!'), end
		if Nper<50, warning('The time window is too narrow!'), end
        prompt = {'Noise fraction'};
		dlg_title = 'Noisy plane wave perturbated only on the amplitude';  options.Interpreter = 'tex';
        num_lines = 1;
        defaultans = {'0.01'};   
        Pertpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        save_params=Pertpar;
        frac = str2double(Pertpar(1));                 
        omega0 = 2*pi/Tnl;
        windowwidth = 50*Tnl;
		
	case 11
		IC_case=11;
        % gaussian zhang paper
		%steepness = 0.1
		%T0=7s; f0=0.14286
		%a0=1.75m, kh=..
		%from -16T0 to 16T0
        if regime< 0 ,  warning('The dynamics maybe unstable in the defocusing regime!'), end
		if Nper<50, warning('The time window is too narrow!'), end
        prompt = {'m'};
        dlg_title = 'm-gaussian';  options.Interpreter = 'tex'; num_lines = 1;
        defaultans = {'4'};      
        Pertpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        save_params=Pertpar;
        m_gaus = str2double(Pertpar(1));           
        omega0=Omega;	
        windowwidth = 20*Tnl;
		
	case 12
		IC_case=12;
	    if regime> 0 ,  warning('Plane waves are stable'), end
		prompt = {'Total initial energy','Sideband fraction','Sideband unbalance',...
			'Relative phase (\phi_1+\phi_{-1})/2-\phi_0','\Omega/\Omega_Max','\sigma_0','\sigma_m'};
		dlg_title = 'Harmonically perturbed Plane Wave';  options.Interpreter = 'tex'; um_lines = 1;
		defaultans = {'1','0e-10','0e-30','pi/2','1.2','0.02','0.2'};
		PPWpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
		save_params=PPWpar;
		U0 = str2double( PPWpar(1));
		eta0 = U0 - str2double(PPWpar(2));
		alpha = str2double(PPWpar(3));
		psi0 = (eval(char(PPWpar(4))));
		omega0 = 1/Tnl*sqrt(2)*str2double(PPWpar(5));
		windowwidth = 10*Tnl;
		eta1 = (U0-eta0+alpha)/2;
		etam1 = (U0-eta0-alpha)/2;    
		sigma_0=str2double( PPWpar(6));
		sigma_m=str2double( PPWpar(7));
		if eta1*etam1 < 0, error('The chosen unbalance is to big or negative intensity are chosen!'),	end
		
	case 13
		IC_case=13;
	    if regime> 0 ,  warning('Plane waves are stable'), end
		prompt = {'gamma'};
		dlg_title = 'Jonswap'; options.Interpreter = 'tex';	num_lines = 1;
		defaultans = {'2.5'};
		PPWpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
		save_params=PPWpar;
% 		Hw=str2double( PPWpar(1) ); % wave height significant (m)
		Hw=B0;
		gama=str2double(PPWpar(1)); % Peak period (s)
		fom=freq_dim;
		Tw=1/fom;
		omega0=Omega;
% 		T0 = 2*pi/omega0;
		T0 = Tw;
		Lt = T0*Nper;
		skl=1;
% 		freq_jon=32;
        windowwidth = 8*Tnl;
		if Lt/nt > 2*Tw/10,	warning('time step too small'),	end
		
	case 14
		IC_case=14;
		if regime> 0 ,  warning('Plane waves are stable'), end
		prompt = {'N'};
		dlg_title = 'Benchmark soliton'; options.Interpreter = 'tex'; num_lines = 1;
		defaultans = {'2'};
		PPWpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
		save_params=PPWpar;
		N_sol=str2double( PPWpar(1)); % wave height significant (m)	
		%for N=2: a0=5mm; esteep=0.16/2=0.08, freq_dim=2,h=1.5m
		%kh=24.146
		%for N=3: a0=2mm, esteep=0.12/3=0.04,freq_dim=2,h=1.5m
		%for N=4:a0=1mm; esteep=0.16/4=0.04, L=10m or freq_dim=2, h=1.5m	
		fom=Omega/(2*pi);
		windowwidth = Nper*Tnl/8;
		omega0 = sqrt(abs(beta0/alpha0)).*B0;
		
	case 15
		IC_case=15;
	    if regime> 0 ,  warning('Plane waves are stable'), end
        prompt = {'a','Initial point \xi_0'};
        dlg_title = 'Kuznetsov-Ma soliton (a>0.5)'; options.Interpreter = 'tex'; num_lines = 1;
        defaultans = {'1','0'};      
        ABpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        save_params=ABpar;
        aAB = str2double( ABpar(1));
        X0 = str2double( ABpar(2));
        bAB = sqrt(8*aAB*(1-2*aAB));
        omegaAB = 2*sqrt(1-2*aAB);
        omega0 = 2*pi/Tnl;
        windowwidth = 20*Tnl;
        if Nper<20, warning('The time window is too narrow!'),   end
        
    otherwise 
        error('No valid initial condition option');
end
save_params_names=prompt;%TO BE USED TO SAVE PARAMETERS TO FILE. 
%% definition of T lattice
% given the type of IC, we define omega0 and the time lattice
% omega0 is the characteristic  angular frequency dependent on the chosen initial
% conditions

% omega0 = sqrt(abs(beta0/alpha0)).*B0;
T0 = 2*pi/omega0;
% calculate the time window span
Lt = T0*Nper;
t = linspace(-Lt/2,Lt/2,nt+1);
t = t(1:end-1);
ht = t(2)-t(1);
omegaxis = 2*pi.*linspace(-1/ht/2,1/ht/2,nt+1); omegaxis = omegaxis(1:end-1);
%FFT phase shifts
kt1 = linspace(0,nt/2 + 1,nt/2)';
kt2 = linspace(-nt/2,-1,nt/2)';
kt = ((2*pi/ht/nt)*[kt1; kt2]);
%% make k axis to as a function of omegaxis for the integration of the hilbert term in the integration step. 
kaxis=zeros(length(kt),1);
omes=omegaxis(end);

k0_est=1; %estimate the deep water k for the highest frequency. use this as initial guess
tol=0.001;
tic
for j=1:length(kt)
	omes=omegaxis(end+1-j);
 	k0_est = nr_k0(k0_est,omes,h0,1,tol);
	kaxis(end+1-j)=k0_est;
end
toc

kaxis_min=0.4/h0;
kaxis(kaxis<kaxis_min)=kaxis_min; %make threshold to avoid k aprox 0.  
% kaxis plot.
% figure()
% subplot(121)
% plot(omegaxis,1./tanh(kaxis.*h0),'LineWidth',1.5)
% ylabel('1/tanh (kh)')
% xlabel('\omega')
% subplot(122)
% plot(omegaxis,sqrt(kaxis)./sqrt(tanh(kaxis.*h0)),'LineWidth',1.5)
% ylabel('\sqrt{k/tanh (kh)}')
% xlabel('\omega')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% field initialization
u = zeros(nt,Nx+1);
uspectrum = u;
saveflag = zeros (Nx+1,1);
N = saveflag;
P = N;

%% Initial conditions
switch excflag
    case 1
        % MI from noise
        % variance = 1e-2;
        u0 = B0.*(sqrt(eta0) + sqrt(variance).*randn(size(t)) + 1i.*sqrt(variance).*randn(size(t)));

    case 2
        % Harmonically perturbed plane wave
        u0 = B0.*(sqrt(eta0) + sqrt(etam1).*exp(1i*omega0.*t).*exp(1i.*psi0) + sqrt(eta1).*exp(-1i*omega0.*t).*exp(1i.*psi0)).*exp(-0*t.^2./(30*T0.^2));
    
    case 3
        % Akhmediev Breather
%         aAB = 0.1;
%         bAB = sqrt(8*aAB*(1-2*aAB));
%         omegaAB = 2*sqrt(1-2*aAB);
        u0 = B0.*(1 + (omegaAB^2/2.*cosh(bAB*X0/Lnl) - 1i*bAB*sinh(bAB.*X0/Lnl))./(sqrt(2*aAB)*cos(omegaAB*t/Tnl) - cosh(bAB.*X0/Lnl))).*exp(1i*X0/Lnl);    
    
    case 4
        % Peregrine Soliton
        % TP = sqrt(abs(2*alpha0./beta0))./B0;
        % Lnl = 1./beta0/B0^2;
        % X0 = -500;
        u0 = B0.*(1-4.*(1-2i.*X0./Lnl)./(1+4*(t./Tnl).^2 + 4*(X0/Lnl)^2)).*exp(-1i*X0/Lnl);
    case 5
        % Bright Soliton
        % soliton order
        % Nsol = 3;
        u0 = Nsol*(B0/tau0).*sech((B0/tau0)*sqrt(abs(beta0/2/alpha0)).*t);
        
    case 6
        % Dark Soliton
        u0 = Nsol*B0*tanh(B0*sqrt(abs(beta0/2/alpha0)).*t);
    
    case 7
        % Gaussian on top of a background 
        u0 = B0*(Bback + Bpert*exp(-(t./Tnl/tau0).^(2*MSG)));
                
    case 8
        % Bright soliton in defocusing regime, see Gandzha&Sedletsky PLA
        % 381 (2017)
        S = -(beta_210+beta_220)./(6*alpha0);
        K0 = B0*sqrt(S);
        omegasol0 = (-alpha0*(beta_210+beta_220) + 3*alpha0*beta0)/(6*alpha0*beta_220);        
        V = K0^2*alpha0 - 2.*alpha0*omegasol0 - 3.*alpha0*omegasol0^2;
        u0 = B0.*sech(K0.*(t-t0)).*exp(-1i*t*omegasol0);
        
    case 9
        % Dam break
        u0 = B0*(Bback + Bpert*exp(-((t+Lt/2-toff)./Lt*2).^(2*MSG))).*exp(-(t./2/Lt*5).^50);    
   
	case 10
        % Real noise perturbated plane wave
        u0 = B0.*(frac.*randn(size(t)));
		
    case 11
        % zhang  paper gaussian
        u0 = B0*(exp(-(t./(m_gaus.*T0)).^(2)));
		windowwidth=16*T0;
	case 12 %3wave gaussians
		gaus_0=sqrt(eta0).*exp(-(pi.*t.*(2.*sigma_0.^2)).^2);
		gaus_1=sqrt(eta1).*exp(-(pi.*t.*(2.*sigma_m.^2)).^2);
		gaus_m=sqrt(etam1).*exp(-(pi.*t.*(2.*sigma_m.^2)).^2);
		u0 = B0.*(gaus_0 + gaus_1.*exp(1i*omega0.*t).*exp(1i.*psi0) + gaus_m.*exp(-1i*omega0.*t).*exp(1i.*psi0)).*exp(-0*t.^2./(30*T0.^2));
	case 13
		wh=cobagelombang4(Hw,Tw*sqrt(g0),gama,t,B0,skl);
%   		wh=real(wh.*exp(-1i*Omega*t));
		u0=hilbert((wh-mean(wh)).*tukeywin(length(t),0.1)');

		u0_mean=mean(abs(u0));
% 		u0=u0/u0_mean*B0;
% 	    wh=wh/u0_mean*B0;
		figure()
		hold on
		plot(t,abs(u0),'-r','linewidth',2)
		plot(t,(wh),'-b','linewidth',0.5)
		hold off
		
	case 14
		%Benchmark soliton
% 		u0 = Nsol*(B0/tau0).*sech((B0/tau0)*sqrt(abs(beta0/2/alpha0)).*t);
		u0=N_sol.*B0.*sech(t);

	case 15
        % Kuznetsov-Ma soliton
        u0 = B0.*(1 + (omegaAB^2/2.*cosh(bAB*X0/Lnl) - 1i*bAB.*sinh(bAB.*X0/Lnl))./(sqrt(2*aAB).*cos(omegaAB*t/Tnl) - cosh(bAB.*X0/Lnl))).*exp(1i*X0./Lnl);    
		
end
%%
% figure()
% hold on
% plot(t/sqrt(g0),abs(u0))
% plot(t/sqrt(g0),real(u0.*cos(omega0.*t')))
% hold off
% title('initial condition')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Initialization of computation
tplot = abs(t)<=windowwidth;
tclean = abs(t)<=2*windowwidth; %array cleaning the borders to have better analysis. this is approximate

pind = 1;

u(:,1) = (u0);
ucurr = u(:,1);
UdBth = -30;
ucurr_spec=fftshift(ifft(ucurr,[],1),1);
uspectrum(:,pind) = ucurr_spec;
Uksp2_0 = 10*log10(abs(ucurr_spec./B0));
Uksp2_0 = (Uksp2_0>UdBth).*(Uksp2_0-UdBth) +  UdBth;

N(pind) = trapz(t,cg0*abs(ucurr).^2);
P(pind) = real(trapz(t,cg0*0.5i.*(fft(-1i.*kt.*ifft(ucurr)).*conj(ucurr)-fft(-1i.*kt.*ifft(conj(ucurr))).*ucurr)));
x = 0;
hx = hx0;

pind = 2;
count = 0;
%%
carrier_phase=zeros(Nx+1,1);
[ d_car, ix_car ] = min( abs( max(ucurr_spec) - ucurr_spec ) );%find the critical value index
ix_car;
carrier_freqs=omegaxis(ix_car);
carrier_fourier=ucurr_spec(ix_car);
carrier_phase(1)=unwrap(angle(ucurr_spec(ix_car)));

%% Computation
f=figure('name','First last','position',[20 15 900 600]);
set_latex_plots(groot)
filename_gif = 'NLSAB.gif';
set(0,'defaulttextfontsize',Fsz);
while x<Lx
        err = tol*10;
        numberofrejstep = 0;
        while err>tol
% 			kh1=khfunsel(x+hx/2,kh0,khfin,xvarstart,xvarstop,xslope);
            [~,k1,~,cg1,~,alpha1,beta1,alpha31,alpha41,beta_211,beta_221,~,muintexp1,dythe_hilb1,D_hilb_general1] = depthdependent_HONLSparameters_onoff(Omega,khfunsel(x+hx/2,kh0,khfin,xvarstart,xvarstop,xslope),FODflag,ON_OFF_coef);
%             kh2=khfunsel(x+hx,kh0,khfin,xvarstart,xvarstop,xslope);
			[~,k2,~,cg2,~,alpha2,beta2,alpha32,alpha42,beta_212,beta_222,~,muintexp2,dythe_hilb2,D_hilb_general2] = depthdependent_HONLSparameters_onoff(Omega,khfunsel(x+hx,kh0,khfin,xvarstart,xvarstop,xslope),FODflag,ON_OFF_coef);		
% 			[ucurrnew,err] = DV4ORK34_hilb(hx,ucurr,kt,alpha0,alpha1,alpha2,beta0,beta1,beta2,alpha30,alpha31,alpha32,alpha40,alpha41,alpha42,beta_210,beta_211,beta_212,beta_220,beta_221,beta_222,muintexp0,muintexp1,muintexp2,dythe_hilb0,dythe_hilb1,dythe_hilb2,shoalingflag,dis);
% 			[ucurrnew,err] = DV4ORK34_shock_deep(hx,ucurr,kt,alpha0,alpha1,alpha2,beta0,beta1,beta2,alpha30,alpha31,alpha32,alpha40,alpha41,alpha42,beta_210,beta_211,beta_212,beta_220,beta_221,beta_222,muintexp0,muintexp1,muintexp2,dythe_hilb0,dythe_hilb1,dythe_hilb2,shoalingflag,dis,kh0,kh1,kh2,Omega,kaxis); 			
			[ucurrnew,err] = DV4ORK34_1(hx,ucurr,kt,alpha0,alpha1,alpha2,beta0,beta1,beta2,alpha30,alpha31,alpha32,alpha40,alpha41,alpha42,beta_210,beta_211,beta_212,beta_220,beta_221,beta_222,muintexp0,muintexp1,muintexp2,shoalingflag,dis);           
			if (err<tol)
                x = x + hx;
                ucurr = ucurrnew;
                count = count + 1;
                step(count) = hx;
            else
                numberofrejstep =  numberofrejstep + 1;
                if numberofrejstep > 10
                    fprintf('10!\n');
                end
            end
            hx = min(max(0.5,min(2,.95*(tol./err).^.25)).*hx,Hx);
            if hx < hxmin 
                error('Integration step too small');
            end
            if isnan(err)
                error('Diverges!');
            end
        end
    if isnan(ucurr)
        error('diverges!!!');
    end
    k0 = k2;
    alpha0 = alpha2;
    muintexp0 = muintexp2;
    beta0 = beta2;
    cg0 = cg2;
    alpha30 = alpha32;
    alpha40 = alpha42;
    beta_210 = beta_212;
    beta_220 = beta_222;
    
    %     hz0 = hz;
    if x>=X(pind) && saveflag(pind) == 0;
        if max(abs(ucurr))*k2>0.4
            warning('Wave breaking probably occurs!');
        end
        saveflag(pind) = 1;   
        N(pind) = trapz(t,cg0*abs(ucurr).^2);
        P(pind) = real(trapz(t,cg0*0.5i.*(fft(-1i.*kt.*ifft(ucurr)).*conj(ucurr)-fft(-1i.*kt.*ifft(conj(ucurr))).*ucurr)));
%         NQ(pind) = trapz(t,(abs(ucurr).^2-1).^2);
%         fprintf('Writing the field %d, X = %g, N = %g, P = %g\n\n', pind,x, N(pind), P(pind)); 
            u(:,pind) = ucurr;
            % computing spectrum
            uspectrum(:,pind) = ifftshift(ifft(ucurr,[],1),1);
			ucurr_spec = ifftshift(ifft(ucurr,[],1),1);

            X(pind) = x;
			Uksp2 = 10*log10(abs(ucurr_spec./B0));
			Uksp2 = (Uksp2>UdBth).*(Uksp2-UdBth) +  UdBth;
			carrier_fourier=ucurr_spec(ix_car);
			carrier_phase(pind)=unwrap(angle(ucurr_spec(ix_car)));

			subplot(131)
            plot(t(tplot),abs(ucurr(tplot)),t(tplot),abs(u0(tplot)),'Linewidth',1.5);
%             xlim([-windowwidth,windowwidth]);
            ylim([0,5*B0]);
			xlim([min(t(tplot)),max(t(tplot))]);
            xlabel('$T [m^{1/2}]$','Interpreter','latex','fontsize',Fsz); 
            set(gca,'fontsize',Fsz-1);
			title(strcat('kh=',num2str(kh0,'%.2f'),', $$\epsilon =$$',num2str(max(abs(ucurr(tplot))).*k0,'%.2f')),'interpreter','latex','fontsize',Fsz-5);
  
			subplot(132)
            plot(t(tplot),unwrap(angle(ucurr(tplot)))/pi-carrier_phase(pind)/pi,t(tplot),unwrap(angle(u0(tplot)))/pi-carrier_phase(1)/pi,'Linewidth',1.5);
            xlabel('$T [m^{1/2}]$','Interpreter','latex','fontsize',Fsz);
			xlim([min(t(tplot)),max(t(tplot))]);
%             xlim([-windowwidth,windowwidth]);
			title(strcat(num2str(100.*x./Lx,'%.1f'),' \% Sim  ',', $\xi =$ ',num2str(x,'%.1f'),'m'),'interpreter','latex','fontsize',Fsz-5);
            set(gca,'fontsize',Fsz-1);        
           
			subplot(133)
% 			plot(kappaxis(kplot),Uksp2(kplot),kappaxis(kplot), Uksp2_0(kplot),'Linewidth',1.5);
%             semilogy(omegaxis,abs(uspectrum(:,pind)).^2,omegaxis,abs(uspectrum(:,1)).^2);
% 			plot(kappaxis(kplot),Uksp2(kplot),kappaxis(kplot), Uksp2_0(kplot),'Linewidth',1.5);
            plot(omegaxis,Uksp2,omegaxis,Uksp2_0,'Linewidth',1.5);
            xlabel('$\delta \Omega [m^{-1/2}]$','Interpreter','latex','fontsize',Fsz);
            xlim([-5*sqrt(2)*omega0,5*sqrt(2)*omega0]);
            set(gca,'fontsize',Fsz-1);        
           
			drawnow
		sgtitle(dlg_title)
        F = getframe(gcf);
        im = frame2im(F);
        [imind,cm] = rgb2ind(im,256);
     %   if pind == 2;
    %        imwrite(imind,cm,filename_gif,'gif', 'DelayTime',.1,'Loopcount',inf); 
     %   else         
     %       imwrite(imind,cm,filename_gif,'gif','WriteMode','append','DelayTime',.1);
     %   end       
        pind = pind + 1;
	end   
end
%%
% figure()
% plot(X,unwrap(carrier_phase))

%% recover the old variables (see B above)
u = u.*(ones(length(t),1)*(Cg.^-0.5)).*Cg(1).^0.5;
uspectrum =  uspectrum.*(ones(length(t),1)*(Cg.^-0.5)).*Cg(1).^0.5;
%% export initial conditions for experiment on sydney
if Exportinit=='yes'
    sample_frequency = 100. ;%hz this is the sample frequency of the output. 
    initial_depth= H(1);
    pos_gau=import_pos_gau_for_input('pos_wavegauges.txt');
    pos_gau=table2array(pos_gau);
    %% make header
    Input_function=inputOptions{excflag};%manually choose the input function to be used
    Posible_functions=['Mod_instability','Soliton','Ak_Breather','Peregrine','Super_Gaussian'];
    input_signal=Factor*real(u(tplot,1).*exp(1i*(freq_dim*(2*pi)*t(tplot)/sqrt(g0)))');
    try
    figure('name','Output sample');
    tp=t(tplot)/sqrt(g0);
    [A1,t_sample] = resample(input_signal,tp,100);
    fprintf('sanity check: Wave maker sample frequency shoud be 0.01 \n sample frequency is:  %.3f \n ',t_sample(2)-t_sample(1))
    if A1(1)>0
        t0 = find(A1<0,1,'first');
        t1 = find(A1<0,1,'last');
    else
        t0 = find(A1>0,1,'first');
        t1 = find(A1>0,1,'last');
    end
    %borders at 0 (a tukey window might work also.)
    A2=[0 A1(t0:t1)' 0]';
    if t1==length(t_sample)
        t_sample(t1+1)=t_sample(t1);
    end
    t2= t_sample(t0-1:t1+1) ;
    M=[t2'-min(t2),A2];
    %% saving stuff
    [flm fullfolder]=save_filename(freq_dim,B0,esteep0,T0_dim);
    mkdir(fullfolder) ;
    filename_input=fullfile(fullfolder,strcat(flm,'_input.txt'));
    print_input_file2(filename_input,Input_function,sample_frequency,initial_depth,Factor,M);
    filename_wm=fullfile(fullfolder,strcat(flm,'_run_001.txt'));
    csvwrite(filename_wm,M) ;
    filename_pos=fullfile(fullfolder,strcat(flm,'_pos.txt'));
    print_pos_file(filename_pos,Input_function,sample_frequency,initial_depth,Factor,pos_gau);
    mkdir(fullfile(fullfolder,'simulation and input'))
	img_folder=fullfile(fullfolder,'simulation and input');
    filename_params=fullfile(fullfolder,strcat(flm,'_params.txt'));
    print_params_file(filename_params,Input_function,sample_frequency, save_params,save_params_names)
    figure('name','signal');
    title('Translated in time and borders at 0','FontSize',Fsz);
    plot(t2'-min(t2),A2,'b');
    xlabel('T (s)','fontsize',Fsz);
    ylabel('Amplitude (m)','fontsize',Fsz);
    savefig(fullfile(img_folder,strcat(flm,'_input_signal')));
 	copyfile('button_dialog_temp_1.txt',fullfolder);
	copyfile('kh_dialog_temp.txt',fullfolder);
 	status = copyfile('default_coef_equation_temp.txt',fullfolder);
	
    %% figures
	if length(t_sample)==length(A1)
		figure('name','Resample')
		title('Resample from simulation','FontSize',Fsz)
		hold on
		plot(t_sample,A1,'.r')
		plot(tp,input_signal,'-b')
		hold off
		xlabel('T (s)','fontsize',Fsz);
		ylabel('Amplitude (m)','fontsize',Fsz);
		savefig(fullfile(img_folder,strcat(flm,'_sample_check')));
	end
%%
	figure('name','signal')
    title('Translated in time and borders at 0','FontSize',Fsz)
    subplot(211);
    l1=plot(t2'-min(t2),A2,'b');
    set(l1,'LineWidth',.8)
    xlabel('T (s)','fontsize',Fsz);
    ylabel('Amplitude (m)','fontsize',Fsz);
    subplot(212);
    sig_ff=ifft(A2);
    L=length(sig_ff);
    P2 = abs(sig_ff/L);
    P1 = P2(1:L/2+1);
    P1(2:end-1) = 2*P1(2:end-1);
    fa=100;
%Define the frequency domain f and plot the single-sided amplitude spectrum P1. The amplitudes are not exactly at 0.7 and 1, as expected, because of the added noise.
% On average, longer signals produce better frequency approximations.
    f = fa*(0:(L/2))/L;
    pl_1=plot(f,P1);
    set(pl_1,'LineWidth',1)
    xlim([0,3]); 
    savefig(fullfile(img_folder,strcat(flm,'_input_signal2')));
	end
end
%% Conservations plot.
f = plot_conservations(X,N,P);if Exportinit=='yes'
        savefig(fullfile(img_folder,strcat(flm,'_params')));
end 



%% Data processing: extracting sidebands for three-wave phase-space
if any(IC_case==[1,2,3])
	IAS = find(omegaxis<=omega0+.1 & omegaxis>=omega0-.1); IAS = IAS(round(length(IAS)/2));
	IS = find(omegaxis<=-omega0+.1 & omegaxis>=-omega0-.1); IS = IS(round(length(IS)/2));
	IAS2 = find(omegaxis<=2*omega0+.1 & omegaxis>=2*omega0-.1); IAS2 = IAS2(round(length(IAS2)/2));
	IS2 = find(omegaxis<=-2*omega0+.1 & omegaxis>=-2*omega0-.1); IS2 = IS2(round(length(IS2)/2));
	IAS3 = find(omegaxis<=3*omega0+.1 & omegaxis>=3*omega0-.1); IAS3 = IAS3(round(length(IAS2)/2));
	IS3 = find(omegaxis<=-3*omega0+.1 & omegaxis>=-3*omega0-.1); IS3 = IS3(round(length(IS2)/2));

	utrunc = uspectrum([nt/2+1,IS,IAS,IS2,IAS2,IS3,IAS3],:);
	Eta0 = abs(utrunc(1,:)).^2/B0^2;
	Eta1 = abs(utrunc(3,:)).^2/B0^2;
	Etam1 = abs(utrunc(2,:)).^2/B0^2;
	Eta2 = abs(utrunc([5],:)).^2/B0^2;
	Etam2 = abs(utrunc([4],:)).^2/B0^2;
	Unb = Eta1 - Etam1;
	Utr = Eta0 + Eta1 + Etam1;
	Psi = unwrap(angle(utrunc(3,:)))+unwrap(angle(utrunc(2,:))) - 2*unwrap(angle(utrunc(1,:)));

	
	
	Psi_2d=linspace(0,2*pi,200+1)';
	eta_2d=linspace(0,1.0001,200+1);
	gamma_3w=(omega0).^2-1;

	H3w=eta_2d.*(eta_2d-1).*cos(2.*Psi_2d)+gamma_3w.*eta_2d+0.75.*eta_2d.^2;

	X3w=eta_2d.*cos(Psi_2d);
	Y3w=eta_2d.*sin(Psi_2d);

	figure('name','regime and decomp');
	set_latex_plots(groot)
	axes('Position',[.12,.58,.74, .36]);
	hold on
	plot(X, Eta0,'linewidth',1.2,'color',color_main)
	plot(X, Eta1,'linewidth',1.2,'color',color_sideband)
	plot(X,Etam1,':','linewidth',1.2,'color',color_sideband)
	plot(X, Eta2,'linewidth',1.2,'color',color_2sideband)
	plot(X,Etam2,':','linewidth',1.2,'color',color_2sideband)
	plot(X, Utr,'--','linewidth',1.2,'color',color_utr);	
	hold off
	legend('$\eta_0$','$\eta_1$','$\eta_{-1}$','$\eta_{-2}$','$\eta_{-3}$','$\eta_{2}$','$\eta+\eta_1+\eta_{-1}$');
	xlim([0,Lx]);
	set(gca,'FontSize',Fsz-1,'XTickLabel','');
	ylabel('$\eta_n$','FontSize',Fsz)
	% text(.15,.8,'(a)');

	%%
	axes('Position',[.12,.15,.74, .36]);
	[phax,phlin,nl] = plotyy(X, Psi/2/pi, ...
		X, omega0./sqrt(abs(Beta./Alpha))./B0);
	set(phlin,'linewidth',1.5,'color','b');
	set(nl,'linewidth',1.5,'linestyle','-','color','r');
	hold(phax(1),'on');
	% plot(phax(2),X,sqrt(2).*ones(size(X)),':','linewidth',1.2)
	plot(phax(1),X,ones(size(X)),':b','linewidth',1.2)
	% legend('\Omega\alpha','P/N');
	xlabel('X','fontsize',Fsz);
	xlim([0,Lx]); 
	% ylim([-.5,.5]);grid;
	set(phax(1),'FontSize',Fsz-1,'xlim',[0,Lx],'Ycolor','b');
	set(phax(2),'FontSize',Fsz-1,'xlim',[0,Lx],'Ycolor','r');
	ylabel(phax(1),'$\psi/\pi$','FontSize',Fsz);
	ylabel(phax(2),'$\beta/\alpha$','FontSize',Fsz);
	% text(.15,1.35,'(b)');
	hold(phax(1),'off');
	if Exportinit=='yes'
	   savefig(fullfile(img_folder,strcat(flm,'_regime')));
	end
%%
%phase plane plots
figure('name','3w space 1');
set_latex_plots(groot)
f=figure('name','3wave','position',[20 15 ratio*r_fig r_fig]);
% subplot(121)
plot(Psi/2/pi,(Eta1+Etam1)./ Utr,Psi(1)/2/pi,(Eta1(1)+Etam1(1))./Utr(1),'rx',Psi/2/pi,(Eta0)./ Utr,'--',Psi(1)/2/pi,Eta0(1)./Utr(1),'rx','linewidth',1);
set(gca,'FontSize',Fsz-1);
xlabel('$\psi/\pi$','Fontsize',Fsz);
ylabel('$\eta$','Fontsize',Fsz);
if Exportinit=='yes'
	savefig(fullfile(img_folder,strcat(flm,'_3W_space1')));
end 
%%
figure('name','3w space 2','position',[20 15 ratio*r_fig r_fig]);
set_latex_plots(groot)
hold on
s=surf(X3w,Y3w,H3w);
set(s, 'EdgeColor', 'None','LineWidth',1.,'FaceColor','interp','facealpha',0.7);
% alpha 0.5
levl=min(min(H3w));
cmap=cmocean('topo','pivot',0);
colormap(cmap);
colorbar()
set(gca,'YDir','normal','FontSize',Fsz-1);
view(2)
plot((Eta1+Etam1)./Utr.*cos(Psi/2),(Eta1+Etam1)./ Utr.*sin(Psi/2),'color',e_color,'linewidth',1.8)
plot((Eta1(1)+Etam1(1))/Utr(1)*cos(Psi(1)/2),(Eta1(1)+Etam1(1))/Utr(1)*sin(Psi(1)/2),'rx','linewidth',1.8);
hold off
set(gca,'FontSize',Fsz-1);
xlabel('$\eta \cos(\psi)$','Fontsize',Fsz);
ylabel('$\eta \sin(\psi)$','Fontsize',Fsz);
if Exportinit=='yes'
	savefig(fullfile(img_folder,strcat(flm,'_3W_space2')));
end 

	% Akhmedievstyle phaseplane plot (full NLS minus global phase)
	if IC_case==3
		Ucomplexplane = u/B0.*exp(-1i*ones(length(t),1)*unwrap(angle(utrunc(1,:))));
		figure('name','Akhmedievstyle phaseplane');
		plot(real(Ucomplexplane(nt/2+1,:)),imag(Ucomplexplane(nt/2+1,:)),real(Ucomplexplane(nt/2+1,1)),imag(Ucomplexplane(nt/2+1,1)),'rx','linewidth',1);
		set(gca,'FontSize',Fsz-1);
		xlabel('Re $B(t=0)$','Fontsize',Fsz);
		ylabel('Im $B(t=0)$','Fontsize',Fsz);
		if Exportinit=='yes'
			savefig(fullfile(img_folder,strcat(flm,'_Akhmediev_space')));
		end 
	end	
	%% 3 wave from data (not done)	
% 	three_wave_getsideband(u,freq_dim,t)
	
end

switch Dimensionflag
    case 1    
      %%
   
 %%  Surface 2d plot with bathymetry   
	cmap='viridis';
% 	cmap='viridis5perc';

	plot_Amplitude_Evolution_wbath(K,H,X,t,u,tplot,Lx,xvarstart,xvarstop,cmap);
    if Exportinit=='yes'
       savefig(fullfile(img_folder,strcat(flm,'_2d_evol')));
       saveas(gcf,fullfile(fullfolder,strcat(flm,'_2d_evol','.jpeg')));
	end
	
%%  Fourier 2d plot with bathymetry
	plot_fourier_bathm(K,H,Omega,omega0,uspectrum,omegaxis,X,B0,Lx,xvarstart,xvarstop,'inferno')
    if Exportinit=='yes'
		savefig(fullfile(img_folder,strcat(flm,'_2d_fourier')));
	end
	
	%% 3d envelope and steepness    
    f=plot_3d_envelope(u,t,X,B0,tplot,cmap,Normalized,Fsz);
%%
%     f=plot_3d_steepness(u,t,X,K,tplot);
    %%
	if any(IC_case==[1,2,3])
		beta_tilde=Beta*Cg(1)./Cg;
		alpha_eta=-1.-Alpha.*((omega0)^2)./(beta_tilde.*B0^2);
		eta_MI=2/7.*(1.-alpha_eta);
		figure('name','alpha and eta');
		plot(X,alpha_eta,'k');
		ylabel('$\alpha$','fontsize',Fsz);
		ax2 = gca;
		yyaxis right
		plot(X,eta_MI);
		ylabel('$\eta$','fontsize',Fsz);
	end
    %% tank plot
	if khfin~=kh0
		f=plot_tank_scheme(X,H,xvarstart,xvarstop,Exportinit,pos_gau,Lx,Fsz);
		if Exportinit=='yes'
			savefig(strcat(img_folder,'\',flm,'_bathm'))
			filename_bathm=strcat(fullfolder,'\',flm,'_bathm.txt');
			M=[X' -H'];
			print_bathym_file2(filename_bathm,Input_function,sample_frequency,initial_depth,Factor,M)
		end
	end
%%   
  if Exportinit=='yes'
    pos_error=zeros(length(pos_gau),1);
	figure('name','aprox signals');
	title('Simulated gauge signals','FontSize',Fsz)
	plot_factor=20;
	hold on
	for j=1:length(pos_gau)
	pos_gau(j);
	[ d, ix ] = min( abs( X - pos_gau(j) ) );%find the critical value index
	if j==1
		ix1=ix;
	end
	X(ix);
	pos_error(j)=X(ix)-pos_gau(j);
	ix0 = find(tplot==1,1,'first');
	plot(t/sqrt(g0)-t(ix0)/sqrt(g0),abs(u(:,ix))*plot_factor+X(ix),'-b');    
	plot(t/sqrt(g0)-t(ix0)/sqrt(g0),...
		real(u(:,ix)'.*exp(1i*2*pi*freq_dim.*(t/sqrt(g0)-t(ix0)/sqrt(g0)))).*plot_factor+X(ix),...
	'-r');   
	end  
	hold off 
	set(gca,'YMinorTick','on')
	xlim([0,30]);
	ylim([0,30]);
	grid minor
	ylabel('Distance to wave maker (m)','Interpreter','latex','fontsize',Fsz);
	xlabel('${t-x/C_g}$ (s)','Interpreter','latex','fontsize',Fsz);
    savefig(fullfile(img_folder,strcat(flm,'_sim_gauges')));
    %%
    [ d, ix ] = min( abs( t/sqrt(g0) - Out_time ) );
    fprintf( 'Usanity check: \n output time: %.2f is lower than half total time %.2f \n', Out_time, t(end)/sqrt(g0) );
    middle=round(length(t)*0.5);
    ix=round(abs(middle-ix)*0.5);
      j=3;
    pos_gau(j);
    [ d, ix2 ] = min( abs( X - pos_gau(j) ) );%find the critical value index
    X(ix2);
    input_signal=Factor*real(u(middle-ix:middle+ix,ix2).*exp(1i*(freq_dim*(2*pi)*t(middle-ix:middle+ix)/sqrt(g0)))');
    try
		figure('name','Output sample');
		tp=t(middle-ix:middle+ix)/sqrt(g0);
		[A1,t_sample_try] = resample(input_signal,tp,100);
		t_sample=zeros(length(A1));
		t_sample=t_sample_try(1:length(A1));
		fprintf('sanity check: Wave maker sample frequency shoud be 0.01 \n sample frequency is:  %.3f \n ',t_sample(2)-t_sample(1))
	%     A1=lowpass(A1,1.95);
		if A1(1)>0
			t0 = find(A1<0,1,'first');
			t1 = find(A1<0,1,'last');
		else
			t0 = find(A1>0,1,'first');
			t1 = find(A1>0,1,'last');
		end
		%borders at 0 (a tukey window might work also.)
		A2=[0 A1(t0:t1)' 0]';
		if t1==length(t_sample)
			t_sample(t1+1)=t_sample(t1);
		end
		t2= t_sample(t0-1:t1+1) ;
		M=[t2'-min(t2),A2];
	end
%     csvwrite(filename_wm3,M) ;

    %%
    stokes_amp=0.5*esteep0^2/k0;
    pos_error=zeros(length(pos_gau),1);
    figure('name','aprox signals stks');
    title('Simulated gauge signals with stokes amplification','FontSize',Fsz)
    plot_factor=20;  
    wmod=2*pi*freq_dim;
    stk_sig_env=zeros(length(t),length(pos_gau));
    stk_sig=zeros(length(t),length(pos_gau));

    hold on
    for j=1:length(pos_gau)
    pos_gau(j);
    [ d, ix ] = min( abs( X - pos_gau(j) ) );%find the critical value index
    if j==1
        ix1=ix;
    end
    X(ix);
    pos_error(j)=X(ix)-pos_gau(j);
    ix0 = find(tplot==1,1,'first');
    u2=u(:,ix).*u(:,ix);
    stk_sig_env(:,j)=abs(u(:,ix)'.*exp(1i*wmod.*(t/sqrt(g0)-t(ix0)/sqrt(g0)))+...
        0.5*k0*u2'.*exp(2*1i*wmod.*(t/sqrt(g0)-t(ix0)/sqrt(g0))));
    stk_sig(:,j)=real(u(:,ix)'.*exp(1i*wmod.*(t/sqrt(g0)-t(ix0)/sqrt(g0)))+...
        0.5*k0*u2'.*exp(2*1i*wmod.*(t/sqrt(g0)-t(ix0)/sqrt(g0))));
      
    l1=plot(t/sqrt(g0)-t(ix0)/sqrt(g0),...
        stk_sig_env(:,j)*plot_factor+X(ix),...
        '--k');   %'color',[.7 .2 .0]
    set(l1,'LineWidth',.7)
    
    l2=plot(t/sqrt(g0)-t(ix0)/sqrt(g0),...
        stk_sig(:,j).*plot_factor+X(ix),...
    '-r');   
    set(l2,'LineWidth',.6)

    end  
    hold off 
    set(gca,'YMinorTick','on')
    xlim([0,30]);
    ylim([0,30]);
    grid minor
    ylabel('Distance to wave maker (m)','Interpreter','latex','fontsize',Fsz);
    xlabel('${t-x/C_g}$ (s)','Interpreter','latex','fontsize',Fsz);
    savefig(fullfile(img_folder,strcat(flm,'_sim_gauges_stokes')));
%     filename_bathm=strcat(fullfolder,'\',flm,'_bathm.txt');
%         M=[X' -H'];
%         print_bathym_file2(filename_bathm,Input_function,sample_frequency,initial_depth,Factor,M)
 %%
 try
	figure('name','Fourier signals')
	stokes_amp=0.5*esteep0^2/k0;
    pos_error=zeros(length(pos_gau),1);
    title('Simulated Fourier gauge signals \n with stokes amplification','FontSize',Fsz)
    plot_factor=20;  
    wmod=2*pi*freq_dim;
    hold on
    for j=1:length(pos_gau)
    pos_gau(j);
    [ d, ix ] = min( abs( X - pos_gau(j) ) );%find the critical value index
    if j==1
        ix1=ix;
    end
    X(ix);
    pos_error(j)=X(ix)-pos_gau(j);
    ix0 = find(tplot==1,1,'first');
    fa=1.127/((t(2)-t(1)));%why is it not right???????
    f = fa*(0:(L/2))/L;
%Define the frequency domain f and plot the single-sided amplitude spectrum P1. The amplitudes are not exactly at 0.7 and 1, as expected, because of the added noise.
% On average, longer signals produce better frequency approximations.
    P2 = abs(ifft(stk_sig_env(:,j))/L);
    P1 = P2(1:L/2+1);
    P1(2:end-1) = 2*P1(2:end-1);   
    P3= abs(ifft(stk_sig(:,j)) /L ) ;  
    P4 = P3(1:L/2+1);
    P4(2:end-1) = 2*P4(2:end-1);
    if j==1
      norm_four=max(P4);
    end
%     l1=plot(f*sqrt(g0),1.5*P1/max(P1)+X(ix),'--r');
    l2=plot(f,7*abs(P4)/norm_four+pos_gau(j),'-r');
    set(l2,'LineWidth',.8)
    end  

	hold off
	xlim([0,4])
	% title('Evolution of the freq.','Interpreter','latex')
	xlabel('f (Hz)','Interpreter','latex','Fontsize',Fsz)    
	savefig(fullfile(img_folder,strcat(flm,'_fourier_sims_stk')));
 end

    %%
	plot_simulated_gauge_fourier_stokes(k0,esteep0,freq_dim,omegaxis,pos_gau,X,t,L,u,tplot)
	savefig(fullfile(img_folder,strcat(flm,'_fourier_sims')));
 end  
%%
    case 2

   
%%
    figure;
    Pos = [.14,.15,.79,.72];
    axes('Position',Pos);
    imagesc(t(abs(t)<=windowwidth),X,abs(u(tplot,:).')/B0);
    hold on;
    [~,nucross] = min(abs(H.*K-1.363));
    plot(t(tplot),xvarstart*ones(size(t(tplot))),'k:',t(tplot),xvarstop*ones(size(t(tplot))),'k:',t(tplot),X(nucross)*ones(size(t(tplot))),'m-.','Linewidth',2);
    set(gca,'YDir','normal','FontSize',Fsz-1);
    xlabel('$\tau$ [$m^{1/2}$]','fontsize',Fsz);
    ylabel('$\xi$ [$m$]','fontsize',Fsz);
    %shading interp; colorbar;
    Pos = get(gca,'Position');
    Yl = get(gca,'ylim');
    % overlayed axis with kh changes
    axes('Position',Pos);
    plot(H.*K,X,'r--','linewidth',2);
    xlabel('kh','fontsize',Fsz);
    set(gca,'XAxisLocation','top','GridAlpha',1,'YTickLabel','','Fontsize',Fsz-1,'Ylim',Yl,'Color','none')
   
    f=figure('position',[20 20 500 500]);
    [ d, ix ] = min( abs( K.*H - crit ) );%find the critical value index
    pos1 = [0.05 0.3 0.9 0.65];
    subplot(4,1,[1 3]);
    % hold on 
    imagesc(X,t(tplot),abs(u(tplot,:)) );
     set(gca,'XDir','normal','FontSize',Fsz-1,'xtick',[]);
     ylabel('$\tau$ [$m^{1/2}$] ','fontsize',Fsz);
    % plot(-X0.*ones(max(size(t(tplot)/sqrt(g0)))),t(tplot)/sqrt(g0),'--g')
    % plot(xvarstart.*ones(max(size(t(tplot)/sqrt(g0)))),t(tplot)/sqrt(g0),'color',[0.87,0.74,0.53],'LineStyle','--')
    % plot(xvarstop.*ones(max(size(t(tplot)/sqrt(g0)))),t(tplot)/sqrt(g0),'color',[0.87,0.74,0.53],'LineStyle','--')
    % if K(ix)*H(ix)<(crit+0.02) & K(ix)*H(ix)>(crit-0.02);
    %      plot(X(ix).*ones(max(size(t(tplot)/sqrt(g0)))),t(tplot)/sqrt(g0),'r','LineStyle',':')
    % end
    % hold off
    shading interp;
    hc=colorbar;
    title(hc,'$|B|$');
    ax1 = gca;
    ax1_pos = ax1.OuterPosition ;
    subplot(4,1,4);
    ax2 = gca;
    ax2_pos = ax2.OuterPosition;
    ax2_pos =  [ ax1_pos(1)  ax2_pos(2)  ax1_pos(3)  ax2_pos(4)];
    set(ax2,'OuterPosition',ax2_pos);
    yline=H.*K;
    yline(end) = NaN;
    c = yline;
    patch(X,yline,c,'EdgeColor','interp','LineWidth',1.2,'MarkerFaceColor','flat');
    % colorbar;
    xlabel('$\xi$ [m]','fontsize',Fsz);
    ylabel('kh','fontsize',Fsz);
    set(gca,'XDir','normal','FontSize',Fsz-1,'Ylim',[0.85*min(H.*K), 1.15*max(H.*K)],'ytick',[min(H.*K)  max(H.*K)*1.0000001],'XLim',[0. Lx]);
    ytickformat('%.1f');
          
    figure;
    meshc(t(tplot),X,abs(u(tplot,:).')/B0);
    set(gca,'YDir','normal','FontSize',Fsz-1);
    xlabel('$T$ [$m^{1/2}$]','fontsize',Fsz);
    ylabel('$X$ [$m$]','fontsize',Fsz);
    zlabel('$|B|/B_0$','fontsize',Fsz);
    % shading interp;
    % colormap hot;

    figure;
    meshc(t(tplot),X,abs(u(tplot,:).').*(K.'*ones(size(t(tplot)))));
    set(gca,'YDir','normal','FontSize',Fsz-1);
    xlabel('$T$ [$m^{1/2}$]','fontsize',Fsz);
    ylabel('$X$ [$m$]','fontsize',Fsz);
    zlabel('Steepness','fontsize',Fsz);
    % shading interp;
    colormap hot;

    % spectrum map
    figure;
	set_latex_plots(groot)
    UdBth = -60;
    Usp = 20*log10(abs(uspectrum.')/B0);
    Usp = (Usp>UdBth).*(Usp-UdBth) +  UdBth;
    imagesc(omegaxis,X,Usp);
    hold on;
    plot(omegaxis,xvarstart*ones(size(omegaxis)),'k:',omegaxis,xvarstop*ones(size(omegaxis)),'k:','Linewidth',2);
    set(gca,'YDir','normal','FontSize',Fsz-1);
    xlim([-4*omega0,4*omega0]);
    xlabel('$\omega$ [$m^{-1/2}$]','fontsize',Fsz);
    ylabel('$X$ [$m$]','fontsize',Fsz);
    % shading interp;
    % colormap hot;
    colorbar;
    Pos = get(gca,'Position');
    Yl = get(gca,'ylim');
    axes('Position',Pos);
    plot(H.*K,X,'r--','linewidth',2);
    xlabel('kh','fontsize',Fsz);
    set(gca,'XAxisLocation','top','GridAlpha',1,'YTickLabel','','Fontsize',Fsz-1,'Ylim',Yl,'Color','none')

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% benchmark plots
if any(IC_case==[13])
    f=figure('name','Fourier','position',[20 20 500 500]);
    hold on
    UdBth = -15;
    Usp = 10*log10(abs(uspectrum.').*sqrt(length(u(:,1)).*2)*sqrt(g0)*(1/(2*pi)));
% 	    Usp = 10*log10(abs(uspectrum.')./max(uspectrum,[],"all"));
% 	    Usp = 20*log10(abs(uspectrum.').*sqrt(length(u(:,1)).*2)*sqrt(g0)*(1/(2*pi))./B0);
%     Usp = (Usp>UdBth).*(Usp-UdBth) +  UdBth;
    Usp = Usp-max(Usp,[],"all");
	Usp = (Usp>UdBth).*(Usp-UdBth)+UdBth;

	%     imagesc(omegaxis*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi),X,abs(uspectrum')*sqrt(g0)*(1/(2*pi))/B0);
    imagesc(omegaxis*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi),X,Usp);

	set(gca,'XDir','normal','FontSize',Fsz-1);
%     ylim([-4.5*omega0*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi),4.5*omega0*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi)]);
    xlim([1,3.5]);
	ylim([X(1),X(end)])
    xlabel('$f$ [^Hz]','fontsize',Fsz);
    ylabel('$x$ [m]','fontsize',Fsz);
    ytickformat('%.1f');
	colorMap = jet(256);
	colormap(colorMap);   % Apply the colormap
	hold off
	colorbar()
	
	L0=2*pi./k0;

	f=figure('position',[20 20 500 500]);
	s=pcolor(t(tplot)./sqrt(g0),fliplr(X),flipud(abs(u(tplot,:)')));
	set(s, 'EdgeColor', 'black','LineWidth',1.,'MeshStyle','None');
% 	set(s,'ytick',[0 20 ] ,'xtick',[ 0 min(H.*K)  max(H.*K)*1.0000001 ],'xticklabel',{'hk:' min(H.*K)  max(H.*K)})
% 	set(gca,'ytick',[0 20 40 60],'yticklabel',{0 20 40 60},'xtick',[-16 -8 0 8 16],'xticklabel',{-16 -8 0 8 16})
	hold on
	plot([t(1)./sqrt(g0) t(end)./sqrt(g0)],[4 4],'--g')
	hold off
	xlabel('t[s] ','fontsize',Fsz); 
	ylabel('x[m]  ','fontsize',Fsz);
	xlim([-10,10])
	ylim([0,10])
	colorMap = jet(256);
%  		colorMap = brighten(colorMap,-0.25);
	colormap(colorMap);   % Apply the colormap
	cb=colorbar();
	ylabel(cb, '$|U|$')
	caxis([0 8*10^(-3)])
end
	
		%%
	if IC_case==11
		f=plot_zhang(k0,t,tplot,X,u,B0,T0);
		if Exportinit=='yes'
			savefig(fullfile(img_folder,strcat(flm,'_zhang_paper_plot')));	
		end
	end
%% Plots for solitions
	if any(IC_case==[5,14])
		f=plot_soliton_benchmark(K,H,Omega,omega0,uspectrum,omegaxis,X,B0,Lx,xvarstart,xvarstop,'jet')
		if Exportinit=='yes'
			savefig(fullfile(img_folder,strcat(flm,'_soliton_benchmark_fourier')));	
			saveas(gcf,fullfile(fullfolder,strcat(flm,'_soliton_benchmark_fourier','.jpeg')));
		end
		if Nsol==4
			f=plot_soliton_benchmark_N4(K,H,X,t,u,tplot,Lx,xvarstart,xvarstop,'jet',B0)
			if Exportinit=='yes'
				savefig(fullfile(img_folder,strcat(flm,'_soliton_benchmark_N4')));	
				saveas(gcf,fullfile(fullfolder,strcat(flm,'_soliton_benchmark_N4','.jpeg')));
			end
			f=plot_fourier_solitonN4(K,H,Omega,omega0,uspectrum,omegaxis,X,B0,Lx,xvarstart,xvarstop,cmap);
			if Exportinit=='yes'
				savefig(fullfile(img_folder,strcat(flm,'_soliton_benchmark_fourier_N4')));	
				saveas(gcf,fullfile(fullfolder,strcat(flm,'_soliton_benchmark_fourier_N4','.jpeg')));
			end	
		end
	end
%% Plots for Gaussian/Supergaussian
	if any(IC_case==[7,13])
% 		        Bback = str2double( Pertpar(2));
%         Bpert = str2double( Pertpar(3));
		cmap=viridis;
		f = plot_Amplitude_Evolution_first_last(K,H,X,Bback,Bpert,B0,t,u,tplot,Lx,xvarstart,xvarstop,cmap,Normalized);
		if Exportinit=='yes'
			savefig(fullfile(img_folder,strcat(flm,'envelope_first_last')));	
			saveas(gcf,fullfile(fullfolder,strcat(flm,'_envelope_first_last','.jpeg')));
		end
	end
		%%
	if any(IC_case==[7])
		UdBth=-20;
		f = plot_fourier_first_last(K,H,Omega,omega0,u,omegaxis,X,B0,Lx,xvarstart,xvarstop,UdBth,'inferno');
		if Exportinit=='yes'
			savefig(fullfile(img_folder,strcat(flm,'_fou_first_last')));	
		end
	end
	%%
	if any(IC_case==[7])
	cmap=colorcet('D2'); %D1-D11 Perceptually linear divergent colormaps. I like D6 or D2, since it has none of the usuall colors. 
	f=plot_chirp_Evolution_first_last(K,H,X,Bback,Bpert,B0,t,u,tplot,Lx,xvarstart,xvarstop,cmap);
	if Exportinit=='yes'
			savefig(fullfile(img_folder,strcat(flm,'_chirp_first_last')));	
		end
	end
	%%
	FOD=0;
	%% deprecated plot, it didn't do anything in the end
% 	if any(IC_case==[7])
% 		cmap='viridis';
% 		f=plot_V_shock(K,H,X,Bback,Bpert,B0,t,u,tplot,Lx,xvarstart,xvarstop,cmap,Omega,kh0,FOD,ON_OFF_coef);
% 		if Exportinit=='yes'
% 			savefig(fullfile(img_folder,strcat(flm,'_shock_speed')));	
% 		end
% 	end
	%%
	if IC_case==[7]
		fig_fou_sanscarrier = plot_fourier_sanscarrier(u,omegaxis,Omega,X);
		if Exportinit=='yes'
			savefig(fullfile(img_folder,strcat(flm,'_fourier_sans_carrier')));	
		end
	%%
	plot_contrast_shock(B0,Bback,u,X);
	if Exportinit=='yes'
		savefig(fullfile(img_folder,strcat(flm,'_contrast')));	
	end
		
	end
	
	