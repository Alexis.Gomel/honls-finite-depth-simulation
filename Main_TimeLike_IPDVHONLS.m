%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% Variable depth NLS extended to 4th order
% See Djordjevic & Redekopp ZAMP 1978
% For the HO coefficients, see Sedletsky JETP 2003 (and same author works up to today)
%
% i B_X = -lambda B_TT + nu |B|^2B - i mu B + i (-alpha B_TTT + beta |A|^2
% A_T + gamma A^2 A^*_T) + alpha4 B_TTTT
% all coefficients depend on X in a cumbersome way, see References above 
% (Sedletsky writes the time evolution, here we employ the space evolution, coefficients change: at 4th order they involve 3rd order)
% The small parameter epsilon does NOT appear explicitely
%
% The reasonable normalization is to put g = 1, so that omega (FIXED!!!) is in unit of
% [m^-1/2] and set kh as a function of X (this IS the important parameter to discern focusing/defocusing regimes).
% Now omega^2 = k tanh kh = k sigma. We can derive h or k explicitely and
% find the dependence of cg, lambda, nu, mu. 
% omega = 1 could as well be chosen. Any different omega = pippo corresponds to a
% rescaling of time (multiply by pippo^-1) and lengths (divide by pippo^2)
%
% The solution is made by means of integrating factor or interaction
% picture. 
%
% The place of the shoaling term (mu B) is customary: the linear step is a better choice, since its integration is simpler (it is a log derivative!!!) 
% Only the 3O shoaling is included for now. The 4O is more cumbersome, but
% can be derived by simple energy arguments. 
%
% Andrea ARMAROLI, GAP University of Geneva, 26.02.2019
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all;
clear all;
clc;
% acceleration of gravity(used only in plotting)
addpath(genpath(fullfile('functions','writers')));
addpath(genpath(fullfile('functions','Depth_parameters')));
addpath(genpath(fullfile('functions','import')));
addpath(genpath(fullfile('functions','plot')));
addpath(genpath(fullfile('functions','colormaps')));
addpath(genpath(fullfile('functions','khfunctions')));
addpath(genpath(fullfile('functions','Dialogs')));
addpath(genpath(fullfile('functions','sidebands')));
addpath(genpath(fullfile('functions','RK')));
addpath(genpath(fullfile('functions','jonswap')));

g0 = 9.81;
crit= 1.363;
Normalized=1; %If =1 this makes the plots Normalized to B0. 
[s_color,e_color]=color_language_beforeafter();
[color_s,color_Ns,color_utr,color_main,color_sideband,color_2sideband]=color_language_3wave();
[dispersion_color,nonlinear_color]=color_language_NLScoefs();
Fsz=15;

%Equation coefficients ON/Off. 1 is ON, 2 is OFF
NLS_coef=onoff_dialog();
ON_OFF_coef=onoff_coef(NLS_coef);

DimensionOptions= {'Dimensional','Adimentional'}; 
defSelection = DimensionOptions{2};
Dimensionflag = bttnChoiseDialog(DimensionOptions, 'Input Style', defSelection,...
 'Input Style'); 
fprintf( 'User selection "%s"\n',DimensionOptions{Dimensionflag});

% selecting a function for kh 
switch Dimensionflag
     case 1 
          prompt = {'f [1/s]','L_\xi [m]','L_\tau (time window, periods/soliton length) [ad]',...
            'n_t (log_2)','Number of steps to store N_x','\epsilon (steepness = k_0 a_0) [ad]',...
            'Shoaling ON/OFF','FOD ON/OFF','Export initial function','W-M Factor','Disipation','Output time [s]'};
        dlg_title = 'Parameters'; options.Interpreter = 'tex'; num_lines = 1;
		if ~exist('button_dialog_temp_1.txt', 'dir') %check if dir already exists
			defaultans=importfile_buttondialog_case1('button_dialog_temp_1.txt');
			defaultans=cellstr(defaultans);
			params = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
		else
			defaultans = {'1.5764','50','60','12','250','0.1','1','1','no','1.83','0.00','100'};
%         defaultans = {'1.5764','50','60','14','250','0.1','1','0'};
			params = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
		end
        % normalized frequency omega = omega[s^-1]/g^{1/2}
        Omega_dim = str2double(params(1));
		freq_dim = str2double(params(1));
        Omega = freq_dim*(2*pi)/sqrt(g0);
        Omega = Omega_dim*(2*pi)/sqrt(g0);
        T0_dim=1/freq_dim;
        fprintf('Dimentional frequency: %.3f \n , Adimentional frequency: %.3f \n ', freq_dim, Omega)
        Lx = str2double(params(2));
        Nper = str2double(params(3));
        nt = 2.^str2double(params(4));
        % number of points to save
        Nx = str2double(params(5));
        esteep0 = str2double(params(6));
        shoalingflag = str2double(params(7));
        FODflag = str2double(params(8)); 
        Exportinit =  string(params(9));
        Factor=str2double(params(10));
        dis=str2double(params(11));
        Out_time=str2double(params(12));
		%update dialog options
		print_button_dialog_parameters_case1(freq_dim,Lx,Nper,str2double(params(4)),Nx,esteep0,shoalingflag,FODflag,Exportinit,Factor,dis,Out_time);

         
     case 2
        prompt = {'\Omega [m^{-1/2}]','L_\xi [m]','L_\tau (time window, periods/soliton length) [ad]',...
            'n_t (log_2)','Number of steps to store N_x','\epsilon (steepness = k_0 a_0) [ad]',...
            'Shoaling ON/OFF','FOD ON/OFF','Export initial function','W-M Factor','Disipation','Output time [s]'};
        dlg_title = 'Parameters';
        options.Interpreter = 'tex';
        num_lines = 1;
		if ~exist('button_dialog_temp_2.txt', 'dir') %check if dir already exists
			defaultans=importfile_buttondialog_case1('button_dialog_temp_2.txt');
			defaultans=cellstr(defaultans);
			params = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
		else
			defaultans = {'1','50','10','11','100','0.14','1','0','yes','1','0.0021072','100'};
			params = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
		end
        Omega = str2double(params(1));
        Omega_dim = Omega*sqrt(g0)/(2*pi);
        fprintf('Dimentional frequency: %.3f \n , Adimentional frequency: %.3f \n ', Omega_dim, Omega)
        Lx = str2double(params(2));
        Nper = str2double(params(3));
        nt = 2.^str2double(params(4));
        % number of points to save
        Nx = str2double(params(5));
        esteep0 = str2double(params(6));
        shoalingflag = str2double(params(7));
        FODflag = str2double(params(8));
        Exportinit =  string(params(9));
        Factor=str2double(params(10));
        dis=str2double(params(11));
        Out_time=str2double(params(12));
		%update dialog options
		print_button_dialog_parameters_case1(Omega,Lx,Nper,str2double(params(4)),Nx,esteep0,shoalingflag,FODflag,Exportinit,Factor,dis,Out_time);
end        

%% Bathymetry
% hk is expressed as a function hk = 1.363 is the focusing/defocusing
% turning poin
bathymetryOptions = {'Linear A->B','Erfc A->B','Linear A->B->A','Erfc A->B->A'}; 
defSelection = bathymetryOptions{2};
bathymetryflag = bttnChoiseDialog(bathymetryOptions, 'Bathymetry profile', defSelection,...
 'Bathymetry profile'); 
fprintf( 'User selection "%s"\n', bathymetryOptions{bathymetryflag});

% selecting a function for kh 
switch bathymetryflag
    case 1
        khfunsel =  @khfun4;
    case 2
        khfunsel =  @khfun1;
    case 3
        khfunsel =  @khfun3;
    case 4
        khfunsel =  @khfun2;
end

% bathymetry params
% hk = 1.363 is the focusing/defocusing turning point
prompt = {'hk_{0}','hk_{fin}','\xi_1','\xi_2','\xi_{slope}'};
dlg_title = 'Solver parameters';options.Interpreter = 'tex';num_lines = 1;
if ~exist('kh_dialog_temp.txt', 'dir') %check if dir already exists
	defaultans=importfile_buttondialog_kh('kh_dialog_temp.txt');
	defaultans=cellstr(defaultans);
	bathymetrypar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
else
	defaultans = {'2.','3.5','15','20','20'};
	bathymetrypar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
end

kh0 = str2double(bathymetrypar(1)); khfin = str2double(bathymetrypar(2));
xvarstart = str2double(bathymetrypar(3)); xvarstop = str2double(bathymetrypar(4)); %[m]
xslope = str2double(bathymetrypar(5));

print_kh_dialog_parameters(kh0,khfin,xvarstart,xvarstop,xslope)
if bathymetryflag == 1, warning('Slope is not used!'); end

% hk is expressed as a function hk = 1.363 is the focusing/defocusing
% turning point
% kh0 = 2.5;
% khfin = 3.45;
% xvarstart = 480; %[m]
% xvarstop = 520; %[m]
% xslope = 10;

% selecting a function for kh
% khfunsel =  @khfun4;

%% solver params;
prompt = {'initial step','minimum step','tolerance'};
dlg_title = 'Solver parameters';options.Interpreter = 'tex';num_lines = 1;
defaultans = {'0.01','1e-7','1e-9'};
solvpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
hx0 = str2double(solvpar(1)); hxmin = str2double(solvpar(2)); tol = str2double(solvpar(3));
% Lx = 1000; % [m] % number of points to save % Nx = 250; % hx0 = 0.01; % hxmin = 1e-6; % tol = 1e-10;

%% definition of X lattice
if hx0<Lx./Nx
    hx0 = Lx./Nx;
end
Hx = Lx./Nx;
X = 0:Hx:Lx;
% nt = 2^12;
% Nper = 20;

%% compute the NLS parameters 
% DO NOT DISPLACE IT, it is necessary to estimate MI, soliton, DSW params
[H,K,Sigma,Cg,Cp,Alpha,Beta,Alpha3,Alpha4,Beta_21,Beta_22,Mu,Muintexp,Dh,H3] = depthdependent_HONLS_TimeLike(Omega,khfunsel(X,kh0,khfin,xvarstart,xvarstop,xslope),FODflag,ON_OFF_coef);
Regime=Beta./Alpha;

f=plot_params_TL(X,K,H,Mu,Cp,Cg,Muintexp,Regime,Alpha3,Alpha4,Beta_21,Beta_22,Fsz);

% ...and their initial values
[h0,k0,sigma0,cg0,cp0,alpha0,beta0,alpha30,alpha40,beta_210,beta_220,mu0,muintexp0,dh0,H30] = depthdependent_HONLS_TimeLike(Omega,kh0,FODflag,ON_OFF_coef);
regime=beta0/alpha0;
% Reference amplitude
B0 = esteep0./k0;
% Normalization scales
Tnl = sqrt(abs(2*alpha0./beta0))./B0;
Lnl = 1./abs(beta0)./B0^2;

Tnl_ev = sqrt(abs(2*Alpha./Beta))./B0;
Lnl_ev = 1./abs(Beta)./B0^2;
%% excitation type
% inputOptions = {'Shot noise like perturbed plane wave','Harmonically perturbed plane wave','Akhmediev Breather',...
%     'Peregrine Breather','Bright Soliton','Dark Soliton','Gaussian on top of a background','Sedletsky bright soliton',...
%     'Steplike initial condition (dam break)'}; 
inputOptions ={'Shot noise like perturbed plane wave','Harmonically perturbed plane wave','Akhmediev Breather',...
    'Peregrine Breather','Bright Soliton','Dark Soliton','Gaussian on top of a background',...
    'Sedletsky bright soliton','Steplike initial condition (dam break)','Amin noise','Benchmark Gaussian','3 wave gaussian','Jonswap','Benchmark soliton','Kuznetsov-Ma','HPPW explicit'}; 

defSelection = inputOptions{12};bttn_display=[6,3];
excflag = bttnChoiseDialog(inputOptions, 'Type of initial conditions', defSelection,...
 'Type of initial conditions',bttn_display); 
fprintf( 'User selection "%s"\n', inputOptions{excflag});

% At each type of initial condition, different parameters are associated.
% Moveover, omega0 (on which the time window depends) and the plotting
% limit windowwidth are different.
IC_case=0;
switch excflag
    case 1
		IC_case=1;
        if regime> 0 ,  warning('Plane waves are stable'), end
        prompt = {sprintf('Noise fraction, B_0 = %g',B0)};
        dlg_title = 'Noisy Plane Wave';  options.Interpreter = 'tex'; num_lines = 1;
        defaultans = {'1e-4'};        
        NPWpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        save_params=NPWpar;
        etanoise =  str2double( NPWpar(1));
        eta0 = 1-etanoise;
        U0 = str2double( NPWpar(1));
        variance = etanoise/nt*2/pi;
        omega0 = 1/Tnl*sqrt(2);
        windowwidth = 10*Tnl;
        
    case 2
		IC_case=2;
	    if regime> 0 ,  warning('Plane waves are stable'), end
        prompt = {'Total initial energy','Sideband fraction','Sideband unbalance','Relative phase (\phi_1+\phi_{-1})/2-\phi_0','\Omega/\Omega_{Max}'};
        dlg_title = 'Harmonically perturbed Plane Wave'; options.Interpreter = 'tex';  num_lines = 1;
%         defaultans = {'1','1e-2','0e-3','pi/2','1'};      
		if exist('button_IC2_temp.txt', 'dir') %check if dir already exists
			defaultans=importfile_IC_case2('button_IC2_temp.txt');
			defaultans=cellstr(defaultans);
			PPWpar  = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
		else
			defaultans = {'1','1e-2','0e-3','pi/2','1'};
%         defaultans = {'1.5764','50','60','14','250','0.1','1','0'};
			PPWpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
		end
		
        save_params=PPWpar;
        U0 = str2double( PPWpar(1));
        eta0 = U0 - str2double(PPWpar(2));
        unb = str2double(PPWpar(3));
        psi0 = eval(char(PPWpar(4)));
		omega_max=sqrt(abs(beta0./alpha0)).*B0;
        omega0 = omega_max*str2double(PPWpar(5));%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        windowwidth = 10*Tnl;
        eta1 = (U0-eta0+unb)/2;
        etam1 = (U0-eta0-unb)/2;       
        if eta1*etam1 < 0, error('The chosen unbalance is to big or negative intensity are chosen!'); end
        print_dialog_IC_case2(U0,str2double(PPWpar(2)),unb,psi0,str2double(PPWpar(5)));
		
    case 3
        IC_case=3;
		if regime> 0 ,  warning('Plane waves are stable'), end
        prompt = {'a','Initial point \xi_0'};
        dlg_title = 'Akhmediev Breather'; options.Interpreter = 'tex'; num_lines = 1;
        defaultans = {'0.25','-500'};        
        ABpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        save_params=ABpar;
        aAB = str2double( ABpar(1));
        X0 = str2double( ABpar(2));
        bAB = sqrt(8*aAB*(1-2*aAB));
        omegaAB = 2*sqrt(1-2*aAB);
        omega0 = omegaAB/Tnl;
        windowwidth = 10*Tnl;
        
    case 4
        IC_case=4;
		if regime> 0 ,  warning('Plane waves are stable'), end
        prompt = {'Initial point \xi_0'};
        dlg_title = 'Peregrine Breather'; options.Interpreter = 'tex'; num_lines = 1;
        defaultans = {'-500'};        
        Perpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);          
        save_params=Perpar;
        X0 = str2double( Perpar(1));        
        omega0 = 2*pi/Tnl;
        windowwidth = 20*Tnl;
        if Nper<20, warning('The time window is too narrow!'), end  
        
        
    case 5
        IC_case=5;
	    if regime> 0 ,  warning('Bright soliton is not a solution'), end
        prompt = {'Soliton width','Soliton number'};
        dlg_title = 'N-soliton';  options.Interpreter = 'tex'; num_lines = 1;
        defaultans = {'1','1'};    
        Solpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        save_params=Solpar;
        tau0 = str2double(Solpar(1));
        Nsol = str2double( Solpar(2));      
        omega0 = 2*pi/tau0/Tnl;
        windowwidth = 20*Tnl;
        if Nper<20, warning('The time window is too narrow!'), end

     case 6
        IC_case=6;
	    if regime< 0 ,  warning('Dark soliton is not a solution'), end
        prompt = {'Soliton width','Soliton number'};
        dlg_title = 'N-soliton';  options.Interpreter = 'tex'; num_lines = 1;
        defaultans = {'1','1'};    
        Solpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        save_params=Solpar;
        tau0 = str2double(Solpar(1));        
        Nsol = str2double(Solpar(2));     
        omega0 = 2*pi/tau0/Tnl;
        windowwidth = 20*Tnl;      
        if Nper<50 , warning('The time window is too narrow!'),end
    
	case 7

		IC_case=7;
	    if regime< 0 ,  warning('The dynamics maybe unstable in the defocusing regime!'), end
        if Nper<50, warning('The time window is too narrow!'),  end
        prompt = {'Perturbation width (in Tnl units)','Background rel. amplitude','Perturbation rel. amplitude','Supergaussian number (=1 Gaussian)'};
        dlg_title = 'Gaussian on a background'; options.Interpreter = 'tex'; num_lines = 1;
        defaultans = {'1','0.5','1','1'};   
        Pertpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        save_params=Pertpar;
        tau0 = str2double(Pertpar(1));           
        Bback = str2double( Pertpar(2));
        Bpert = str2double( Pertpar(3));
        MSG = str2double( Pertpar(4));     
        omega0 = 2*pi/tau0/Tnl;
        windowwidth = (19+tau0)*Tnl; %this is for the plot.
        
    case 8
		IC_case=8;
	    if regime< 0 ,  warning('This is a special bright soliton'), end
        prompt = {'t_0'};
        dlg_title = 'Sedletsky 1-soliton'; options.Interpreter = 'tex'; num_lines = 1;
        defaultans = {'-200'};   
        Solpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
                save_params=Solpar;
        t0 = str2double(Solpar(1));             
        omega0 = 2*pi/Tnl;
        windowwidth = 60*Tnl;      
        if Nper<50, warning('The time window is too narrow!'), end
        
                
    case 9
		IC_case=9;
        % Steplike profile (dam break)
	    if regime< 0 ,  warning('The dynamics maybe unstable in the defocusing regime!'), end
        if Nper<50, warning('The time window is too narrow!'), end
        prompt = {'Perturbation offset (in Tnl units)','Background rel. amplitude (in units of \varepsilon)','Perturbation rel. amplitude','Supergaussian number (=1 Gaussian)'};
        dlg_title = 'Steplike for dam break';  options.Interpreter = 'tex'; num_lines = 1;
        defaultans = {'0','1','5','100'};      
        Pertpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        save_params=Pertpar;
        toff = str2double(Pertpar(1));           
        Bback = str2double( Pertpar(2));
        Bpert = str2double( Pertpar(3));
        MSG = str2double( Pertpar(4));   
        omega0 = 2*pi/Tnl;
        windowwidth = 50*Tnl;
        case 9
        IC_case=9;
        % Steplike profile (dam break)
	    if regime< 0 ,  warning('The dynamics maybe unstable in the defocusing regime!'), end
        if Nper<50, warning('The time window is too narrow!'), end
        prompt = {'Perturbation offset (in Tnl units)','Background rel. amplitude (in units of \varepsilon)','Perturbation rel. amplitude','Supergaussian number (=1 Gaussian)'};
        dlg_title = 'Steplike for dam break';  options.Interpreter = 'tex'; num_lines = 1;
        defaultans = {'0','1','5','100'};      
        Pertpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        save_params=Pertpar;
        toff = str2double(Pertpar(1));           
        Bback = str2double( Pertpar(2));
        Bpert = str2double( Pertpar(3));
        MSG = str2double( Pertpar(4));   
        omega0 = 2*pi/Tnl;
        windowwidth = 50*Tnl;
		
		
   case 10
	    IC_case=10;
        % Real noise pert plane wave
	    if regime< 0 ,  warning('The dynamics maybe unstable in the defocusing regime!'), end
		if Nper<50, warning('The time window is too narrow!'), end
        prompt = {'Noise fraction'};
		dlg_title = 'Noisy plane wave perturbated only on the amplitude';  options.Interpreter = 'tex';
        num_lines = 1;
        defaultans = {'0.01'};   
        Pertpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        save_params=Pertpar;
        frac = str2double(Pertpar(1));                 
        omega0 = 2*pi/Tnl;
        windowwidth = 50*Tnl;
		
	case 11
		IC_case=11;
        % gaussian zhang paper
		%steepness = 0.1
		%T0=7s; f0=0.14286
		%a0=1.75m, kh=..
		%from -16T0 to 16T0
        if regime< 0 ,  warning('The dynamics maybe unstable in the defocusing regime!'), end
		if Nper<50, warning('The time window is too narrow!'), end
        prompt = {'m'};
        dlg_title = 'm-gaussian';  options.Interpreter = 'tex'; num_lines = 1;
        defaultans = {'4'};      
        Pertpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        save_params=Pertpar;
        m_gaus = str2double(Pertpar(1));           
        omega0=Omega;	
        windowwidth = 20*Tnl;
		
	case 12
		IC_case=12;
	    if regime> 0 ,  warning('Plane waves are stable'), end
		prompt = {'Total initial energy','Sideband fraction','Sideband unbalance',...
			'Relative phase (\phi_1+\phi_{-1})/2-\phi_0','\Omega/\Omega_Max','\sigma_0','\sigma_m'};
		dlg_title = 'Harmonically perturbed Plane Wave';  options.Interpreter = 'tex'; um_lines = 1;
		defaultans = {'1','0e-10','0e-30','pi/2','1.2','0.02','0.2'};
		PPWpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
		save_params=PPWpar;
		U0 = str2double( PPWpar(1));
		eta0 = U0 - str2double(PPWpar(2));
		alpha = str2double(PPWpar(3));
		psi0 = (eval(char(PPWpar(4))));
		omega0 = 1/Tnl*sqrt(2)*str2double(PPWpar(5));
		windowwidth = 10*Tnl;
		eta1 = (U0-eta0+alpha)/2;
		etam1 = (U0-eta0-alpha)/2;    
		sigma_0=str2double( PPWpar(6));
		sigma_m=str2double( PPWpar(7));
		if eta1*etam1 < 0, error('The chosen unbalance is to big or negative intensity are chosen!'),	end
		
	case 13
		IC_case=13;
	    if regime> 0 ,  warning('Plane waves are stable'), end
		prompt = {'gamma'};
		dlg_title = 'Jonswap'; options.Interpreter = 'tex';	num_lines = 1;
		defaultans = {'2.5'};
		PPWpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
		save_params=PPWpar;
% 		Hw=str2double( PPWpar(1) ); % wave height significant (m)
		Hw=B0;
		gama=str2double(PPWpar(1)); % Peak period (s)
		fom=freq_dim;
		Tw=1/fom;
		omega0=Omega;
% 		T0 = 2*pi/omega0;
		T0 = Tw;
		Lt = T0*Nper;
		skl=1;
% 		freq_jon=32;
        windowwidth = 8*Tnl;
		if Lt/nt > 2*Tw/10,	warning('time step too small'),	end
		
	case 14
		IC_case=14;
		if regime> 0 ,  warning('Plane waves are stable'), end
		prompt = {'N'};
		dlg_title = 'Benchmark soliton'; options.Interpreter = 'tex'; num_lines = 1;
		defaultans = {'2'};
		PPWpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
		save_params=PPWpar;
		N_sol=str2double( PPWpar(1)); % wave height significant (m)	
		%for N=2: a0=5mm; esteep=0.16/2=0.08, freq_dim=2,h=1.5m
		%kh=24.146
		%for N=3: a0=2mm, esteep=0.12/3=0.04,freq_dim=2,h=1.5m
		%for N=4:a0=1mm; esteep=0.16/4=0.04, L=10m or freq_dim=2, h=1.5m	
		fom=Omega/(2*pi);
		windowwidth = Nper*Tnl/8;
		omega0 = sqrt(abs(beta0/alpha0)).*B0;
		
	case 15
		IC_case=15;
	    if regime> 0 ,  warning('Plane waves are stable'), end
        prompt = {'a','Initial point \xi_0'};
        dlg_title = 'Kuznetsov-Ma soliton (a>0.5)'; options.Interpreter = 'tex'; num_lines = 1;
        defaultans = {'1','0'};      
        ABpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        save_params=ABpar;
        aAB = str2double( ABpar(1));
        X0 = str2double( ABpar(2));
        bAB = sqrt(8*aAB*(1-2*aAB));
        omegaAB = 2*sqrt(1-2*aAB);
        omega0 = 2*pi/Tnl;
        windowwidth = 20*Tnl;
        if Nper<20, warning('The time window is too narrow!'),   end
           
	 case 16
		IC_case=2;
	    if regime> 0 ,  warning('Plane waves are stable'), end
        prompt = {'Total initial energy','Sideband fraction','Sideband unbalance','Relative phase (\phi_1+\phi_{-1})/2-\phi_0','\Omega'};
        dlg_title = 'Harmonically perturbed Plane Wave'; options.Interpreter = 'tex';  num_lines = 1;
%         defaultans = {'1','1e-2','0e-3','pi/2','1'};      
		if exist('button_IC2_temp.txt', 'dir') %check if dir already exists
			defaultans=importfile_IC_case2('button_IC2_temp.txt');
			defaultans=cellstr(defaultans);
			PPWpar  = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
		else
			defaultans = {'1','1e-2','0e-3','pi/4','0.28256'};
%         defaultans = {'1.5764','50','60','14','250','0.1','1','0'};
			PPWpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
		end
		
        save_params=PPWpar;
        U0 = str2double( PPWpar(1));
        eta0 = U0 - str2double(PPWpar(2));
        unb = str2double(PPWpar(3));
        psi0 = eval(char(PPWpar(4)));
		omega_max=sqrt(abs(beta0./alpha0)).*B0;
        omega0 = str2double(PPWpar(5));%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        windowwidth = 10*Tnl;
        eta1 = (U0-eta0+unb)/2;
        etam1 = (U0-eta0-unb)/2;       
        if eta1*etam1 < 0, error('The chosen unbalance is to big or negative intensity are chosen!'); end
        print_dialog_IC_case2(U0,str2double(PPWpar(2)),unb,psi0,str2double(PPWpar(5)));
		
		
    otherwise 
        error('No valid initial condition option');
end
save_params_names=prompt;%TO BE USED TO SAVE PARAMETERS TO FILE. 

%% definition of T lattice
% given the type of IC, we define omega0 and the time lattice
% omega0 is the characteristic  angular frequency dependent on the chosen initial
% conditions

% omega0 = sqrt(abs(beta0/alpha0)).*B0;
% omega0 = 0.1;
T0 = 2*pi/omega0;
% calculate the time window span
Lt = T0*Nper;
t = linspace(-Lt/2,Lt/2,nt+1);
t = t(1:end-1);
ht = t(2)-t(1);
omegaxis = 2*pi.*linspace(-1/ht/2,1/ht/2,nt+1); omegaxis = omegaxis(1:end-1);
%FFT phase shifts
kt1 = linspace(0,nt/2 + 1,nt/2)';
kt2 = linspace(-nt/2,-1,nt/2)';
kt = ((2*pi/ht/nt)*[kt1; kt2]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% field initialization
u = zeros(nt,Nx+1);
uspectrum = u;
saveflag = zeros (Nx+1,1);
N = saveflag;
P = N;

%% Initial conditions

switch excflag
    case 1 % MI from noise    % variance = 1e-2;
        u0 = B0.*(sqrt(eta0) + sqrt(variance).*randn(size(t)) + 1i.*sqrt(variance).*randn(size(t)));

    case 2   % Harmonically perturbed plane wave
        u0 = B0.*(sqrt(eta0) + sqrt(etam1).*exp(1i*omega0.*t).*exp(1i.*psi0) + sqrt(eta1).*exp(-1i*omega0.*t).*exp(1i.*psi0)).*exp(-0*t.^2./(30*T0.^2));
    
    case 3  % Akhmediev Breather 
		%aAB = 0.1;  bAB = sqrt(8*aAB*(1-2*aAB));   omegaAB = 2*sqrt(1-2*aAB);
        u0 = B0.*(1 + (omegaAB^2/2.*cosh(bAB*X0/Lnl) - 1i*bAB*sinh(bAB.*X0/Lnl))./(sqrt(2*aAB)*cos(omegaAB*t/Tnl) - cosh(bAB.*X0/Lnl))).*exp(1i*X0/Lnl);    
    
    case 4  % Peregrine Soliton
        % TP = sqrt(abs(2*alpha0./beta0))./B0; Lnl = 1./beta0/B0^2; X0 = -500;
        u0 = B0.*(1-4.*(1-2i.*X0./Lnl)./(1+4*(t./Tnl).^2 + 4*(X0/Lnl)^2)).*exp(-1i*X0/Lnl);
  
	case 5   % Bright Soliton
        % soliton order    % Nsol = 3;
        u0 = Nsol*(B0/tau0).*sech((B0/tau0)*sqrt(abs(beta0/2/alpha0)).*t);
		Bback=0;
		Bpert=Nsol*(B0/tau0);
    case 6  % Dark Soliton
        u0 = Nsol*B0*tanh(B0*sqrt(abs(beta0/2/alpha0)).*t);
    
    case 7   % Gaussian on top of a background 
        u0 = B0*(Bback + Bpert*exp(-(t./Tnl/tau0).^(2*MSG)));
                
    case 8 % Bright soliton in defocusing regime, see Gandzha&Sedletsky PLA
        % 381 (2017)
        S = -(beta0+gamma0)./(6*alpha0);
        K0 = B0*sqrt(S);
        omegasol0 = (-alpha0*(beta0+gamma0) + 3*alpha0*beta0)/(6*alpha0*gamma0);        
        V = K0^2*alpha0 - 2.*alpha0*omegasol0 - 3.*alpha0*omegasol0^2;
        u0 = B0.*sech(K0.*(t-t0)).*exp(-1i*t*omegasol0);
        
    case 9     %Dam break
        u0 = B0*(Bback + Bpert*exp(-((t+Lt/2-toff)./Lt*2).^(2*MSG))).*exp(-(t./2/Lt*5).^50);

	case 10
        % Real noise perturbated plane wave
        u0 = B0.*(frac.*randn(size(t)));
		
    case 11
        % zhang  paper gaussian
        u0 = B0*(exp(-(t./(m_gaus.*T0)).^(2)));
		windowwidth=16*T0;
	
	case 12 %3wave gaussians
		gaus_0=sqrt(eta0).*exp(-(pi.*t.*(2.*sigma_0.^2)).^2);
		gaus_1=sqrt(eta1).*exp(-(pi.*t.*(2.*sigma_m.^2)).^2);
		gaus_m=sqrt(etam1).*exp(-(pi.*t.*(2.*sigma_m.^2)).^2);
		u0 = B0.*(gaus_0 + gaus_1.*exp(1i*omega0.*t).*exp(1i.*psi0) + gaus_m.*exp(-1i*omega0.*t).*exp(1i.*psi0)).*exp(-0*t.^2./(30*T0.^2));
	
	case 13
		wh=cobagelombang4(Hw,Tw*sqrt(g0),gama,t,B0,skl);
%   		wh=real(wh.*exp(-1i*Omega*t));
		u0=hilbert((wh-mean(wh)).*tukeywin(length(t),0.1)');
		u0_mean=mean(abs(u0));
% 		u0=u0/u0_mean*B0;
% 	    wh=wh/u0_mean*B0;
		figure()
		hold on
		plot(t,abs(u0),'-r','linewidth',2)
		plot(t,(wh),'-b','linewidth',0.5)
		hold off
		
	case 14
		%Benchmark soliton	;u0 = Nsol*(B0/tau0).*sech((B0/tau0)*sqrt(abs(beta0/2/alpha0)).*t);
		u0=N_sol.*B0.*sech(t);

	case 15
        % Kuznetsov-Ma soliton
        u0 = B0.*(1 + (omegaAB^2/2.*cosh(bAB*X0/Lnl) - 1i*bAB.*sinh(bAB.*X0/Lnl))./(sqrt(2*aAB).*cos(omegaAB*t/Tnl) - cosh(bAB.*X0/Lnl))).*exp(1i*X0./Lnl);    
		
    case 16   % Harmonically perturbed plane wave
        u0 = B0.*(sqrt(eta0) + sqrt(etam1).*exp(1i*omega0.*t).*exp(1i.*psi0) + sqrt(eta1).*exp(-1i*omega0.*t).*exp(1i.*psi0)).*exp(-0*t.^2./(30*T0.^2));
   
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Initialization of computation
tplot = abs(t)<=windowwidth;
tclean = abs(t)<=2*windowwidth; %array cleaning the borders to have better analysis. this is approximate

pind = 1;
u(:,1) = (u0);
ucurr = u(:,1);
UdBth = -30;
ucurr_spec=fftshift(ifft(ucurr,[],1),1);
uspectrum(:,pind) = ucurr_spec;
Uksp2_0 = 10*log10(abs(ucurr_spec./B0));
Uksp2_0 = (Uksp2_0>UdBth).*(Uksp2_0-UdBth) +  UdBth;

Deltat=t(end)-t(1);
N(pind) = 1./(Deltat).*trapz(t,cg0*abs(ucurr).^2);
P(pind) = 1./(Deltat).*real(trapz(t,cg0*0.5i.*(fft(-1i.*kt.*ifft(ucurr)).*conj(ucurr)-fft(-1i.*kt.*ifft(conj(ucurr))).*ucurr)));
x = 0;
hx = hx0;
pind = 2;
count = 0;
B_ref=max(abs(ucurr));
%%
carrier_phase=zeros(Nx+1,1);
[ d_car, ix_car ] = min( abs( max(ucurr_spec) - ucurr_spec ) );%find the critical value index
ix_car;
carrier_freqs=omegaxis(ix_car);
carrier_fourier=ucurr_spec(ix_car);
carrier_phase(1)=unwrap(angle(ucurr_spec(ix_car)));

%% Computation
f=figure('name','First last','position',[20 15 900 600]);
set_latex_plots(groot)
filename = 'NLSAB_TimeLike.gif';
set(0,'defaulttextfontsize',Fsz);
dis=0;
while x<Lx
        err = tol*10;
        numberofrejstep = 0;
        while err>tol
            [~,k1,~,cg1,~,alpha1,beta1,alpha31,alpha41,beta_211,beta_221,~,muintexp1,dh1,H31] = depthdependent_HONLS_TimeLike(Omega,khfunsel(x+hx/2,kh0,khfin,xvarstart,xvarstop,xslope),FODflag,ON_OFF_coef);
            [~,k2,~,cg2,~,alpha2,beta2,alpha32,alpha42,beta_212,beta_222,~,muintexp2,dh2,H32] = depthdependent_HONLS_TimeLike(Omega,khfunsel(x+hx,kh0,khfin,xvarstart,xvarstop,xslope),FODflag,ON_OFF_coef);
            [ucurrnew,err] = DV4ORK34_TL(hx,ucurr,kt,alpha0,alpha1,alpha2,beta0,beta1,beta2,alpha30,alpha31,alpha32,alpha40,alpha41,alpha42,beta_210,beta_211,beta_212,beta_220,beta_221,beta_222,muintexp0,muintexp1,muintexp2,H30,H31,H32,shoalingflag,dis);           
            if (err<tol)
                x = x + hx;
                ucurr = ucurrnew;
                count = count + 1;
                step(count) = hx;
            else
                numberofrejstep =  numberofrejstep + 1;
                if numberofrejstep > 10
                    fprintf('10!\n');
                end
            end
            hx = min(max(0.5,min(2,.95*(tol./err).^.25)).*hx,Hx);
            if hx < hxmin 
                error('Integration step too small');
            end
            if isnan(err)
                error('Diverges!');
            end
        end
    if isnan(ucurr)
        error('diverges!!!');
    end
    k0 = k2;
    alpha0 = alpha2;
    muintexp0 = muintexp2;
    beta0 = beta2;
    cg0 = cg2;
    alpha30 = alpha32;
    alpha40 = alpha42;
    beta_210 = beta_212;
    beta_220 = beta_222;
    H30=H32;
    %     hz0 = hz;
    if x>=X(pind) && saveflag(pind) == 0;
        if max(abs(ucurr))*k2>0.4
            warning('Wave breaking probably occurs!');
        end
        saveflag(pind) = 1;   
        N(pind) = 1./(Deltat).*trapz(t,cg0*abs(ucurr).^2);
        P(pind) = 1./(Deltat).*real(trapz(t,cg0*0.5i.*(fft(-1i.*kt.*ifft(ucurr)).*conj(ucurr)-fft(-1i.*kt.*ifft(conj(ucurr))).*ucurr)));
%         NQ(pind) = trapz(t,(abs(ucurr).^2-1).^2);
%         fprintf('Writing the field %d, X = %g, N = %g, P = %g\n\n', pind,x, N(pind), P(pind)); 
            u(:,pind) = ucurr;
            % computing spectrum
            uspectrum(:,pind) = ifftshift(ifft(ucurr,[],1),1);
   			ucurr_spec = ifftshift(ifft(ucurr,[],1),1);

			X(pind) = x;			
			Uksp2 = 10*log10(abs(ucurr_spec./B0));
			Uksp2 = (Uksp2>UdBth).*(Uksp2-UdBth) +  UdBth;
			carrier_fourier=ucurr_spec(ix_car);
			carrier_phase(pind)=unwrap(angle(ucurr_spec(ix_car)));
			
			subplot(131)
				plot(t(tplot),abs(ucurr(tplot)),t(tplot),abs(u0(tplot)),'Linewidth',1.5);
	%             xlim([-windowwidth,windowwidth]);
				ylim([0,3.1*B_ref]);
				xlim([min(t(tplot)),max(t(tplot))]);
				xlabel('$T [m^{1/2}]$','fontsize',Fsz); 
				set(gca,'fontsize',Fsz-1);
				title(strcat('kh=',num2str(kh0,'%.2f'),', $$\epsilon =$$',num2str(max(abs(ucurr(tplot))).*k0,'%.2f')),'fontsize',Fsz-5);
  
% 			subplot(132)
% 				plot(t(tplot),unwrap(angle(ucurr(tplot)))/pi-carrier_phase(pind)/pi,t(tplot),unwrap(angle(u0(tplot)))/pi-carrier_phase(1)/pi,'Linewidth',1.5);
% 				xlabel('$T [m^{1/2}]$','fontsize',Fsz);
% 				xlim([min(t(tplot)),max(t(tplot))]);
% 	%             xlim([-windowwidth,windowwidth]);
% 				title(strcat(num2str(100.*x./Lx,'%.1f'),' \% Sim  ',', $\xi =$ ',num2str(x,'%.1f'),'m'),'fontsize',Fsz-5);
% 				set(gca,'fontsize',Fsz-1);         
           
			subplot(132)
			plot(omegaxis,Uksp2,omegaxis,Uksp2_0,'Linewidth',1.5);
			xlabel('$\delta \Omega [m^{-1/2}]$','fontsize',Fsz);
			xlim([-5*sqrt(2)*omega0,5*sqrt(2)*omega0]);
			set(gca,'fontsize',Fsz-1);    
			title(strcat(num2str(100.*x./Lx,'%.1f'),' \% Sim  ',', $\xi =$ ',num2str(x,'%.1f'),'m'),'fontsize',Fsz-5);

			subplot(433)
			% plot(kx,abs(Hhat3p.*1i.*ucurr.*fft(1./(tanh(h0.*abs(kx+k0))).*ifft(abs(ucurr).^2))))
			plot(t,H32.*abs(1i.*ucurr.*fft(abs(kt).*ifft(abs(ucurr).^2))),'b','DisplayName','$H_3 U H[\partial_x |U|^2]$')
	% 		xlim([-plot_periods.*L0,plot_periods.*L0])
			xlim([-windowwidth,windowwidth]);
			title('$H_3 U H[\partial_x |U|^2]$','interpreter','latex','fontsize',Fsz-5);

			subplot(436)	
			plot(t,beta_212.*abs(abs(ucurr).^2.*fft(-1i.*kt.*ifft(ucurr))),'b','DisplayName','$$\beta_{21}|U|^2\partial_x U$$')
	% 		xlim([-plot_periods.*L0,plot_periods.*L0])
			xlim([-windowwidth,windowwidth]);
	% 		legend()
			title('$ \beta_{21}|U|^2\partial_x U$','interpreter','latex','fontsize',Fsz-5);

			subplot(439)		
			plot(t,beta_222.*abs(ucurr.^2.*fft(-1i.*kt.*ifft(conj(ucurr)))),'b','DisplayName','$\beta_{22}U^2\partial_x U^*$')
	% 		xlim([-plot_periods.*L0,plot_periods.*L0])
			xlim([-windowwidth,windowwidth]);
	% 		xlabel('\xi')
			title('$\beta_{22}U^2\partial_x U^*$','fontsize',Fsz-5);
		
% 			subplot(4,3,12)		
% 			plot(t,gamma.*ucurr.^2.*fft(-1i.*omega.*ifft(conj(ucurr))),'b','DisplayName','$\beta_{22}U^2\partial_x U^*$')
% 			xlim([-windowwidth,windowwidth]);
% 			xlabel('$\tau$')
% 			title(strcat('alpha_3 \partial^3_x U $',num2str(bhat_D.*H/Hhat3)),'interpreter','latex','fontsize',Fsz-5);
			  
		drawnow
		sgtitle(dlg_title)
        F = getframe(gcf);
        im = frame2im(F);
        [imind,cm] = rgb2ind(im,256);
     %   if pind == 2;        
    %        imwrite(imind,cm,filename,'gif', 'DelayTime',.1,'Loopcount',inf);          
     %   else          
     %       imwrite(imind,cm,filename,'gif','WriteMode','append','DelayTime',.1);           
     %   end
        pind = pind + 1;
	end   
end


%% recover the old variables (see B above)
u = u.*(ones(length(t),1)*(Cg.^-0.5)).*Cg(1).^0.5;
uspectrum =  uspectrum.*(ones(length(t),1)*(Cg.^-0.5)).*Cg(1).^0.5;

Norm=1./(Deltat/sqrt(g0)).*trapz(t/sqrt(g0),abs(u(:,:)).^2);
%% Generic plots
Debug=0;

if Exportinit=='yes'
	sample_frequency = 100. ;%hz this is the sample frequency of the output. 
    initial_depth= H(1);
    pos_gau=import_pos_gau_for_input('pos_wavegauges.txt');
    pos_gau=table2array(pos_gau);
    %% make header
    Input_function=inputOptions{excflag};%manually choose the input function to be used
    Posible_functions=['Mod_instability','Soliton','Ak_Breather','Peregrine','Super_Gaussian'];
    input_signal=Factor*real(u(tplot,1).*exp(1i*(freq_dim*(2*pi)*t(tplot)/sqrt(g0)))');
	fig_sample=figure('name','Output sample');
    tp=t(tplot)/sqrt(g0);
    [A1,t_sample] = resample(input_signal,tp,100);
    fprintf('sanity check: Wave maker sample frequency shoud be 0.01 \n sample frequency is:  %.3f \n ',t_sample(2)-t_sample(1))
    if A1(1)>0
        t0 = find(A1<0,1,'first');
        t1 = find(A1<0,1,'last');
    else
        t0 = find(A1>0,1,'first');
        t1 = find(A1>0,1,'last');
    end
    %borders at 0 (a tukey window might work also.)
    A2=[0 A1(t0:t1)' 0]';
    if t1==length(t_sample)
        t_sample(t1+1)=t_sample(t1);
    end
    t2= t_sample(t0-1:t1+1) ;
    M=[t2'-min(t2),A2];
	if Debug~=1
		close(fig_sample)
	end
	[flm fullfolder]=save_filename(freq_dim,B0,esteep0,T0_dim);
    mkdir(fullfolder) ;
    filename_input=fullfile(fullfolder,strcat(flm,'_input.txt'));
    print_input_file2(filename_input,Input_function,sample_frequency,initial_depth,Factor,M);
    filename_wm=fullfile(fullfolder,strcat(flm,'_run_001.txt'));
    csvwrite(filename_wm,M) ;
    filename_pos=fullfile(fullfolder,strcat(flm,'_pos.txt'));
    print_pos_file(filename_pos,Input_function,sample_frequency,initial_depth,Factor,pos_gau);
    mkdir(fullfile(fullfolder,'simulation and input'))
	img_folder=fullfile(fullfolder,'simulation and input');
    filename_params=fullfile(fullfolder,strcat(flm,'_params.txt'));
    print_params_file(filename_params,Input_function,sample_frequency, save_params,save_params_names)
    fig_signal=figure('name','signal');
		title('Translated in time and borders at 0','FontSize',Fsz);
		plot(t2'-min(t2),A2,'b');
		xlabel('T (s)','fontsize',Fsz);
		ylabel('Amplitude (m)','fontsize',Fsz);
		savefig(fullfile(img_folder,strcat(flm,'_input_signal')));
		copyfile('button_dialog_temp_1.txt',fullfolder);
		copyfile('kh_dialog_temp.txt',fullfolder);
		status = copyfile('default_coef_equation_temp.txt',fullfolder);
	if Debug~=1
		close(fig_signal)
	end
    %% figures
	if length(t_sample)==length(A1)
		fig_resample=figure('name','Resample')
			title('Resample from simulation','FontSize',Fsz)
			hold on
			plot(t_sample,A1,'.r')
			plot(tp,input_signal,'-b')
			hold off
			xlabel('T (s)','fontsize',Fsz);
			ylabel('Amplitude (m)','fontsize',Fsz);
		savefig(fullfile(img_folder,strcat(flm,'_sample_check')));
		if Debug~=1
			close(fig_resample)
		end
	end
%%
	f1=figure('name','signal');
    title('Translated in time and borders at 0','FontSize',Fsz)
    subplot(211);
    l1=plot(t2'-min(t2),A2,'b');
    set(l1,'LineWidth',.8)
    xlabel('T (s)','fontsize',Fsz);
    ylabel('Amplitude (m)','fontsize',Fsz);
    subplot(212);
    sig_ff=ifft(A2);
    L=length(sig_ff);
    P2 = abs(sig_ff/L);
    P1 = P2(1:L/2+1);
    P1(2:end-1) = 2*P1(2:end-1);
    fa=100;
%Define the frequency domain f and plot the single-sided amplitude spectrum P1. The amplitudes are not exactly at 0.7 and 1, as expected, because of the added noise.
% On average, longer signals produce better frequency approximations.
    f = fa*(0:(L/2))/L;
    pl_1=plot(f,P1);
    set(pl_1,'LineWidth',1)
    xlim([0,3]); 
    savefig(fullfile(img_folder,strcat(flm,'_input_signal2')));
	if Debug~=1
		close(f1)
	end	
end
	%%
f = plot_conservations_TL(X,Norm,P,Fsz);
if Exportinit=='yes', savefig(fullfile(img_folder,strcat(flm,'_params')));end 
	  
%% Conservations plot.
%%
switch Dimensionflag
    case 1       
	%% Data processing: extracting sidebands
	if any(IC_case==[1,2,3,4])
	utrunc=get_sideband_andreas(omegaxis,omega0,uspectrum,nt);
	
	Eta0 = abs(utrunc(1,:)).^2./Norm(1);
	Eta1 = abs(utrunc(3,:)).^2./Norm(1);
	Etam1 = abs(utrunc(2,:)).^2./Norm(1);
	Eta2 = abs(utrunc([5],:)).^2./Norm(1);
	Etam2 = abs(utrunc([4],:)).^2./Norm(1);
	Unb = Eta1 - Etam1;
	Utr = Eta0 + Eta1 + Etam1;
% 	Phi = unwrap(angle(utrunc(3,:)))+unwrap(angle(utrunc(2,:))) - 2*unwrap(angle(utrunc(1,:)));
	Phi =unwrap( ...
		unwrap( ...
		unwrap(angle(utrunc(3,:)))+unwrap(angle(utrunc(2,:)))...
		) - 2.*unwrap(angle(utrunc(1,:)))...
		);
	Psi=Phi;

	Eta=(Eta1+Etam1)./Utr;	
	Beta_cg=Beta*Cg(1)./Cg;
	omega_max=sqrt(abs(Beta_cg./Alpha)).*sqrt(Norm(1));
	Psi_2d=linspace(0,2*pi,200+1)';
	eta_2d=linspace(0,1.0001,200+1);
	gamma_3w=(omega0./omega_max(1)).^2-1;

	H3w=eta_2d.*(eta_2d-1).*cos(2.*Psi_2d)+gamma_3w.*eta_2d+0.75.*eta_2d.^2;

	X3w=eta_2d.*cos(Psi_2d);
	Y3w=eta_2d.*sin(Psi_2d);

	figure('name','regime and decomp');
	set_latex_plots(groot)
	axes('Position',[.12,.58,.74, .36]);
% 	tly = tiledlayout(1,1);
	hold on
	plot(X, Eta0,'linewidth',1.2,'color',color_main)
	plot(X, Eta1,'linewidth',1.2,'color',color_sideband)
	plot(X,Etam1,':','linewidth',1.2,'color',color_sideband)
	plot(X, Eta2,'linewidth',1.2,'color',color_2sideband)
	plot(X,Etam2,':','linewidth',1.2,'color',color_2sideband)
	plot(X, Utr,'--','linewidth',1.2,'color',color_utr);	
	hold off
	legend('$\eta_0$','$\eta_1$','$\eta_{-1}$','$\eta_{-2}$','$\eta_{-3}$','$\eta_{2}$','$\eta+\eta_1+\eta_{-1}$');
	xlim([0,Lx]);
	set(gca,'FontSize',Fsz-1,'XTickLabel','');
	ylabel('$\eta_n$','FontSize',Fsz)
	% text(.15,.8,'(a)');
	xt = xticks;
% 
% 	ax2 = axes(tly);
% 	ax2.XAxisLocation = 'top';
% % 	xxaxis up 
% 	set(gca, 'xtick', xt/Lnl, 'xlim', [0,X(end)./Lnl], 'Xcolor', 'k','XDir','normal','FontSize',Fsz-1)
% 	xtickformat('%.1f');
% 	xlabel('$L_{nl}$','fontsize',Fsz-2);
	
	
	
	axes('Position',[.12,.15,.74, .36]);
	[phax,phlin,nl] = plotyy(X, Psi/2/pi, ...
		X, omega0./omega_max);
	set(phlin,'linewidth',1.5,'color','b');
	set(nl,'linewidth',1.5,'linestyle','-','color','k');
	hold(phax(1),'on');
	xlabel('$\xi$','fontsize',Fsz);
	xlim([0,Lx]); 
	set(phax(1),'FontSize',Fsz-1,'xlim',[0,Lx],'Ycolor','b');
	set(phax(2),'FontSize',Fsz-1,'xlim',[0,Lx],'Ycolor','k');
	ylabel(phax(1),'$\psi/\pi$','FontSize',Fsz);
	ylabel(phax(2),'$\Omega/\Omega_M$','FontSize',Fsz);
	hold(phax(1),'off');
	if Exportinit=='yes',  savefig(fullfile(img_folder,strcat(flm,'_regime')));
	end

	%phase plane plots
	figure('name','3w space 1');
	set_latex_plots(groot)
	ratio=20/9;	r_fig=400;
	f=figure('name','3wave','position',[20 15 ratio*r_fig r_fig]);
	subplot(121)
	hold on
	plot(Psi/2/pi,Eta,'k','linewidth',1,'DisplayName','$\eta$' )
% 	Psi(1)/2/pi,Eta(1),'rx'
	plot(Psi/2/pi,(Eta0)./Utr,'--k','linewidth',1,'DisplayName','$E-\eta$')
% 	,Psi(1)/2/pi,Eta0(1)./Utr(1),'rx','linewidth',1);
	legend()
	hold off
	set(gca,'FontSize',Fsz-1);
	xlabel('$\psi/\pi$','Fontsize',Fsz);
	ylabel('$\eta$','Fontsize',Fsz);

	
	subplot(122)
	alpha_eta=-(omega0./omega_max).^2-1;
	eta_MI=2/7.*(1.-alpha_eta);
% 	figure('name','alpha and eta');
		plot(X,alpha_eta,'k');
		ylabel('$\gamma$','fontsize',Fsz);
		ax2 = gca;
		yyaxis right
		plot(X,eta_MI);
		ylabel('$\eta$','fontsize',Fsz);

		if Exportinit=='yes', savefig(fullfile(img_folder,strcat(flm,'_3W_space1')));
	end 

	figure('name','3w space 2','position',[20 20 3./2.*r_fig r_fig]);
		set_latex_plots(groot)
		hold on
		s=surf(X3w,Y3w,H3w);
		set(s, 'EdgeColor', 'None','LineWidth',1.,'FaceColor','interp','facealpha',0.7);
		% alpha 0.5
		levl=min(min(H3w));
		cmap=cmocean('topo','pivot',0);
		colormap(cmap);
		colorbar()
		set(gca,'YDir','normal','FontSize',Fsz-1);
		view(2)
		plot(Eta.*cos(Psi./2),Eta.*sin(Psi./2),'color',e_color,'linewidth',1.8)
		plot(Eta(1)*cos(Psi(1)/2),Eta(1)*sin(Psi(1)/2),'rx','linewidth',1.8);
		hold off
		set(gca,'FontSize',Fsz-1);
		xlabel('$\eta \cos(\psi)$','Fontsize',Fsz);
		ylabel('$\eta \sin(\psi)$','Fontsize',Fsz);
		ylim([-1,1]);
		xlim([-1,1]);
	if Exportinit=='yes', savefig(fullfile(img_folder,strcat(flm,'_3W_space2')));
	end 

	% Akhmedievstyle phaseplane plot (full NLS minus global phase)
	if IC_case==3
		Ucomplexplane = u/B0.*exp(-1i*ones(length(t),1)*unwrap(angle(utrunc(1,:))));
		figure('name','Akhmedievstyle phaseplane');
		set_latex_plots(groot)
		plot(real(Ucomplexplane(nt/2+1,:)),imag(Ucomplexplane(nt/2+1,:)),real(Ucomplexplane(nt/2+1,1)),imag(Ucomplexplane(nt/2+1,1)),'rx','linewidth',1);
		set(gca,'FontSize',Fsz-1);
		xlabel('Re $B(t=0)$','Fontsize',Fsz);
		ylabel('Im $B(t=0)$','Fontsize',Fsz);
		if Exportinit=='yes',savefig(fullfile(img_folder,strcat(flm,'_Akhmediev_space')));
		end 
	end	
end
%%
f=plot_contrast_TL(u,X,Fsz);

	%% 3 wave from data (not done)	
% 	three_wave_getsideband(u,freq_dim,t)
	%%
deltaH=abs(H(1:end-1)-H(2:end));
bathm_change=any(deltaH)>0; %is 1 if there is a change in bathymetry
%%
f=plot_Amplitude_and_fourier_TL(K,H,X,B0,t,u,tclean,Omega,omega0,omegaxis,-12,Normalized,Fsz);
savefig(fullfile(img_folder,strcat(flm,'_evol')));
saveas(gcf,fullfile(fullfolder,strcat(flm,'_evol','.jpeg')));
%%  Surface and fourier 2d plot with bathymetry   
if bathm_change==1 %plot if there is a change in bathymetry.
	cmap='viridis';
% 	cmap='viridis5perc';
	plot_Amplitude_Evolution_wbath_TL(K,H,X,t,u,tplot,Lx,xvarstart,xvarstop,cmap);
    if Exportinit=='yes'
       savefig(fullfile(img_folder,strcat(flm,'_2d_evol')));
       saveas(gcf,fullfile(fullfolder,strcat(flm,'_2d_evol','.jpeg')));
	end
	%  Fourier 2d plot with bathymetry
	plot_fourier_bathm_TL(K,H,Omega,omega0,uspectrum,omegaxis,X,B0,Lx,xvarstart,xvarstop,'inferno');
    if Exportinit=='yes'
		savefig(fullfile(img_folder,strcat(flm,'_2d_fourier')));
	end
else 
	cmap='viridis';
	plot_Amplitude_Evolution_TL(K,H,X,B0,t,u,tplot,Lx,Lnl,cmap,Normalized)
	if Exportinit=='yes'
       savefig(fullfile(img_folder,strcat(flm,'_2d_evol')));
       saveas(gcf,fullfile(fullfolder,strcat(flm,'_2d_evol','.jpeg')));
	end
end
%% Plots for Gaussian/Supergaussian
if any(IC_case==[7,13])
%Bback = str2double( Pertpar(2));  Bpert = str2double( Pertpar(3));
	cmap=viridis;
	f = plot_Amplitude_Evolution_first_last_TL(K,H,X,Bback,Bpert,B0,t,u,tplot,Lx,xvarstart,xvarstop,cmap,Normalized);
	if Exportinit=='yes'
		savefig(fullfile(img_folder,strcat(flm,'envelope_first_last')));	
		saveas(gcf,fullfile(fullfolder,strcat(flm,'_envelope_first_last','.jpeg')));
	end
end	
if any(IC_case==[5,7])
	UdBth=-20;
	f = plot_fourier_first_last_TL(K,H,Omega,omega0,u,omegaxis,X,B0,Lx,xvarstart,xvarstop,UdBth,'inferno');
	if Exportinit=='yes'
		savefig(fullfile(img_folder,strcat(flm,'_fou_first_last')));	
	end
	cmap=colorcet('D2'); %D1-D11 Perceptually linear divergent colormaps. I like D6 or D2, since it has none of the usuall colors. 
	f=plot_chirp_Evolution_first_last_TL(K,H,X,Bback,Bpert,B0,t,u,tplot,Lx,xvarstart,xvarstop,cmap);
	if Exportinit=='yes'
		savefig(fullfile(img_folder,strcat(flm,'_chirp_first_last')));	
	end
	fig_fou_sanscarrier = plot_fourier_sanscarrier_TL(u,omegaxis,Omega,X);
	if Exportinit=='yes'
		savefig(fullfile(img_folder,strcat(flm,'_fourier_sans_carrier')));	
	end
	plot_contrast_shock_TL(B0,Bback,u,X);
	if Exportinit=='yes'
		savefig(fullfile(img_folder,strcat(flm,'_contrast')));	
	end
end

%% 3d envelope and steepness

f=plot_3d_envelope_TL(u,t,X,B0,tplot,viridis,Normalized,Fsz);
if Exportinit=='yes',
	savefig(fullfile(img_folder,strcat(flm,'_3d_surf')));	
    saveas(gcf,fullfile(fullfolder,strcat(flm,'_2d_evol','.jpeg')));
end
%% tank plot
if khfin~=kh0
	f=plot_tank_scheme_TL(X,H,xvarstart,xvarstop,Exportinit,pos_gau,Lx,Fsz);
	if Exportinit=='yes'
		savefig(strcat(img_folder,'\',flm,'_bathm'))
		filename_bathm=strcat(fullfolder,'\',flm,'_bathm.txt');
		M=[X' -H'];
		print_bathym_file2(filename_bathm,Input_function,sample_frequency,initial_depth,Factor,M)
	end
end

% %% old plots
% f=bath_plot2dsurf_old(K,H,t,tplot,X,Lx,u,'viridis',Fsz);
% UdBth = -40;
% f=bath_plot2dfourier_old(K,H,uspectrum,omegaxis,Omega,omega0,B0,X,Lx,UdBth,'inferno',Fsz);

case 2
%% Data processing: extracting sidebands
IAS = find(omegaxis<=omega0+.1 & omegaxis>=omega0-.1); IAS = IAS(round(length(IAS)/2));
IS = find(omegaxis<=-omega0+.1 & omegaxis>=-omega0-.1); IS = IS(round(length(IS)/2));
IAS2 = find(omegaxis<=2*omega0+.1 & omegaxis>=2*omega0-.1); IAS2 = IAS2(round(length(IAS2)/2));
IS2 = find(omegaxis<=-2*omega0+.1 & omegaxis>=-2*omega0-.1); IS2 = IS2(round(length(IS2)/2));
IAS3 = find(omegaxis<=3*omega0+.1 & omegaxis>=3*omega0-.1); IAS3 = IAS3(round(length(IAS2)/2));
IS3 = find(omegaxis<=-3*omega0+.1 & omegaxis>=-3*omega0-.1); IS3 = IS3(round(length(IS2)/2));

utrunc = uspectrum([nt/2+1,IS,IAS,IS2,IAS2,IS3,IAS3],:);
Eta0 = abs(utrunc(1,:)).^2/B0^2;
Eta1 = abs(utrunc(3,:)).^2/B0^2;
Etam1 = abs(utrunc(2,:)).^2/B0^2;
Eta2 = abs(utrunc([5],:)).^2/B0^2;
Etam2 = abs(utrunc([4],:)).^2/B0^2;
Alpha = Eta1 - Etam1;
Utr = Eta0 + Eta1 + Etam1;
Phi = unwrap(angle(utrunc(3,:)))+unwrap(angle(utrunc(2,:))) - 2*unwrap(angle(utrunc(1,:)));


figure;
UdBth = -50;
Usp = 20*log10(abs(uspectrum.')/B0);
Usp = (Usp>UdBth).*(Usp-UdBth) +  UdBth;
imagesc(omegaxis,X,Usp);
hold on;
plot(omegaxis,xvarstart*ones(size(omegaxis)),'k:',omegaxis,xvarstop*ones(size(omegaxis)),'k:','Linewidth',2);
set(gca,'YDir','normal','FontSize',14);
xlim([-4*omega0,4*omega0]);
xlabel('\omega [m^{-1/2}]','fontsize',16);
ylabel('X [m]','fontsize',16);
% shading interp;
% colormap hot;
colorbar;
Pos = get(gca,'Position');
Yl = get(gca,'ylim');
axes('Position',Pos);
plot(H.*K,X,'r--','linewidth',2);
xlabel('hk','fontsize',16);
set(gca,'XAxisLocation','top','GridAlpha',1,'YTickLabel','','Fontsize',14,'Ylim',Yl,'Color','none')


figure;
Pos = [.14,.15,.79,.72];
axes('Position',Pos)
imagesc(t(abs(t)<=windowwidth),X,abs(u(tplot,:).')/B0);
hold on;
plot(t(tplot),xvarstart*ones(size(t(tplot))),'k:',t(tplot),xvarstop*ones(size(t(tplot))),'k:','Linewidth',2);
set(gca,'YDir','normal','FontSize',14);
xlabel('\tau [m^{1/2}]','fontsize',16);
ylabel('\xi [m]','fontsize',16);
%shading interp; colorbar;
Pos = get(gca,'Position');
Yl = get(gca,'ylim');
axes('Position',Pos);
plot(H.*K,X,'r--','linewidth',2);
xlabel('hk','fontsize',16);
set(gca,'XAxisLocation','top','GridAlpha',1,'YTickLabel','','Fontsize',14,'Ylim',Yl,'Color','none')

figure;
meshc(t(tplot),X,abs(u(tplot,:).')/B0);
set(gca,'YDir','normal','FontSize',14);
xlabel('T [m^{1/2}]','fontsize',16);
ylabel('X [m]','fontsize',16);
zlabel('|B|/B_0','fontsize',16);
% shading interp;
% colormap hot;

figure;
meshc(t(tplot),X,abs(u(tplot,:).').*(K.'*ones(size(t(tplot)))));
set(gca,'YDir','normal','FontSize',14);
xlabel('T [m^{1/2}]','fontsize',16);
ylabel('X [m]','fontsize',16);
zlabel('Steepness','fontsize',16);
% shading interp;
colormap hot;

figure;
axes('Position',[.12,.6,.78, .35]);
plot(X, Eta0, X, Eta1,'g-.', X, Etam1,'r-.', ...
    X, Eta2,'g--', X , Etam2,'r--', X, Utr,'k:','linewidth',1.2);
% legend('\eta_0','\eta_1','\eta_{-1}','\eta_{-2}','\eta_{-3}','\eta_{2}','\eta+\eta_1+\eta_{-1}');
xlim([0,Lx]);set(gca,'FontSize',14);
text(.15,1.35,'(a)');
axes('Position',[.12,.15,.78, .35]);
[phax,phlin,nl]= plotyy(X, Phi/2/pi, ...
    X, Beta./Alpha);
set(phlin,'linewidth',1.5);
set(nl,'linewidth',1.5);
% legend('\Omega\alpha','P/N');
xlabel('X','fontsize',15);
xlim([0,Lx]); 
% ylim([-.5,.5]);grid;
set(phax(1),'FontSize',14,'xlim',[0,Lx]);
set(phax(2),'FontSize',14,'xlim',[0,Lx]);
text(.15,1.35,'(b)');


figure;
plot(Phi/2/pi,(Eta1+Etam1)./ Utr,Phi(1)/2/pi,(Eta1(1)+Etam1(1))./Utr(1),'rx',Phi/2/pi,(Eta0)./ Utr,'--',Phi(1)/pi,Eta0(1)./Utr(1),'rx','linewidth',1);
set(gca,'FontSize',14);
xlabel('\psi/\pi','Fontsize',16);
ylabel('\eta','Fontsize',16);

figure;
plot((Eta1+Etam1)./ Utr.*cos(Phi/2),(Eta1+Etam1)./ Utr.*sin(Phi/2),(Eta1(1)+Etam1(1))/Utr(1)*cos(Phi(1)/2),(Eta1(1)+Etam1(1))/Utr(1)*sin(Phi(1)/2),'rx','linewidth',1);
set(gca,'FontSize',14);
xlabel('\eta \cos(\psi)','Fontsize',16);
ylabel('\eta \sin(\psi)','Fontsize',16);

end


%% benchmark plots
if any(IC_case==[13])
    f=figure('name','Fourier','position',[20 20 500 500]);
    hold on
    UdBth = -15;
    Usp = 10*log10(abs(uspectrum.').*sqrt(length(u(:,1)).*2)*sqrt(g0)*(1/(2*pi)));
% 	    Usp = 10*log10(abs(uspectrum.')./max(uspectrum,[],"all"));
% 	    Usp = 20*log10(abs(uspectrum.').*sqrt(length(u(:,1)).*2)*sqrt(g0)*(1/(2*pi))./B0);
%     Usp = (Usp>UdBth).*(Usp-UdBth) +  UdBth;
    Usp = Usp-max(Usp,[],"all");
	Usp = (Usp>UdBth).*(Usp-UdBth)+UdBth;

	%     imagesc(omegaxis*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi),X,abs(uspectrum')*sqrt(g0)*(1/(2*pi))/B0);
    imagesc(omegaxis*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi),X,Usp);

	set(gca,'XDir','normal','FontSize',Fsz-1);
%     ylim([-4.5*omega0*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi),4.5*omega0*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi)]);
    xlim([1,3.5]);
	ylim([X(1),X(end)])
    xlabel('$f$ [^Hz]','fontsize',Fsz);
    ylabel('$x$ [m]','fontsize',Fsz);
    ytickformat('%.1f');
	colorMap = jet(256);
	colormap(colorMap);   % Apply the colormap
	hold off
	colorbar()
	
	L0=2*pi./k0;

	f=figure('position',[20 20 500 500]);
	s=pcolor(t(tplot)./sqrt(g0),fliplr(X),flipud(abs(u(tplot,:)')));
	set(s, 'EdgeColor', 'black','LineWidth',1.,'MeshStyle','None');
% 	set(s,'ytick',[0 20 ] ,'xtick',[ 0 min(H.*K)  max(H.*K)*1.0000001 ],'xticklabel',{'hk:' min(H.*K)  max(H.*K)})
% 	set(gca,'ytick',[0 20 40 60],'yticklabel',{0 20 40 60},'xtick',[-16 -8 0 8 16],'xticklabel',{-16 -8 0 8 16})
	hold on
	plot([t(1)./sqrt(g0) t(end)./sqrt(g0)],[4 4],'--g')
	hold off
	xlabel('t[s] ','fontsize',Fsz); 
	ylabel('x[m]  ','fontsize',Fsz);
	xlim([-10,10])
	ylim([0,10])
	colorMap = jet(256);
%  		colorMap = brighten(colorMap,-0.25);
	colormap(colorMap);   % Apply the colormap
	cb=colorbar();
	ylabel(cb, '$|U|$')
	caxis([0 8*10^(-3)])
end
	
		%%
	if IC_case==11
		f=plot_zhang_TL(k0,t,tplot,X,u,B0,T0);
		if Exportinit=='yes'
			savefig(fullfile(img_folder,strcat(flm,'_zhang_paper_plot')));	
		end
	end


%% Plots for solitions
	if any(IC_case==[5,14])
		f=plot_soliton_benchmark_TL(K,H,Omega,omega0,uspectrum,omegaxis,X,B0,Lx,xvarstart,xvarstop,'jet')
		if Exportinit=='yes'
			savefig(fullfile(img_folder,strcat(flm,'_soliton_benchmark_fourier')));	
			saveas(gcf,fullfile(fullfolder,strcat(flm,'_soliton_benchmark_fourier','.jpeg')));
		end
		if Nsol==4
			f=plot_soliton_benchmark_N4_TL(K,H,X,t,u,tplot,Lx,xvarstart,xvarstop,'jet',B0)
			if Exportinit=='yes'
				savefig(fullfile(img_folder,strcat(flm,'_soliton_benchmark_N4')));	
				saveas(gcf,fullfile(fullfolder,strcat(flm,'_soliton_benchmark_N4','.jpeg')));
			end
			f=plot_fourier_solitonN4(K,H,Omega,omega0,uspectrum,omegaxis,X,B0,Lx,xvarstart,xvarstop,cmap);
			if Exportinit=='yes'
				savefig(fullfile(img_folder,strcat(flm,'_soliton_benchmark_fourier_N4')));	
				saveas(gcf,fullfile(fullfolder,strcat(flm,'_soliton_benchmark_fourier_N4','.jpeg')));
			end	
		end
	end
%%

	%% Data processing: extracting sidebands
	pos_omegac = nt/2+1;
	search_range = find(omegaxis<=omega0+omega0/3 & omegaxis>=omega0/3);
	[~, pos_eta1] = max(abs(uspectrum(search_range,1)));
	dN =  search_range(pos_eta1)-pos_omegac;% spreading of peaks on frequency axis;
	omegaMod=omega0;
	utrunc = uspectrum([pos_omegac,pos_omegac-dN ,pos_omegac+dN,pos_omegac-2*dN,pos_omegac+2*dN,pos_omegac-3*dN,pos_omegac+3*dN],:);
	Eta0 = abs(utrunc(1,:)).^2./B0.^2;
	Eta1 = abs(utrunc(3,:)).^2./B0.^2;
	Etam1 = abs(utrunc(2,:)).^2./B0.^2;
	Alpha = Eta1 - Etam1;
	Utr = Eta0 + Eta1 + Etam1;
	% Phi = unwrap(angle(utrunc(3,:)))+unwrap(angle(utrunc(2,:))) - 2*unwrap(angle(utrunc(1,:)));

	Phi =unwrap( ...
		unwrap( ...
		unwrap(angle(utrunc(3,:)))+unwrap(angle(utrunc(2,:)))...
		) - 2.*unwrap(angle(utrunc(1,:)))...
		);

	% plotting normalized frequency components 
	L0=1;
	f=figure('name','Fourier','position',[20 20 500 500]);	
	subplot(211)
	plot(X*L0, Eta0, X*L0, abs(utrunc([2,4,6],:)).^2./B0.^2,'g-.', X*L0, abs(utrunc([3,5,7],:)).^2./B0.^2,'r-.',X*L0, Utr,'k:',X*L0,N./N(1),'linewidth',1.2);
% 	legend('\eta_0','\eta_{-1}','\eta_{-2}','\eta_{-3}','\eta_{+1}','\eta_{+2}','\eta_{+3}','\eta+\eta_1+\eta_{-1}','N/N(0)');
	xlabel('X[m]','fontsize',15);
	set(gca,'fontsize',14)
	xlim([0,Lx*L0]);

	subplot(223);
	plot((Eta1+Etam1).*cos(Phi/2), (Eta1+Etam1).*sin(Phi/2),(Eta1(1)+Etam1(1)).*cos(Phi(1)/2), (Eta1(1)+Etam1(1)).*sin(Phi(1)/2),'diamond');
	set(gca,'fontsize',14);
	axis equal
	xlabel('\eta'' cos \psi/2','fontsize',16);
	ylabel('\eta'' sin \psi/2','fontsize',16);

	subplot(224);
	plot(X*L0, omegaMod*Alpha, ...
	X*L0,real(P./N),'--k','linewidth',1.2);
	legend('\Omega\alpha','P/N');
	xlabel('X[m]','fontsize',15);
	set(gca,'fontsize',14);
	xlim([0,Lx*L0]); ylim([-2,2]);

%%
try %%%%this doesn't work well
	data_series=u(tclean,:);
	central_frequency=omega0;
	var=t(tclean);

	[ModesAmplitudes]=getsidebands_static_theoretical(data_series,central_frequency,var,0);
	Norm_modes =1./((var(end)-var(1))).*trapz(var,abs(data_series).^2);
	a0_norm=sqrt(Norm_modes);

	n_wavegauges=length(X);
	pos_wg=x;


	Eta0 = abs(ModesAmplitudes(1,:)).^2./Norm_modes;
	Eta1 = abs(ModesAmplitudes(3,:)).^2./Norm_modes;
	Etam1 = abs(ModesAmplitudes(2,:)).^2./Norm_modes;
	Eta2 = abs(ModesAmplitudes(5,:)).^2./Norm_modes;
	Etam2 = abs(ModesAmplitudes(4,:)).^2./Norm_modes;
	Alpha = Eta1 - Etam1;
	Utr = Eta0 + Eta1 + Etam1;
	% Eta0 = Eta0./Utr(1) ; Eta1 = Eta1./Utr(1) ;Etam1 = Etam1./Utr(1) ; Eta2 = Eta2./Utr(1)  ;Etam2=Etam2./Utr(1),Utr=Utr./Utr(1);
	% Psi_exp =unwrap( unwrap(angle(ModesAmplitudes(3,:)))+unwrap(angle(ModesAmplitudes(2,:))) - 2.*unwrap(angle(ModesAmplitudes(1,:))));
	Psi_left =unwrap(angle(ModesAmplitudes(3,:)));
	Psi_right=unwrap(angle(ModesAmplitudes(2,:)));
	Psi_main =unwrap(angle(ModesAmplitudes(1,:)));
	Psi_exp =unwrap( ...
		unwrap( ...
		unwrap(angle(ModesAmplitudes(3,:)))+unwrap(angle(ModesAmplitudes(2,:)))...
		) - 2.*unwrap(angle(ModesAmplitudes(1,:)))...
		);
	Psi_exp2 =unwrap( ...
		unwrap( ...
		unwrap(angle(ModesAmplitudes(4,:)))+unwrap(angle(ModesAmplitudes(5,:)))...
		) - 2.*unwrap(angle(ModesAmplitudes(1,:)))...
		);
	%using the linear phase definition
	% alpha_exp=-(Lambda_exp* )
	figure;
	set_latex_plots(groot)
	figure('name','regime and decomp');
	set_latex_plots(groot)
	axes('Position',[.12,.58,.74, .36]);
	hold on
	plot(X, Eta0,'linewidth',1.2,'color',color_main)
	plot(X, Eta1,'linewidth',1.2,'color',color_sideband)
	plot(X,Etam1,':','linewidth',1.2,'color',color_sideband)
	plot(X, Eta2,'linewidth',1.2,'color',color_2sideband)
	plot(X,Etam2,':','linewidth',1.2,'color',color_2sideband)
	plot(X, Utr,'--','linewidth',1.2,'color',color_utr);	
	hold off
	legend('$\eta_0$','$\eta_1$','$\eta_{-1}$','$\eta_{2}$','$\eta_{-2}$','$\eta+\eta_1+\eta_{-1}$');
	xlim([0,Lx]);
	set(gca,'FontSize',Fsz-1,'XTickLabel','');
	ylabel('$\eta_n$','FontSize',Fsz)
	% text(.15,.8,'(a)');
	axes('Position',[.12,.15,.74, .36]);
	[phax,phlin,nl] = plotyy(X, Psi/2/pi, ...
		X, omega0./omega_max);
	set(phlin,'linewidth',1.5,'color','b');
	set(nl,'linewidth',1.5,'linestyle','-','color','k');
	hold(phax(1),'on');
	xlabel('$\xi$','fontsize',Fsz);
	xlim([0,Lx]); 
	set(phax(1),'FontSize',Fsz-1,'xlim',[0,Lx],'Ycolor','b');
	set(phax(2),'FontSize',Fsz-1,'xlim',[0,Lx],'Ycolor','k');
	ylabel(phax(1),'$\psi/\pi$','FontSize',Fsz);
	ylabel(phax(2),'$\Omega/\Omega_M$','FontSize',Fsz);
	hold(phax(1),'off');
	if Exportinit=='yes',  savefig(fullfile(img_folder,strcat(flm,'_regime')));
	end

	%phase plane plots
	figure('name','3w space 1');
	set_latex_plots(groot)
	ratio=20/9;	r_fig=400;
	f=figure('name','3wave','position',[20 15 ratio*r_fig r_fig]);
	subplot(121)
	hold on
	plot(Psi/2/pi,Eta,'k','linewidth',1,'DisplayName','$\eta$' )
% 	Psi(1)/2/pi,Eta(1),'rx'
	plot(Psi/2/pi,(Eta0)./Utr,'--k','linewidth',1,'DisplayName','$E-\eta$')
% 	,Psi(1)/2/pi,Eta0(1)./Utr(1),'rx','linewidth',1);
	legend()
	hold off
	set(gca,'FontSize',Fsz-1);
	xlabel('$\psi/\pi$','Fontsize',Fsz);
	ylabel('$\eta$','Fontsize',Fsz);

	
	subplot(122)
	alpha_eta=-(omega0./omega_max).^2-1;
	eta_MI=2/7.*(1.-alpha_eta);
% 	figure('name','alpha and eta');
		plot(X,alpha_eta,'k');
		ylabel('$\gamma$','fontsize',Fsz);
		ax2 = gca;
		yyaxis right
		plot(X,eta_MI);
		ylabel('$\eta$','fontsize',Fsz);

		if Exportinit=='yes', savefig(fullfile(img_folder,strcat(flm,'_3W_space1')));
	end 

	figure('name','3w space 2','position',[20 20 3./2.*r_fig r_fig]);
		set_latex_plots(groot)
		hold on
		s=surf(X3w,Y3w,H3w);
		set(s, 'EdgeColor', 'None','LineWidth',1.,'FaceColor','interp','facealpha',0.7);
		% alpha 0.5
		levl=min(min(H3w));
		cmap=cmocean('topo','pivot',0);
		colormap(cmap);
		colorbar()
		set(gca,'YDir','normal','FontSize',Fsz-1);
		view(2)
		plot(Eta.*cos(Psi./2),Eta.*sin(Psi./2),'color',e_color,'linewidth',1.8)
		plot(Eta(1)*cos(Psi(1)/2),Eta(1)*sin(Psi(1)/2),'rx','linewidth',1.8);
		hold off
		set(gca,'FontSize',Fsz-1);
		xlabel('$\eta \cos(\psi)$','Fontsize',Fsz);
		ylabel('$\eta \sin(\psi)$','Fontsize',Fsz);
		ylim([-1,1]);
		xlim([-1,1]);
	if Exportinit=='yes', savefig(fullfile(img_folder,strcat(flm,'_3W_space2')));
	end 
	
end