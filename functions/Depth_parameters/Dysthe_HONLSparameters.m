function [h,k,sigma,cg,cp,alpha,beta,alpha3,alpha4,beta21,beta22,mu,H3,muintexp] = Dysthe_HONLSparameters(omega,kh,x,FOD)
     %%
    % input params 
    % omega is the angular frequency in units of g^{1/2}
    % hk is the current value of h(x)k(x)
    % x is the position along the propagation distance
    % 
    % output params
    % k(x) wavenumber
    % sigma = tanh(kh)
    % cg = group velocity
    % cp = phase velocity
    
    %
    % The shoaling is trivial, see Djordjevic&Redekopp ZAMP 1978
    % muintexp is the exp of the integral of mu(x) exp(int(mu))
    %
    % The parameters are defined as in Sedletsky JETP Lett. 97, 180-193 (2003)
    % where the envelope of the surface elevation is used and the expansion
    % to fourth order is performed
    %
    % alpha = group velocity dispersion
    % alpha3 = third order dispersion (=0 in the limit of deep water)
    % alpha4 = fourth order dispersion
    % beta = cubic nonlinear coefficient
    % beta21 = coefficient of |A|^2 D_t A
    % beta22 = coefficient of A D_t |A|^2
    %
    % Dysthe according to Debbie's thesis.
    %%
    
  % auxiliary quantities
%     sigma = tanh(kh);
%     k = omega.^2./sigma;
	sigma=1;
	h=1000;
	g=1;
	k = omega.^2;
%     h = kh.*sigma./omega^2;
    % group and phase speed
    cp = omega./k;
    cg = 0.5.*cp;
    
	
	H3=2*k.^3./omega;
	
    % beta in the paper
    sigma2 = (2*omega*cg).^2 - 4.*kh.*sigma;
    sigmaq = sigma.^2-1;
    % mu in the paper
    sigma3 = sigmaq.^2.*kh - sigma.*(sigma.^2-5);
    % NLS quantities
    % beta21''/2 = omega''/Cg^3/2
    alpha = -k/(omega.^2); %checked
    
    % THIS FORMULA IS NOT CORRECT!!!
    % alphaT = -omega'''/6
    % changing to space evolution
    % TOD
    % beta213/6 = alphaT/cg^4 - 2*alpha^2*cg   
    
    % FOD
    % beta214/24
    % TBC
    % TBC !!!!!!!
    if FOD ~=0
        alpha4 = -1./(3*128*omega.^7.*cg.^7).*h.*sigmaq.*(...
            - 3*kh.^4.*sigmaq.^4.*(1-5.^kh)  ...
            - 4.*kh.^sigma.^3.*sigmaq.*(1-kh.*sigma).*(3-4*kh.*sigma).*(5-8.*kh.*sigma)...
            - 4.*kh.^3.*sigma.*sigmaq.^3.*(15+kh.*sigma.*(-31+24.*kh.*sigma))...
            + sigma.^4.*(-3 + kh.*sigma.*(39+8.*kh.*(-7+2*kh.*sigma)))...
            + 6.*kh.^2.*sigma.^2.*sigmaq.^2.*(-19+kh.*sigma.*(51+4*kh.*sigma.*(-11 + 4.*kh.*sigma))));
    else 
        alpha4 = zeros(size(kh));
    end
    % cubic nonlinearity
    beta = k.^3;
	beta21=k.^3*8./omega;
	beta22=2*k.^3./omega;
	% shoaling (depth-related loss/amplification)    
    mu = (1-sigma.^2).*(1-kh.*sigma)./(sigma+kh.*(1-sigma.^2));
    muintexp = ((2.*kh + sinh(2*kh))./cosh(kh).^2).^0.5;
	alpha3 = 0;
% 	alpha = alpha.*ON_OFF_coef(1);
% 	beta = beta.*ON_OFF_coef(2);
% 	alpha3 = alpha3.*ON_OFF_coef(3);
%     beta21 = beta.*ON_OFF_coef(4);
%     beta22 = beta22.*ON_OFF_coef(5);

   
end