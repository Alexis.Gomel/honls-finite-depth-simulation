%%% Sanity chek for the parameter convergence between Dysthe and finite depth.
clear all 
clc
close all
crit= 1.363;

addpath(genpath(fullfile('functions','plot')));
addpath(genpath(fullfile('functions','colormaps')));
color_disp=[0, 0.4470, 0.7410]; %new default blue dispersion
color_nl=[0.6350, 0.0780, 0.1840]; %new default red nonlinear
Fsz=15;

kh=[0.5:0.001:50];
omega=1;
oness=ones(length(kh),1);
sigma = tanh(kh);
k = omega.^2./sigma;
h = kh.*sigma./omega^2;
% group and phase speed
cp = omega./k;
cg = (sigma + kh.*(1-sigma.^2))/2./omega;
x=[1];
FOD=1;
ON_OFF_coef=[1,1,1,1,1,1];
[h,k,sigma,cg,cp,alpha,beta,alpha3,alpha4,beta_21,beta_22,mu,muintexp,Dys_hilb_slun,H3] = depthdependent_HONLS_TimeLike(omega,kh,FOD,ON_OFF_coef);
[h2,k2,sigma2,cg2,cp2,alpha2,beta2,alpha32,alpha42,beta_212,beta_222,mu2,H32,muintexp] = Dysthe_HONLSparameters(omega,kh,x,FOD);


ratio=20/9;
r1=400
figure('position',[20 0 ratio*r1 r1])
set_latex_plots(groot)

hold on
plot(kh,cg./cg2,'-k','LineWidth',1.5)
% plot(kh,cg2.*oness,'--k')
plot([crit, crit], [min(cg./cg2), max(cg./cg2)],':k','LineWidth',1.2)
plot([0, max(kh)], [1, 1],':k','LineWidth',1.2)
ylabel('$C_g/C_g^{\infty}$','interpreter','latex','FontSize',Fsz)
xticks(sort([crit,linspace(0,max(kh),6)]))
hold off
xlabel('$kh$','interpreter','latex','FontSize',Fsz)
%%
Lnl=1./abs(beta);
Tnl=sqrt(abs(2.*alpha.*Lnl));
figure('position',[20 0 ratio*r1 r1])
set_latex_plots(groot)
yyaxis left
hold on
plot(kh,Tnl./Tnl(end),'LineWidth',1.3,'DisplayName','Tnl')
plot([crit, crit], [-3,5],':k','LineWidth',1.2,'DisplayName','Critical $kh$')
ylim([0,4])
% yticks([0,1,3])
% yticklabels({'$\alpha \Omega_M^2$'})
hold off
ylabel('$T_{nl}/Tnl^\infty$','interpreter','latex','FontSize',Fsz)
yyaxis right
plot(kh,Lnl./Lnl(end),'LineWidth',1.3,'DisplayName','Lnl')
ylabel('$L_{nl}/Lnl^\infty$','interpreter','latex','FontSize',Fsz)
ylim([0,4])
xticks(sort([crit,linspace(0,max(kh),6)]))
% yticks([abs(alpha*oM^2)])
% yticklabels({'$\alpha \Omega_M^2$'})
xlabel('$kh$','interpreter','latex','FontSize',Fsz)
savefig('Nonlinear_lenghts')
%%
figure('position',[20 0 ratio*r1 r1])
set_latex_plots(groot)
hold on
yyaxis left
plot(kh,alpha.*sign(alpha2)./alpha2,'-','color',color_disp,'DisplayName',' $\sign( \alpha^\infty ) \, \alpha (kh)/\alpha^\infty $ ','LineWidth',1.5)
plot(kh,oness.*sign(alpha2),'--','color',color_disp,'DisplayName','$\sgn(\alpha^\infty)$')
plot([crit, crit], [min(alpha.*sign(alpha2)./alpha2), max(alpha.*sign(alpha2)./alpha2)],':k','LineWidth',1.2,'DisplayName','Critical $kh$')
ylabel('Dispersion','interpreter','latex','FontSize',Fsz)
% set(gca,'YColor','blue')
yyaxis right
plot(kh,oness.*sign(beta2),'--','color',color_nl,'DisplayName', '$\sgn(\beta^\infty) \, \beta(kh)/\beta^\infty$')
plot(kh,beta./beta2.*sign(beta2),'-','color',color_nl,'DisplayName','$\sgn(\beta^\infty)$','LineWidth',1.5)
ylabel('Non-Linearity','interpreter','latex','FontSize',Fsz)
% set(gca,'YColor','r')
xticks(sort([crit,linspace(0,max(kh),6)]))
hold off
ylim([-2,2])
legend()
xlabel('$kh$','interpreter','latex','FontSize',15)
savefig('disp_v_nonlinear')

hold off
ylim([-2,2])
legend('Interpreter','Latex')
xlabel('$kh$','interpreter','latex','FontSize',15)
%%
ix_beta21= detectzerocross(beta_21);
ix_beta22= detectzerocross(beta_22);

figure()
hold on
yyaxis left
plot([min(kh), max(kh)],[0,0] ,'-k','LineWidth',0.8)
plot(kh,beta_21./beta_212,'-b','DisplayName','|A|^2 D_t A','LineWidth',1.5)
plot([crit, crit], [min(beta), max(beta)],':k','LineWidth',1.2)
ylabel('$\beta_{21}/\beta_{21}^\infty$','interpreter','latex','FontSize',15)
set(gca,'YColor','blue')
ylim([-15,1.2])
yyaxis right
plot(kh,beta_22./beta_222,'-r','DisplayName','A D_t |A|^2','LineWidth',1.5)
set(gca,'YColor','red')
ylabel('$\beta_{22}/\beta_{22}^\infty$','interpreter','latex','FontSize',15)
hold off
ylim([-15,1.2])
xlabel('$kh$','interpreter','latex','FontSize',15)
xticks(sort([crit,linspace(0,max(kh),6)]))
axes('Position',[.45 .18 .4 .4])
title('Real values around $kh=1.36$')
box on
hold on
yyaxis left
plot([min(kh), max(kh)],[0,0] ,'-k','LineWidth',0.8)
plot(kh,beta_21,'-b','DisplayName','\beta_{21}','LineWidth',1.5)
plot(kh,beta_212.*oness,'--b','DisplayName','\beta_{21} (DW)')
plot([crit, crit], [min(beta_21), max(beta_21)],':k','LineWidth',1.2)
set(gca,'YColor','blue')
% ylim([-15,1.01])
xlim([0.7,1.6])
yyaxis right
plot(kh,beta_222.*oness,'--r','DisplayName','$\beta_{22}$ (DW)')
plot(kh,beta_22,'-r','DisplayName','$\beta_{22}$','LineWidth',1.5)
set(gca,'YColor','red')
% ylabel('$A D_t |A|^2$','interpreter','latex')
hold off
% ylim([-15,1.01])
xlim([0.9,1.8])
xticks([kh(ix_beta21),crit,kh(ix_beta22)])
hold off
savefig('High order 1 normalized')

%%
sigma2 = (2*omega*cg).^2 - 4 .* kh .* sigma;
beta_paper_sigma=((sigma+1).^2.*kh-sigma).*((sigma-1).^2.*kh-sigma);
beta_dw=1-4.*kh;
figure()
hold on
plot(kh,sigma2)
plot(kh,beta_paper_sigma)	
plot(kh,beta_dw)
xlabel('$kh$','interpreter','latex','FontSize',15)

%%
val_cross= detectzerocross(alpha3);
figure()
hold on
% yyaxis left
plot(kh,alpha3,'-b','LineWidth',1.5)
plot(kh,0.*oness,'--b')
plot([crit, crit], [min(alpha3), max(alpha3)],':k','LineWidth',1.2)
ylabel('Third order dispersion $\alpha_3$','interpreter','latex','FontSize',15)
set(gca,'YColor','blue')
% ylim([-15,10])
xticks(sort([kh(val_cross),crit,linspace(0,max(kh),6)]))
hold off
xlabel('$kh$','interpreter','latex','FontSize',15)

%% D hilbert term
% val_cross= detectzerocross(alpha);
beta_g=((sigma+1).^2.*kh-sigma).*((sigma-1).^2.*kh-sigma);
mu_g=2.*sigma./omega.*(2.*omega-k.*cg.*(sigma.^2-1));
D=-h.*(omega).*k.*mu_g./(2.*sigma.*beta_g);
	
figure()
hold on
% yyaxis left
plot(kh,D,':b','LineWidth',1.5,'DisplayName','D_2(k_0,H)')
plot(kh,H3./(mu_g.*k./(4.*sigma.*cg.^2)),'-b','LineWidth',1.5,'DisplayName','D(k_0,H)')
plot(kh,0.5*omega*oness,':k','LineWidth',1.5,'DisplayName','\omega_0/2')
% plot(kh,Dys_hilb_slun,'--b')
% plot([crit, crit], [min(alpha), max(alpha)],':k','LineWidth',1.2)
ylabel('D Hilbert term','interpreter','latex','FontSize',15)
% set(gca,'YColor','blue')
legend()
% ylim([-15,10])
% xticks(sort([kh(val_cross),crit,linspace(0,max(kh),6)]))
hold off
xlabel('$kh$','interpreter','latex','FontSize',15)


%% D hilbert term

figure()
set_latex_plots(groot)
hold on
% yyaxis left
plot(kh,H3,'-b','LineWidth',1.5,'DisplayName','$D\mu_g k/(4\sigma \, c_g^2)$ ')
plot(kh,2.*k.^3./omega,':k','LineWidth',1.5,'DisplayName','$2k^3/\omega$')
plot(kh,H3./tanh(kh),':b','LineWidth',1.5,'DisplayName','$D\mu_g k/(4\sigma\, c_g^2)*1/tanh(kh)$ ')
% plot(kh,Dys_hilb_slun,'--b')
% plot([crit, crit], [min(alpha), max(alpha)],':k','LineWidth',1.2)
ylabel('$D\frac{\mu_g \, k}{4\sigma \,c_g^2}$','interpreter','latex','FontSize',15)
set(gca,'YColor','blue')
legend()
ylim([1.9,2.3])
% xticks(sort([kh(val_cross),crit,linspace(0,max(kh),6)]))
hold off
xlabel('$kh$','interpreter','latex','FontSize',15)

axes('Position',[.45 .45 .45 .3])
box on
hold on
plot(kh,H3,'-b','LineWidth',1.5,'DisplayName','D\mu_g k/(4\sigma.c_g^2) ')
plot(kh,2.*k.^3./omega,':k','LineWidth',1.5,'DisplayName','2k^3/\omega')
plot(kh,H3./tanh(kh),':b','LineWidth',1.5,'DisplayName','D\mu_g k/(4\sigma.c_g^2)*1/tanh(kh) ')

hold off
% ylabel('$|A|^2 D_t A$','interpreter','latex')
set(gca,'YColor','blue')
ylim([1.9,10])
xlim([0.7,5])
%%
figure()
set_latex_plots(groot)
hold on
% yyaxis left
plot(kh,H3./H32,'-b','LineWidth',1.5,'DisplayName','$H_3/H_3^\?nfty$ ')
plot([crit, crit], [min(H3./H32), max(H3./H32)],':k','LineWidth',1.2)
xticks(sort([crit,linspace(0,max(kh),20)]))

% plot(kh,2.*k.^3./omega,':k','LineWidth',1.5,'DisplayName','$2k^3/\omega$')
% plot(kh,H3./tanh(kh),':b','LineWidth',1.5,'DisplayName','$D\mu_g k/(4\sigma\, c_g^2)*1/tanh(kh)$ ')
% plot(kh,Dys_hilb_slun,'--b')
% plot([crit, crit], [min(alpha), max(alpha)],':k','LineWidth',1.2)
ylabel('$H_3/H_3^\infty$','interpreter','latex','FontSize',Fsz)
set(gca,'YColor','blue')
% legend()
xlim([0.7,5])
ylim([0.5,2.3])
plot(kh,oness,'--b')

% xticks(sort([kh(val_cross),crit,linspace(0,max(kh),6)]))
hold off
xlabel('$kh$','interpreter','latex','FontSize',Fsz)

axes('Position',[.45 .55 .45 .3])
box on
hold on
plot(kh,log10(H3./H32),'-b','LineWidth',1.5,'DisplayName','$H_3/H_3^\?nfty$ ')
plot(kh,log10(oness),'--b')
hold off
ylabel('$\log {H_3/H_3^\infty}$','interpreter','latex','FontSize',Fsz-5)
set(gca,'YColor','blue')
ylim([-0.1,.3])
xlabel('$kh$','interpreter','latex','FontSize',Fsz-5)
% xlim([0.7,5])

%% mu_g and beta lim
% val_cross= detectzerocross(alpha);
figure()
hold on
% yyaxis left
plot(kh,(2.*sigma./omega).*(2.*omega-k.*cg.*(sigma.^2-1)),'-b','LineWidth',1.5,'DisplayName','\mu_g')
plot(kh,4.*oness,':b','LineWidth',1.5,'DisplayName','\mu_g lim')
plot(kh,(4.*k.*sigma.*(cg.^2-h)),'-r','LineWidth',1.5,'DisplayName','\beta_g')
plot(kh,(oness'-4.*kh),':r','LineWidth',1.5,'DisplayName','\beta_g lim')
set(gca,'YColor','blue')
legend()
% ylim([-15,10])
% xticks(sort([kh(val_cross),crit,linspace(0,max(kh),6)]))
hold off
xlabel('$kh$','interpreter','latex','FontSize',15)
%% Easy approximation
figure()
x=-10:0.01:10;
set_latex_plots(groot)
hold on

plot(x,1./(tanh(x)),'-r','LineWidth',1.5,'DisplayName','$\coth (kh)$')
plot(x,1./x+sign(x),'--b','LineWidth',1.5,'DisplayName','$1/kh+sgn (k)$')
% plot(x,1./x+sign(x)-sign(x).*abs(x).^(-4/5),':b','LineWidth',1.5,'DisplayName','1/kh+sgn(k)')

xlabel('$kh$','interpreter','latex','FontSize',15)
ylim([-5,5])
legend()
hold off
%%
val_cross= detectzerocross(alpha4);
figure()
hold on
plot(kh,alpha4,'-k','LineWidth',1.5)
plot(kh,0.*oness,'--k')
plot([crit, crit], [min(alpha4), max(alpha4)],':k','LineWidth',1.2)
xticks(sort([kh(val_cross),linspace(0,max(kh),6)]))
ylabel('$\alpha_4$','interpreter','latex','FontSize',15)
xlabel('$kh$','interpreter','latex','FontSize',13)
hold off
%%

function bla=set_latex_plots(groot)
	set(groot,'defaulttextinterpreter','latex');
	set(groot,'defaultAxesTickLabelInterpreter','latex');  
	set(groot,'defaultLegendInterpreter','latex');
	hAxes.TickLabelInterpreter = 'latex';
end

function zerocross = detectzerocross(x)
	test_cross=x(1:end-1).*x(2:end);
	zerocross= find(test_cross<0);
end