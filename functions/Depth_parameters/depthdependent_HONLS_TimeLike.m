function [h,k,sigma,cg,cp,alpha,beta,alpha_3,alpha4,beta_21,beta_22,mu,muintexp,Dys_hilb,H3] = depthdependent_HONLS_TimeLike(omega,kh,FOD,ON_OFF_coef)
     %%
    % input params 
    % omega is the angular frequency in units of g^{1/2}
    % hk is the current value of h(x)k(x)
    % x is the position along the propagation distance
    % 
    % output params
    % h(x) depth
    % k(x) wavenumber
    % sigma = tanh(kh)
    % cg = group velocity
    % cp = phase velocity
    
    %
    % The shoaling is trivial, see Djordjevic&Redekopp ZAMP 1978
    % muintexp is the exp of the integral of mu(x) exp(int(mu))
    %
    % The parameters are defined as in Sedletsky JETP Lett. 97, 180-193 (2003)
    % where the envelope of the surface elevation is used and the expansion
    % to fourth order is performed
    %
    % alpha = group velocity dispersion
    % alpha_3 = third order dispersion (=0 in the limit of deep water)
    % alpha4 = fourth order dispersion
    % beta = cubic nonlinear coefficient
    % beta_21 = coefficient of |A|^2 D_t A
    % beta_22 = coefficient of A D_t |A|^2
    %
    % Andrea ARMAROLI, GAP UniGE, 25/02/2019
    %%
    
    % auxiliary quantities
% 	k=zeros(length(h),1)';
% for j=1:length(h)
%     syms k0_vpa   
%     k(j) = -vpasolve(k0_vpa*tanh(k0_vpa*abs(h(j))) == (omega)^2, k0_vpa);
%     k(j) = double(k(j)); % convert sym to number
% end
  % auxiliary quantities
    sigma = tanh(kh);
    k = omega.^2./sigma;
    h = kh.*sigma./omega.^2;
    % group and phase speed
    cp = omega./k;
    cg = (sigma + kh.*(1-sigma.^2))/2./omega;
	se=sech(kh);
	co=cosh(kh);
	co2=cosh(2.*kh);
	co3=cosh(3.*kh);
	co4=cosh(4.*kh);
	co5=cosh(5.*kh);
	co6=cosh(6.*kh);
	co7=cosh(7.*kh);
	co8=cosh(8.*kh);
	co9=cosh(9.*kh);
	
%     d_cg=-(kh.*se.^2+sigma).^2./((4.*k.*sigma).^(3/2))+(2.*h.*se.^2-2.*h^2.*k.*se^2.*sigma)./(2.*sqrt(k.*sigma));
	
    % beta in the paper
%     sigma2 = (2*omega*cg).^2 - 4 .* kh .* sigma; %checked
	sigma2 = ((sigma+1).^2.*kh-sigma).*((sigma-1).^2.*kh-sigma); %from Maura's correction
    sigmaq = sigma.^2-1;	%checked
    % mu in the paper
    sigma3 = sigmaq.^2.*kh - sigma.*(sigma.^2-5);
    % NLS quantities
    % beta_21''/2 = omega''/Cg^3/2

% 	    alpha = -(1 - h./cg.^2.*(1-kh.*sigma).*(1-sigma.^2))./(2*omega.*cg);   %  OLD
    alpha = cp.*h./sigma.*(1-sigma.^2).*(1-kh.*sigma)-cg.^2/omega;%from Maura's correction
    alpha = alpha./cg.^3/2;

	% THIS FORMULA IS NOT CORRECT!!!
    % alphaT = -omega'''/6
%      alphaT = omega./(48.*k.^3.*sigma.^3).*((sigma.^2-1).*(15.*sigma.^4-2.*sigma.^2 + 3).*kh.^3 - ...
%          3.*sigma.*(sigma.^2-1).*(3*sigma.^2+1).*kh.^2-...
%          3.*sigma.^2.*(sigma.^2-1).*kh - 3.*sigma.^3);
    
%      alphaT = (3*(sigma - kh .*sigmaq).^3 + 12*h.*sigmaq .*(sigma - kh.*sigmaq) *omega.^2 - ...
%          4*h.^2    .*sigmaq.*(-3*sigma + kh.*(1+3*sigma.^2))*omega.^5)./(48.*omega.^5);
%     changing to space evolution
%     TOD
%     beta3/6 = alphaT/cg^4 - 2*alpha^2*cg   
%     alpha = alphaT./cg.^4 - 2 * alpha.^2.*cg;   
%     
% 	
	alphaTold = omega./(48.*k.^3.*sigma.^3).*((sigma.^2-1).*(15.*sigma.^4-2.*sigma.^2 + 3).*kh.^3 - ...
          3.*sigma.*(sigma.^2-1).*(3*sigma.^2+1).*kh.^2-...
          3.*sigma.^2.*(sigma.^2-1).*kh - 3.*sigma.^3);
    alphaT = -alphaTold;  
	alpha_3 = alphaT./cg.^4 - 2 * alpha.^2.*cg;   
    
	
    % FOD
    % beta4/24

    if FOD ~=0
%         alpha4 = -1./(3*128*omega.^7.*cg.^7).*h.*sigmaq.*(...
%             - 3*kh.^4.*sigmaq.^4.*(1-5.^kh)  ...
%             - 4.*kh.^sigma.^3.*sigmaq.*(1-kh.*sigma).*(3-4*kh.*sigma).*(5-8.*kh.*sigma)...
%             - 4.*kh.^3.*sigma.*sigmaq.^3.*(15+kh.*sigma.*(-31+24.*kh.*sigma))...
%             + sigma.^4.*(-3 + kh.*sigma.*(39+8.*kh.*(-7+2*kh.*sigma)))...
%             + 6.*kh.^2.*sigma.^2.*sigmaq.^2.*(-19+kh.*sigma.*(51+4*kh.*sigma.*(-11 + 4.*kh.*sigma))));
	alpha4_num_1 = 64.*exp(14.*kh).*co.^3;
	alpha4_num_2 = 2.*(-9+576.*kh.^2+640.*kh.^4).*co+4.*(3+140.*kh.^2+32.*kh.^4).*co3;
	alpha4_num_3 = 3.*(-4.*co5+co7+co9);
	alpha4_num_4 = kh.*(384.*co.^4.*(27+13.*co2).*sinh(kh).^3+8.*kh.*(-2.*(55+104.*kh.^2).*co5-(97+64.*kh.^2).*co7-7.*co9+...
		4.*kh.*(81+24.*kh.^2+168.*co2+2.*(79+48.*kh.^2).*co4+72.*co6 +co8).*sinh(kh))  );
	alpha4_den=3.*((-1+exp(4.*kh)+4.*exp(2.*kh).*kh).^7);
	alpha4=alpha4_num_1.*(alpha4_num_2-alpha4_num_3+alpha4_num_4)./alpha4_den;
	else 
        alpha4 = zeros(size(kh));
    end
    % cubic nonlinearity
    beta = k.^2.*omega./(16*sigma.^4.*cg).*(9 - 10*sigma.^2 + 9*sigma.^4-...
        2.*sigma.^2.*cg.^2./(h-cg.^2).*(4*cp.^2./cg.^2 + 4* cp./cg.*(1-sigma.^2) + h./cg.^2.*(1-sigma.^2).^2));
    % 
    % 4O coefficients
    Q41tilde = 1./(16.*sigma.^5.*sigma2).*((2*sigma.^6 - 11 .* sigma.^4 - 10*sigma.^2 + 27).*sigmaq.^3.*kh.^3 - ...
        sigma.*sigmaq.*(6.*sigma.^8-21.*sigma.^6 +9 .*sigma.^4 - 43.*sigma.^2 + 81).*kh.^2 - ...
        sigma.^3.*(sigma.^2+1).*(2*sigma.^4-7*sigma.^2-27)+...
        sigma.^2.*kh.*(6*sigma.^8-15*sigma.^6-77*sigma.^4+71*sigma.^2 -81));
    Q42tilde = 1./(32.*sigma.^5.*sigma2).*(-(4*sigma.^6 + 5.* sigma.^4 - 10*sigma.^2 + 9).*sigmaq.^3.*kh.^3 + ...
        sigma.*sigmaq.*(12.*sigma.^8 - 45.*sigma.^6 +71 .*sigma.^4 - 15.*sigma.^2 + 9).*kh.^2 + ...
        sigma.^3.*(4*sigma.^6 - 43*sigma.^4 + 118*sigma.^2 + 9) ...
        -sigma.^2.*kh.*(12*sigma.^8-93*sigma.^6+215*sigma.^4-111*sigma.^2+9));
    % Slunyaev correction checked by Maura
    DeltaS = -1./(16.*sigma.^3.*sigma2).*(sigmaq.^4.*(3*sigma.^2+1).*kh.^3 ...
        - sigma.*sigmaq.^2.*(5.*sigma.^4 - 18 *sigma.^2 - 3).*kh.^2 ...
        + sigma.^2.*sigmaq.^2.*(sigma.^2-9).*kh ...
        + sigma.^3.*sigmaq.*(sigma.^2-5));
    % overall |A|^2 D_x A term 
    Q41 = 1./(32.*sigma.^5.*sigma2.^2).*(...
        sigmaq.^5.*(3.*sigma.^6-20.*sigma.^4-21.*sigma.^2 +54).*kh.^5 ...
        - sigma.*sigmaq.^3.*(11.*sigma.^8-99.*sigma.^6-61.*sigma.^4+7.*sigma.^2+270).*kh.^4  ...
        + 2*sigma.^2.*sigmaq.*(7.*sigma.^10-58.*sigma.^8+38.*sigma.^6+52.*sigma.^4-181.*sigma.^2+270).*kh.^3  ...
        - 2.*sigma.^3.*(3.*sigma.^10 + 18.*sigma.^8 - 146.*sigma.^6 - 172.*sigma.^4 + 183.*sigma.^2 - 270).*kh.^2  ...
        - sigma.^4.*(sigma.^8 - 109.*sigma.^6 + 517.*sigma.^4 + 217.*sigma.^2 + 270).*kh ...
        + sigma.^5.*(sigma.^6 - 40.*sigma.^4 + 193.*sigma.^2 + 54) ...
        ) + DeltaS;
    betaT = omega.*k.*Q41;   
    % change to time evolution variable
    % |A|^2 D_t A term
    beta_21 = betaT./(cg.^2) - 4 * alpha.*cg.*beta;  %beta doesn't work for deep limit
    
    % A^2 D_x A* term
    Q42 = 1./(32.*sigma.^5.*sigma2.^2).*(...
        - sigmaq.^5.*(3.*sigma.^6 + 7.*sigma.^4-11.*sigma.^2 + 9).*kh.^5 ...
        + sigma.*sigmaq.^3.*(11.*sigma.^8-48.*sigma.^6 + 66.*sigma.^4+8.*sigma.^2+27).*kh.^4  ...
        - 2*sigma.^2.*sigmaq.*(7.*sigma.^10-79.*sigma.^8+ 282.*sigma.^6 - 154.*sigma.^4 - sigma.^2+9).*kh.^3  ...
        + 2.*sigma.^3.*(3.*sigma.^10 - 63.*sigma.^8 + 314.*sigma.^6 - 218.*sigma.^4 + 19.*sigma.^2 + 9).*kh.^2  ...
        + sigma.^4.*(sigma.^8 + 20.*sigma.^6 - 158.*sigma.^4 - 28.*sigma.^2 - 27).*kh  ...
        - sigma.^5.*(sigma.^6 - 7.*sigma.^4 + 7.*sigma.^2 - 9) ...
        ) - DeltaS;
    beta_22T = omega.*k.*Q42;
    % change to time evolution variable
    % A D_t |A|^2 term
    beta_22 = beta_22T./cg.^2 - 2.*alpha.*cg.*beta;

	% shoaling (depth-related loss/amplification)    
    mu = (1-sigma.^2).*(1-kh.*sigma)./(sigma+kh.*(1-sigma.^2));
    muintexp = ((2.*kh + sinh(2*kh))./cosh(kh).^2).^0.5;

	D_hilb_num=2+(1-sigma.^2).*cg./cp;
	D_hilb_den=kh-sigma.*cg.^2./(cp.^2);
	D_hilb_general=kh.*(D_hilb_num./D_hilb_den).*omega./(4.*sigma);
	nu_g=((sigma+1).^2.*kh-sigma).*((sigma-1).^2.*kh-sigma);
	mu_g=2.*sigma./omega.*(2.*omega-k.*cg.*(sigma.^2-1));
	D=-h.*(omega).*k.*mu_g/(2.*sigma.*nu_g);
	
	alpha = alpha.*ON_OFF_coef(1);
	beta = beta.*ON_OFF_coef(2);
	alpha_3 = alpha_3.*ON_OFF_coef(3);
    beta_21 = beta_21.*ON_OFF_coef(4);
    beta_22 = beta_22.*ON_OFF_coef(5);
	Dys_hilb=2*k.^3/omega.*ON_OFF_coef(6);
	H3=(mu_g.*k./(4.*sigma.*cg.^2)).*D_hilb_general.*ON_OFF_coef(6);
end