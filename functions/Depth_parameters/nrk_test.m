clc
close all
clear all

addpath(genpath(fullfile('functions','Depth_parameters')));
nt=1000;
omegaxis=linspace(-15,15,nt+1);
g=9.81;
% nt=length(omegaxis);
h=0.2;

kaxis=zeros(nt+1,1);
tol=0.0001;
tic
for j=1:nt+1
	w0=omegaxis(end+1-j);
	k0_deep=w0^2/g;
	kaxis(j) = nr_k0(k0_deep,w0,h,g,tol); 
end
toc
% kaxis

figure()
plot(omegaxis,kaxis.\omegaxis'.^2)
ylabel('k/ \omega^2')
xlabel('\omega')