function [color_s,color_Ns,color_utr,color_main,color_sideband,color_2sideband]=color_language_3wave()
	color_s=[100/255,180/255,170/255]; %runs with step (teal)
	color_Ns=[249/255,113/255,113/255]; %runs without step (coral) 
	% palette from https://www.color-hex.com/color-palette/71855
	color_utr=[0.631,0.733,0.333]; %green sideband summ
	color_main=[0.141,0.118,0.306]; %blue main frequency
	color_sideband=[0.855,0.675,0]; %yellow 1st sideband
	color_2sideband=[0.525,0.20,0.435]; %purple 2nd sideband
	%palette from https://paletton.com/#uid=72f1T1kcUjW2Idr7rgnhuoHoWtfkfovoamNfcXDUhFrkjPmukkqkGgyEuips7mGe2oZ7aksesebnmTi+ptAyrlQ2qR
end

