function [dispersion_color,nonlinear_color]=color_language_NLScoefs()
%this are the new colour mtlab colour defaults. 
	dispersion_color=[0, 0.4470, 0.7410]; %dispersion
    nonlinear_color=[0.6350, 0.0780, 0.1840]; %nonlinearity. 	
end
	