function [S,W]=HitSpek3(z,n,m,sfr,skl);
% HitSpek2 : generate wave spectrum from time signal
%
zf = fft(z);
R  = zf.*conj(zf)/n;
fr = (0:n-1)/n*sfr;
P  = 2*R/sfr;
w  = hamming(m) ;                
w  = w/sum(w) ;                  
w  = [w(ceil((m+1)/2):m);zeros(n-m,1);w(1:ceil((m+1)/2)-1)];  
w    = fft(w) ;                    
pavg = fft(P) ;                 
pavg = ifft(w.*pavg) ; 
S = abs(pavg(1:ceil(n/2)));
F = fr(1:ceil(n/2));
S=S/(2*pi)*sqrt(skl);% Spectral (m^2.s)
W=2*pi*F/sqrt(skl); % w (rad/s)
end