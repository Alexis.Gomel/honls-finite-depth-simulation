function wave=cobagelombang4(Hw,Tw,gama,t,B0,skl)
% caobagel3 : generates wave time signal from wave spectrum, 
% JONSWAP etc
% 
% Baharuddin Ali, 
% email : b4li313@gmail.com
% Kediri-East Java-Indonesia
fp = 2*pi/Tw;
w=linspace(0.2*fp,5*fp,130);
% w=linspace(0.2,2.5,80);
delta_w = w(2)-w(1);  
w = w + delta_w .* rand(1,length(w)); % random (uniform) selection of frequencies
w3=w;
%----- Jonswap spectrum ------
fp    = 2*pi/Tw;
fac1  = (320*Hw^2)/Tw^4;
sigma = (w<=fp)*0.07+(w>fp)*0.09;
Aa    = exp(-((w/fp-1)./(sigma*sqrt(2))).^2);
fac2  = w.^-5;
fac3  = exp(-(1950*w.^-4)/Tw^4);
fac31 = exp(-5/4*(w/fp).^-4);
fac4  = gama.^Aa;
S     = fac1.*fac2.*fac3.*fac4;
%--------------------------------           
% skl  = 50;   % use scale model to reduce time consume in calculation..!!
% tend = length(t); % example : about 3 hours for model scale 1:50
sfr   =  abs(1/diff(t(1:2)));   % sampling frequency (Hz)
% st=t*sqrt(skl);
st=t;

phi = 2*pi*(rand(1,length(w))-0.5); % random phase of ith frequency
A = sqrt(2*S.*delta_w); % amplitude of ith frequency
for i = 1:length(st)
    wave(i) = sum(A .* cos(w*st(i) + phi));
end
[S2,W2]=HitSpek3(wave',length(wave),800,sfr,skl); % 400 :hamming variabel, custom, can be modified
smax=(max(S)<=max(S2))*max(S2)+(max(S)>max(S2))*max(S);
figure()
subplot(2,1,1)
plot(W2,S2,w3,S,'r');xlabel('w (rad/s)');ylabel('Spectral (m^2.s)');
legend('measured','theoretical');
grid;
% axis([w3(1) w3(end) 0 smax*1.2]);
subplot(2,1,2)
plot(t,wave);xlabel('t (sec)');ylabel('Z_a (m)');grid;
% axis([0 t(end) -inf*1.2 inf*1.2]);

end