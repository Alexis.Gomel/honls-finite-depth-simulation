function kh = khfun4(x,kh0,khf,xvarstart,xvarstop,xslope)
    %%
    % Gradual variation in the form of linear slope
    % INPUT
    % x point in the propagation distance
    % kh0 initial kh
    % khf final kh
    % xvarstart/stop initial/final coordinate of kh variation
    % xslope, a normalization for the transition
    % 
    % OUTPUT
    % kh(x)
    %%
    kh = zeros(size(x));
    kh(x<=xvarstart) = kh0;
    kh(x>xvarstop) = khf;
    kh(x>xvarstart & x<=xvarstop) = kh0  + (khf-kh0)./(xvarstop-xvarstart).*(x(x>xvarstart & x<=xvarstop)-xvarstart);
    
end