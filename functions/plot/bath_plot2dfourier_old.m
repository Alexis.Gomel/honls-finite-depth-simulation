function f=bath_plot2dfourier_old(K,H,uspectrum,omegaxis,Omega,omega0,B0,X,Lx,UdBth,cmap,Fsz)

f=figure;
set_latex_plots(groot)
g0=9.81;
crit=1.363;
[ d, ix ] = min( abs( K.*H - crit ) );%find the critical value index
hold on
% UdBth = -30;
	Usp = 20*log10(abs(uspectrum.')*sqrt(g0)*(1/(2*pi))/B0);
	Usp = (Usp>UdBth).*(Usp-UdBth) +  UdBth;
	imagesc(omegaxis*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi),X,Usp);
	set(gca,'YDir','normal','FontSize',Fsz-1);
	xlim([-4.5*omega0*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi),4.5*omega0*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi)]);
	ylim([min(X),max(X)]);
	xlabel('$f$ [Hz]','fontsize',Fsz);
	ylabel('$X$ [m]','fontsize',Fsz);
	colormap(cmap)
	colorbar;
	try
		plot(omegaxis*sqrt(g0)/(2*pi),-X0.*ones(max(size(omegaxis*sqrt(g0)/(2*pi)))),'--g')
		plot(omegaxis*sqrt(g0)/(2*pi),xvarstart.*ones(max(size(omegaxis*sqrt(g0)/(2*pi)))),'color',[0.87,0.74,0.53],'LineStyle','--')
		plot(omegaxis*sqrt(g0)/(2*pi),xvarstop.*ones(max(size(omegaxis*sqrt(g0)/(2*pi)))),'color',[0.87,0.74,0.53],'LineStyle','--')
	end
	if K(ix)*H(ix)<(crit+0.02) & K(ix)*H(ix)>(crit-0.02);
		plot(omegaxis*sqrt(g0)/(2*pi),X(ix).*ones(max(size(omegaxis*sqrt(g0)/(2*pi)))),'r','LineStyle',':')
	end
	axis([-4.5*omega0*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi) 4.5*omega0*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi)  0 Lx])
	hold off
	ax1 = gca; % current axes
	ax1_pos = ax1.Position; % position of first axes
	ax2 = axes('Position',ax1_pos,...
		'XAxisLocation','top',...
		'YAxisLocation','right',...
		'Color','none');
	line(H.*K,X,'Parent',ax2,'Color',[0.87,0.74,0.53],'LineWidth',1.2)
	ax2.YLim=([0. Lx]);
	set(gca,'xlim',[0 5*max(H.*K)],'ytick',[] ,'xtick',[ 0 min(H.*K)  max(H.*K)*1.0000001 ],'xticklabel',{'hk:' min(H.*K)  max(H.*K)})

end