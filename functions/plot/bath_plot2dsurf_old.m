function f=bath_plot2dsurf_old(K,H,t,tplot,X,Lx,u,cmap,Fsz)
f=figure;
set_latex_plots(groot)
g0=9.81;
crit=1.363;
[ d, ix ] = min( abs( K.*H - crit ) );%find the critical value index
hold on
imagesc(t(tplot)/sqrt(g0),X,abs(u(tplot,:).'));
set(gca,'YDir','normal','FontSize',Fsz-1);
xlabel('$\tau$ [s] ','fontsize',Fsz);
ylabel('$\xi$ [m]','fontsize',Fsz);
% if IC=='Peregrine Soliton'
%     caxis([0 B0*3]);
% end
colormap(cmap)
shading interp;
colorbar
% colormap jet;
%colormap hot;
try
    plot(t(tplot)/sqrt(g0),-X0.*ones(max(size(t(tplot)/sqrt(g0)))),'--g')
    plot(t(tplot)/sqrt(g0),xvarstart.*ones(max(size(t(tplot)/sqrt(g0)))),'color',[0.87,0.74,0.53],'LineStyle','--')
    plot(t(tplot)/sqrt(g0),xvarstop.*ones(max(size(t(tplot)/sqrt(g0)))),'color',[0.87,0.74,0.53],'LineStyle','--')
end
if K(ix)*H(ix)<(crit+0.02) & K(ix)*H(ix)>(crit-0.02);
    plot(t(tplot)/sqrt(g0),X(ix).*ones(max(size(t(tplot)/sqrt(g0)))),'r','LineStyle',':')
end
axis([min(t(tplot)/sqrt(g0)) max(t(tplot)/sqrt(g0))  0 Lx])
hold off
ax1 = gca; % current axes
ax1_pos = ax1.Position; % position of first axes
ax2 = axes('Position',ax1_pos,...
    'XAxisLocation','top',...
    'YAxisLocation','right',...
    'Color','none');
line(H.*K,X,'Parent',ax2,'Color',[0.87,0.74,0.53],'LineWidth',1.2)
ax2.YLim=([0. Lx]);
set(gca,'xlim',[0 5*max(H.*K)],'ytick',[] ,'xtick',[ 0 min(H.*K)  max(H.*K)*1.0000001 ],'xticklabel',{'hk:' min(H.*K)  max(H.*K)})
end