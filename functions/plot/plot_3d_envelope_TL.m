function f=plot_3d_envelope_TL(u,t,X,B0,tplot,cmap,Normalized,Fsz)
	f=figure('name','3d envelope');
	
	set_latex_plots(groot)
% 	Fsz=15; %Fontsize
	g0=9.81;
	
	if Normalized==1
		 s=surf(t(tplot)/sqrt(g0),X,abs(u(tplot,:).')/B0);
	else
		s=surf(t(tplot)/sqrt(g0),X,abs(u(tplot,:).')/B0);
	end
	set(s, 'EdgeColor', 'black','LineWidth',1.,'MeshStyle','None');
	s.FaceColor = 'interp';
	colormap(cmap)	
    set(gca,'YDir','normal','FontSize',Fsz-1);
    xlabel('$T$ [s]','fontsize',Fsz);
    ylabel('$X$ [m]','fontsize',Fsz);
	
	if Normalized==1
    zlabel('$|B|/B_0$','fontsize',Fsz);
	else
    zlabel('$|B|$','fontsize',Fsz);

	end
    % shading interp;
    % colormap hot;
end