function f=plot_Amplitude_Evolution_first_last_TL(K,H,X,Bback,Bpert,B0,t,u,tplot,Lx,xvarstart,xvarstop,cmap,Normalized)
	g0=9.81;
	s_color=[48,117,222]./255; %color at the start
    e_color=[223,78,192]./255; %color at the end
% 
% 	s_color=[2.579861074778473928e-01 2.048213417591974728e-01 6.260687943604991146e-01]; %color at the start
%     e_color=[9.806011989875685897e-01 5.897331838357899869e-01 2.509319710696591987e-01]; %color at the end

	Fsz=15; %Fontsize
	
	f=figure('name','Amplitude evolution','position',[20 20 550 550]);
	set_latex_plots(groot)
    subplot(4,1,[1 3]);
	
	if Normalized==1
			s2=pcolor(t(tplot)/sqrt(g0),X,abs(u(tplot,:))'./B0) ;

	else
			s2=pcolor(t(tplot)/sqrt(g0),X,abs(u(tplot,:))' );

	end
% 	caxis([2*mean(mean((abs_env_part(edge_plot_time:end-edge_plot_time,1:end))))-0.9*max(max(abs_env_part(edge_plot_time:end-edge_plot_time,1:end)))   max(max(abs_env_part(edge_plot_time:end-edge_plot_time,1:end))) ]);
	set(s2, 'EdgeColor', 'black','LineWidth',1.,'MeshStyle','None');
	s2.FaceColor = 'interp';
% 	set(gcf, 'Position',  [110, 50, 900, 500])
	ylabel('$x$ [m]','Interpreter','latex','fontsize',Fsz);
	xlabel('$\tau$ [s]','Interpreter','latex','fontsize',Fsz);
	tp=t(tplot)./sqrt(g0);
%     s=imagesc(X,t(tplot)/sqrt(g0),abs(u(tplot,:)) );
    set(gca,'XDir','normal','FontSize',Fsz-1);

    shading interp;
	colormap(cmap)
    hc=colorbar;
	if Normalized==1
		title(hc,'$|B/B_0|$ [m]','Interpreter','latex','FontSize',Fsz-1)
	else
		title(hc,'$|B|$ [m]','Interpreter','latex','FontSize',Fsz-1)
	end
    ax1 = gca;
	xlim([tp(1),tp(end)])
	xt = xticks;

	boxlim=0.75;
	
    ax1_pos = ax1.Position ;
	ax1_pos = [ ax1_pos(1)   ax1_pos(2) boxlim  ax1_pos(4)] 
	set(ax1,'Position',ax1_pos);
	
	set(hc,'Units','normalized');
	set(hc,'Position', [ boxlim+0.15  ax1_pos(2)  0.03 ax1_pos(4) ]);
	
    subplot(4,1,4);
    ax2 = gca;
    ax2_pos = ax2.Position;
    ax2_pos =  [ ax2_pos(1)  .75*ax2_pos(2)  boxlim  .7*ax2_pos(4)];
    set(ax2,'Position',ax2_pos);
		
	X_set=1;
	[ d, ix ] = min( abs( X - X_set ) );%find the critical value index

	hold on
	plot(t(tplot)/sqrt(g0),abs(u(tplot,ix))','-','color',s_color,'LineWidth',1.8,'Displayname',sprintf('%.2f m',X(ix)));
	plot(t(tplot)/sqrt(g0),abs(u(tplot,end))','-','color',e_color,'LineWidth',1.5,'Displayname',sprintf('%.2f m',X(end)));
	hold off
	legend()
	mmax=max([max(abs(u(tplot,ix))),max(abs(u(tplot,end)))]);
	mmin=min([min(abs(u(tplot,ix))),min(abs(u(tplot,end)))]);
	ylim([0,mmax*1.1])
	yticks(sort([0 B0*Bback B0*(Bpert+Bback) mmax]))
%     ytickformat('%.1e');
	ylabel('$|B|$ [m]','Interpreter','latex','fontsize',Fsz);
	xlabel('$\tau$ [s]','Interpreter','latex','fontsize',Fsz);
	xlim([tp(1),tp(end)])
    ax2 = gca;
% 	set(gca,'XDir','normal','FontSize',Fsz-2);

    yyaxis(ax2, 'right');
% 	hold on
% 	plot(t(tplot)/sqrt(g0),K(ix)*abs(u(tplot,ix))','-r','LineWidth',1.5,'Displayname',sprintf('%.2f m',X(ix)));
% 	plot(t(tplot)/sqrt(g0),K(end)*abs(u(tplot,end))','-b','LineWidth',1.5,'Displayname',sprintf('%.2f m',X(end)));
% 	hold off
	ylabel('$\epsilon$ ','Interpreter','latex','fontsize',Fsz);
% 	set(gca,'Color',0.7*[1,1,1])
	set(gca,'YColor','k')
	mmax=max([max(K(ix)*abs(u(tplot,ix))),max(K(end)*abs(u(tplot,end)))]);
	ylim([0,mmax*1.1]);
    yticks(sort([0 K(ix)*B0*Bback K(ix)*B0*(Bpert+Bback) mmax]))
	xlim([tp(1),tp(end)])
    xticks(xt)
%     set(gca,'XDir','normal','FontSize',Fsz-2);

% 	ytickformat('%.2e');

%     yline=H.*K;
%     yline(end) = NaN;
%     c = yline;
%     patch(X,yline,c,'EdgeColor','interp','LineWidth',1.2,'MarkerFaceColor','flat');  
%     ylabel('hk','fontsize',15);
%     set(gca,'FontSize',15,'Ylim',[0.85*min(H.*K), 1.15*max(H.*K)],'ytick',[min(H.*K)  max(H.*K)*1.0000001],'XLim',[0. Lx],'xtick',[xvarstart,xvarstop]);
%     ax2.XAxis.FontSize = 8;
%     ylabel('hk ','Interpreter','latex','fontsize',15);
%     ytickformat('%.1f');
%     xtickformat('%.1f');
%     linkaxes([ax1, ax2], 'x');
%     yyaxis(ax2, 'right');
%     plot(X,-H,'--');
%     y2fontsize=12;
%     ylabel('h [m]','Interpreter','latex','fontsize',y2fontsize);
%     try
%     ax2.YAxis(2).TickValues=sort([min(-H),max(-H)]);
%     ax2.YAxis(2).FontSize = y2fontsize;
%     ax2.YAxis(2).Exponent=-2;
%     end   
%     ytickformat('%.1f');




end