function f=plot_Amplitude_Evolution_wbath_TL(K,H,X,t,u,tplot,Lx,xvarstart,xvarstop,cmap)
	crit= 1.363;
	g0=9.81;

    f=figure('name','Amplitude evolution','position',[20 20 550 550]);
	set_latex_plots(groot)

    [ d, ix ] = min( abs( K.*H - crit ) );%find the critical value index
%     pos1 = [0.05 0.3 0.9 0.65];
    subplot(4,1,[1 3]);
    % hold on 
	s2=pcolor(X,t(tplot)/sqrt(g0),abs(u(tplot,:)) );
% 	caxis([2*mean(mean((abs_env_part(edge_plot_time:end-edge_plot_time,1:end))))-0.9*max(max(abs_env_part(edge_plot_time:end-edge_plot_time,1:end)))   max(max(abs_env_part(edge_plot_time:end-edge_plot_time,1:end))) ]);
	set(s2, 'EdgeColor', 'black','LineWidth',1.,'MeshStyle','None');
	s2.FaceColor = 'interp';
% 	set(gcf, 'Position',  [110, 50, 900, 500])
	xlabel('$x$ [m]','Interpreter','latex','fontsize',13);
	ylabel('$\tau$ [s]','Interpreter','latex','fontsize',13);

%     s=imagesc(X,t(tplot)/sqrt(g0),abs(u(tplot,:)) );
    set(gca,'XDir','normal','FontSize',14);
%     ylabel('\tau [s] ','fontsize',16);
%     xlabel('x [m]','fontsize',16);
    % hold off
    shading interp;
	colormap(cmap)
    hc=colorbar;
    title(hc,'$|B|$ [m]')
  
	tp=t(tplot)./sqrt(g0);
	ax1 = gca;
	xlim([X(1),X(end)]);
% 	ylim([0,X(end)]);
	xt = xticks;
	boxlim=0.75;
    ax1_pos = ax1.Position ;
	ax1_pos = [ ax1_pos(1)   ax1_pos(2) boxlim  ax1_pos(4)] 
	set(ax1,'Position',ax1_pos);	
	set(hc,'Units','normalized');
	set(hc,'Position', [ boxlim+0.15  ax1_pos(2)  0.03 ax1_pos(4) ]);
	
	
	
	subplot(4,1,4);
    ax2 = gca;
    ax2_pos = ax2.Position;
    ax2_pos =  [ ax2_pos(1)  .75*ax2_pos(2)  boxlim  .7*ax2_pos(4)];
    set(ax2,'Position',ax2_pos);

    yline=H.*K;
    yline(end) = NaN;
    c = yline;
    patch(X,yline,c,'EdgeColor','interp','LineWidth',1.2,'MarkerFaceColor','flat');  
    ylabel('hk','fontsize',15);
	try
    set(gca,'FontSize',15,'Ylim',[0.85*min(H.*K), 1.15*max(H.*K)],'ytick',[min(H.*K)  max(H.*K)*1.0000001],'XLim',[0. Lx],'xtick',[xvarstart,xvarstop]);
	end
	ax2.XAxis.FontSize = 8;
    ylabel('hk ','Interpreter','latex','fontsize',15);
    ytickformat('%.1f');
    xtickformat('%.1f');
    linkaxes([ax1, ax2], 'x');
    yyaxis(ax2, 'right');
    plot(X,-H,'--');
    y2fontsize=12;
    ylabel('h [m]','Interpreter','latex','fontsize',y2fontsize);
    try
    ax2.YAxis(2).TickValues=sort([min(-H),max(-H)]);
    ax2.YAxis(2).FontSize = y2fontsize;
    ax2.YAxis(2).Exponent=-2;
    end   
    ytickformat('%.1f');
   
end