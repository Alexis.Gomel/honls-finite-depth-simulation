function f=plot_V_shock_v2(K,H,X,Bback,Bpert,B0,t,u,tplot,Lx,xvarstart,xvarstop,cmap)
		crit= 1.363;
		g0=9.81;
		middle=round(length(tplot)/2);
		
		tplot_left=tplot(1:middle);
		tp=t(tplot)./sqrt(g0);
		tplot_right=tplot(middle:end);
		tp_left=tp(1:middle);
		tp_right=tp(middle:end);
		phase=unwrap(angle(u(tplot,:)));
		chirp=diff(phase)./(diff(t(tplot))');
		t_chirp_plot=t(tplot);
	
	
		f=figure('name','Chirp Evolution','position',[20 20 550 550]);
		[ d, ix ] = min( abs( K.*H - crit ) );%find the critical value index
		pos1 = [0.05 0.5 0.9 0.9];
		ax1_left=subplot(5,2,[1 3 5]);
		hold on
		s2=pcolor(tp_left,X,abs(u(tplot_left,:))');        
		set(s2, 'EdgeColor', 'black','LineWidth',1.,'MeshStyle','None');
		title('Choose X position');
		s2.FaceColor = 'interp';
		colormap(cmap)
% 		hc=colorbar;
		xlim([tp_left(1),tp_left(end-1)])
		ylim([X(1), X(end)])
		ax_plt = gca;
		ax_plt_pos = ax_plt.Position ;
		hold off		
		%%%%%
		
		ax1_right=subplot(5,2,[2 4 6]);
		hold on
		s3=pcolor(tp_right,X,abs(u(tplot_right,:))');        
		set(s3, 'EdgeColor', 'black','LineWidth',1.,'MeshStyle','None');
		title('Choose X position');
		s3.FaceColor = 'interp';
		colormap(cmap)
% 		hc=colorbar;
		xlim([tp_right(1),tp_right(end-1)])
		ylim([X(1), X(end)])
		ax_plt2 = gca;
		ax_plt_pos2 = ax_plt2.Position ;
		start_end_pos = ginput(1); %input
		startpos_ti = start_end_pos(1,1);
	
		[ d, ix_y_right ] = min( abs( X - start_end_pos(1,2) ) );%find the critical value index
		[ d, ix_x_right ] = min( abs( t_chirp_plot(1:end-1)/sqrt(g0)- start_end_pos(1,1) ) );%find the critical value index
		x_init_right=X(ix_y_right);
		plot(t_chirp_plot(ix_x_right)/sqrt(g0),x_init_right,'*r');
		hold off	
		
		
		%%%%%
		
		axes(ax1_right)
		title('Choose right time');
		subplot(5,2,7);
		ax2 = gca;
		ax2_pos = ax2.Position;
		ax2_pos =  [ ax2_pos(1)  ax2_pos(2)  .84*ax_plt_pos2(3)  ax2_pos(4)];
% 		set(ax2,'Position',ax2_pos);%set to same width as first plot
		hold on
		plot(tp_right,abs(u(tplot_right,ix_y_right)),'-b','LineWidth',1.3)
		hold off
		ylabel('$\rho$','Interpreter','latex','fontsize',13);
		xlabel('$\tau$ [s]','Interpreter','latex','fontsize',13);
		xlim([tp_right(1),tp_right(end-1)])
		
		subplot(5,2,10);
		ax2 = gca;
		ax2_pos = ax2.Position;
		ax2_pos =  [ ax2_pos(1)  ax2_pos(2)  .84*ax_plt_pos2(3)  ax2_pos(4)];
% 		set(ax2,'Position',ax2_pos);%set to same width as first plot
		hold on
		plot(t_chirp_plot(1:end-1)/sqrt(g0),chirp(:,ix_y_right),'-b','LineWidth',1.3)
		hold off
		ylabel('$u$','Interpreter','latex','fontsize',13);
		xlabel('$\tau$ [s]','Interpreter','latex','fontsize',13);
		xlim([tp_right(1),tp_right(end-1)])

		
		%%%%%
		subplot(5,2,7);
		ax2 = gca;
		ax2_pos = ax2.Position;
		ax2_pos =  [ ax2_pos(1)  ax2_pos(2)  .84*ax_plt_pos(3)  ax2_pos(4)];
% 		set(ax2,'Position',ax2_pos);%set to same width as first plot
		hold on
		plot(tp_left,abs(u(tplot_left,ix_y_right)),'-b','LineWidth',1.3)
		hold off
		ylabel('$\rho$','Interpreter','latex','fontsize',13);
		xlabel('$\tau$ [s]','Interpreter','latex','fontsize',13);
		xlim([tp_left(1),tp_left(end-1)])
		
	
		%%%%
		subplot(5,2,10);
		ax2 = gca;
		ax2_pos = ax2.Position;
		ax2_pos =  [ ax2_pos(1)  ax2_pos(2)  .84*ax_plt_pos(3)  ax2_pos(4)];
% 		set(ax2,'Position',ax2_pos);%set to same width as first plot
		hold on
		plot(t_chirp_plot(1:end-1)/sqrt(g0),chirp(:,ix_y_right),'-b','LineWidth',1.3)
		hold off
		ylabel('$u$','Interpreter','latex','fontsize',13);
		xlabel('$\tau$ [s]','Interpreter','latex','fontsize',13);
		xlim([tp_left(1),tp_left(end-1)])
		%%%%%%
	

		start_end_pos = ginput(1);
		[ d, ix_x_right ] = min( abs( t_chirp_plot(1:end-1)/sqrt(g0)- start_end_pos(1,1) ) );%find the critical value index
		t_init_right=t_chirp_plot(ix_x_right)/sqrt(g0);
		V_right_plus=chirp(ix_x_right,ix_y_right)+(abs(u(ix_x_right,ix_y_right)))
		V_right_minus=chirp(ix_x_right,ix_y_right)-(abs(u(ix_x_right,ix_y_right)))

		axes(ax1_left)
		title('Choose left time');
		start_end_pos = ginput(1);
		[ d, ix_x_left ] = min( abs( t_chirp_plot(1:end-1)/sqrt(g0)- start_end_pos(1,1) ) );%find the critical value index
		t_init_left=t_chirp_plot(ix_x_left)/sqrt(g0); 
		V_left_plus=chirp(ix_x_right,ix_y_right)-(abs(u(ix_x_right,ix_y_right)))
		V_left_minus=chirp(ix_x_right,ix_y_right)-(abs(u(ix_x_right,ix_y_right)))

		%%%%%%
		axes(ax1_right)
		title('Shock soliton propagation');
		hold on
		plot(t_init_right,x_init_right,'*g');
		plot(t_init_right+V_right_plus.*(X(end)-x_init_right), X(end),'*g');
		plot([t_init_right t_init_right+V_right_plus.*(X(end)-x_init_right)],[x_init_right X(end)],'--r','LineWidth',1.8)
		plot([t_init_right t_init_right+V_right_minus.*(X(end)-x_init_right)],[x_init_right X(end)],':r','LineWidth',1.8)
		hold off
		
		axes(ax1_left)
		hold on
		plot([t_init_left t_init_left-V_left_plus.*(X(end)-x_init_right)],[x_init_right X(end)],'--r','LineWidth',1.8)
		plot([t_init_left t_init_left-V_left_minus.*(X(end)-x_init_right)],[x_init_right X(end)],':r','LineWidth',1.8)
		hold off
		
end