function f=plot_chirp_Evolution_first_last_TL(K,H,X,Bback,Bpert,B0,t,u,tplot,Lx,xvarstart,xvarstop,cmap)
	g0=9.81;

	
	s_color=[48,117,222]./255; %color at the start
    e_color=[223,78,192]./255; %color at the end
	
	Fsz=15; %Fontsize
	
    f=figure('name','Chirp Evolution','position',[20 20 550 550]);
	set_latex_plots(groot)
    subplot(4,1,[1 3]);

	phase=unwrap(angle(u(tplot,:)));
	chirp=diff(phase)./(diff(t(tplot))');

	t_chirp_plot=t(tplot);
	s2=pcolor(t_chirp_plot(1:end-1)/sqrt(g0),X,chirp' );
% 	caxis([2*mean(mean((abs_env_part(edge_plot_time:end-edge_plot_time,1:end))))-0.9*max(max(abs_env_part(edge_plot_time:end-edge_plot_time,1:end)))   max(max(abs_env_part(edge_plot_time:end-edge_plot_time,1:end))) ]);
	set(s2, 'EdgeColor', 'black','LineWidth',1.,'MeshStyle','None');
	s2.FaceColor = 'interp';
% % 	set(gcf, 'Position',  [110, 50, 900, 500])
	ylabel('$x$ [m]','Interpreter','latex','fontsize',Fsz);
	xlabel('$\tau$ [s]','Interpreter','latex','fontsize',Fsz);
	tp=t(tplot)./sqrt(g0);
% %     s=imagesc(X,t(tplot)/sqrt(g0),abs(u(tplot,:)) );
    set(gca,'XDir','normal','FontSize',Fsz-1);

    shading interp;
	colormap(cmap)
    hc=colorbar;
    title(hc,'chirp [1/s]')
    ax1 = gca;
	xlim([tp(1),tp(end-1)])
	m_chirp=max(abs(chirp),[],'all');
	caxis([-m_chirp m_chirp])

	ax1 = gca;
	xlim([tp(1),tp(end)])
	xt = xticks;

	boxlim=0.75;
	
    ax1_pos = ax1.Position ;
	ax1_pos = [ ax1_pos(1)   ax1_pos(2) boxlim  ax1_pos(4)] 
	set(ax1,'Position',ax1_pos);
	
	set(hc,'Units','normalized');
	set(hc,'Position', [ boxlim+0.15  ax1_pos(2)  0.03 ax1_pos(4) ]);   
	subplot(4,1,4);
  

	ax2 = gca;
    ax2_pos = ax2.Position;
    ax2_pos =  [ ax2_pos(1)  .75*ax2_pos(2)  boxlim  .7*ax2_pos(4)];
    set(ax2,'Position',ax2_pos);
		
	X_set=1;
	[ d, ix ] = min( abs( X - X_set ) );%find the critical value index

	hold on
	plot(t_chirp_plot(1:end-1)/sqrt(g0),chirp(:,ix)','-','color',s_color,'LineWidth',1.8,'Displayname',sprintf('%.2f m',X(ix)));
	plot(t_chirp_plot(1:end-1)/sqrt(g0),chirp(:,end)','-','color',e_color,'LineWidth',1.5,'Displayname',sprintf('%.2f m',X(end)));
	hold off
	legend('Location','northwest')
% 	mmax=max([max(abs(u(tplot,ix))),max(abs(u(tplot,end)))]);
% 	mmin=min([min(abs(u(tplot,ix))),min(abs(u(tplot,end)))]);
	ylim(1.3 .*[-m_chirp m_chirp])
% 	yticks(sort([0 B0*Bback B0*(Bpert+Bback) mmax]))
    ytickformat('%.1f ');
	ylabel('$u$ [1/s]','Interpreter','latex','fontsize',Fsz);
	xlabel('$\tau$ [s]','Interpreter','latex','fontsize',Fsz);
	xlim([tp(1),tp(end)])
%     ax2 = gca;
%     yyaxis(ax2, 'right');
% % 	hold on
% % 	plot(t(tplot)/sqrt(g0),K(ix)*abs(u(tplot,ix))','-r','LineWidth',1.5,'Displayname',sprintf('%.2f m',X(ix)));
% % 	plot(t(tplot)/sqrt(g0),K(end)*abs(u(tplot,end))','-b','LineWidth',1.5,'Displayname',sprintf('%.2f m',X(end)));
% % 	hold off
% 	ylabel('$\epsilon$ ','Interpreter','latex','fontsize',13);
% 	mmax=max([max(K(ix)*abs(u(tplot,ix))),max(K(end)*abs(u(tplot,end)))]);
% 	ylim([0,mmax*1.1]);
%     yticks(sort([0 K(ix)*B0*Bback K(ix)*B0*(Bpert+Bback) mmax]))
% % 	ytickformat('%.2e');
% 	
   
end