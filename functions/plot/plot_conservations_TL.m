function f = plot_conservations_TL(X,N,P,Fsz)

f=figure('name','Conservations');
	set_latex_plots(groot)
	subplot(411);
		plot(X,N./N(1),'linewidth',1.5);
% 		xlabel('$X$','fontsize',Fsz);
		set(gca,'FontSize',Fsz-1);
		ylabel('$\frac{N}{N_0}$','fontsize',Fsz);
		xlim([0,X(end)]);
% 		 hYLabel = get(gca,'YLabel');
% 	 set(hYLabel,'rotation',0,'VerticalAlignment','middle')
	subplot(412);
		plot(X,P./P(1),'linewidth',1.5);
		set(gca,'FontSize',Fsz-1);
% 		xlabel('X','fontsize',Fsz);
	xlim([0,X(end)]);
		ylabel('$\frac{P}{P_0}$','fontsize',Fsz);
% 		 hYLabel = get(gca,'YLabel');
% 		set(hYLabel,'rotation',0,'VerticalAlignment','middle')
	subplot(413);
		plot(X(2:end),diff(real(P./N')),'linewidth',1.5);
		set(gca,'FontSize',Fsz-1);
% 		xlabel('$X$','fontsize',Fsz);
		ylabel('$\frac{d(P/N)}{dx}$','fontsize',Fsz);
		xlim([0,X(end)]);
		% suptitle('Conservation of momenta');
% 		 hYLabel = get(gca,'YLabel');
% 		 set(hYLabel,'rotation',0,'VerticalAlignment','middle')

	subplot(414);
		plot(X,(real(P./N')),'linewidth',1.5);
		set(gca,'FontSize',Fsz-1);
		xlabel('$X$','fontsize',Fsz);
		ylabel('$(P/N)$','fontsize',Fsz);
		xlim([0,X(end)]);
end