function f=plot_contrast_TL(u,X,Fsz)


% 	Bkgnd=B0*Bback;
	max_env=max(abs(u));
	Lu=length(u);
% 	ix=int16(Lu/2);
	max_env=max(abs(u(1:end,:)));
	min_env=min(abs(u(1:end,:)));
% 	min_env_l=min(abs(u(ix:end,:)));

	contrast=1-min_env./max_env;
% 	contrast_r=1-min_env_r/Bkgnd;
	
	f = figure();
	set_latex_plots(groot)
% 	subplot(121)
	plot(X,contrast,'linewidth',2)
	title('$\tau$','interpreter','latex','fontsize',Fsz)
	xlabel('$x$ [m]','Interpreter','latex','fontsize',Fsz)    
	ylabel('Contrast','Interpreter','latex','fontsize',Fsz)    
	ylim([0,1.05])
	xlim([X(1),X(end)])
	
% 	subplot(122)
% 	plot(X,contrast_r,'linewidth',2)
% 	title('$\tau >0$','interpreter','latex','fontsize',14)
% 	xlabel('x [m]','Interpreter','latex','fontsize',14)   
% 	ylim([0,1.05])
% 	xlim([X(1),X(end)])
	
end