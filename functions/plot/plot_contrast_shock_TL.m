function f=plot_contrast_shock_TL(B0,Bback,u,X)


	Bkgnd=B0*Bback;
	max_env=max(abs(u));
	Lu=length(u);
	ix=int16(Lu/2);
	min_env_r=min(abs(u(1:ix,:)));
	min_env_l=min(abs(u(ix:end,:)));

	contrast_l=1-min_env_l./Bkgnd;
	contrast_r=1-min_env_r./Bkgnd;
	
	f = figure();
	subplot(121)
	plot(X,contrast_l,'linewidth',2)
	title('$\tau <0$','interpreter','latex','fontsize',14)
	xlabel('x [m]','Interpreter','latex','fontsize',14)    
	ylabel('Contrast','Interpreter','latex','fontsize',14)    
	ylim([0,1.05])
	xlim([X(1),X(end)])
	
	subplot(122)
	plot(X,contrast_r,'linewidth',2)
	title('$\tau >0$','interpreter','latex','fontsize',14)
	xlabel('x [m]','Interpreter','latex','fontsize',14)   
	ylim([0,1.05])
	xlim([X(1),X(end)])
	
end