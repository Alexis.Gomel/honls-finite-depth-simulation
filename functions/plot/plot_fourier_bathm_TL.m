function f = plot_fourier_bathm_TL(K,H,Omega,omega0,uspectrum,omegaxis,X,B0,Lx,xvarstart,xvarstop,UdBth,cmap)

	crit= 1.363;
	g0=9.81;

    f=figure('name','Fourier','position',[20 20 500 500]);
    [ d, ix ] = min( abs( K.*H - crit ) );%find the critical value index
	set_latex_plots(groot)

    subplot(4,1,[1 3]);
    hold on
%     UdBth = -15;
    Usp = 10*log10(abs(uspectrum.')*sqrt(g0)*(1/(2*pi))/B0);
	Usp = Usp-max(Usp,[],"all");
    Usp = (Usp>UdBth).*(Usp-UdBth) +  UdBth;
	s2=pcolor(X,omegaxis*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi),Usp' );
% 	caxis([2*mean(mean((abs_env_part(edge_plot_time:end-edge_plot_time,1:end))))-0.9*max(max(abs_env_part(edge_plot_time:end-edge_plot_time,1:end)))   max(max(abs_env_part(edge_plot_time:end-edge_plot_time,1:end))) ]);
	set(s2, 'EdgeColor', 'black','LineWidth',1.,'MeshStyle','None');
	
	s2.FaceColor = 'interp';
%     imagesc(X,omegaxis*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi),Usp');
    set(gca,'XDir','normal','FontSize',14);
	xlim([min(X),max(X)]);
    ylabel('$f$ [Hz]','Interpreter','latex','fontsize',13);
    xlabel('$x$ [m]','Interpreter','latex','fontsize',13);
    if K(ix)*H(ix)<(crit+0.02) & K(ix)*H(ix)>(crit-0.02);
        plot(omegaxis*sqrt(g0)/(2*pi),X(ix).*ones(max(size(omegaxis*sqrt(g0)/(2*pi)))),'r','LineStyle',':')
    end
    axis([min(X) max(X) -4.5*omega0*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi) 4.5*omega0*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi)  ]  )
    hold off;
	shading interp;
	colormap(cmap)
    hc=colorbar;
   	ylim([0.4*Omega*sqrt(g0)/(2*pi),2.5*Omega*sqrt(g0)/(2*pi)]);
	ylim([-4.5*omega0*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi),4.5*omega0*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi)]);

	title(hc,'|dB|');
   
	ax1 = gca;
	xt = xticks;
	boxlim=0.75;
    ax1_pos = ax1.Position ;
	ax1_pos = [ ax1_pos(1)   ax1_pos(2) boxlim  ax1_pos(4)] 
	set(ax1,'Position',ax1_pos);
	set(hc,'Units','normalized');
	set(hc,'Position', [ boxlim+0.15  ax1_pos(2)  0.03 ax1_pos(4) ]);
	
    subplot(4,1,4);
    ax2 = gca;
    ax2_pos = ax2.Position;
    ax2_pos =  [ ax2_pos(1)  .75*ax2_pos(2)  boxlim  .7*ax2_pos(4)];
    set(ax2,'Position',ax2_pos);
    yline=H.*K;
    yline(end) = NaN;
    c = yline;
    patch(X,yline,c,'EdgeColor','interp','LineWidth',1.2,'MarkerFaceColor','flat');  
    set(gca,'FontSize',15,'Ylim',[0.85*min(H.*K), 1.15*max(H.*K)],'ytick',[min(H.*K)  max(H.*K)*1.0000001],'XLim',[0. Lx],'xtick',[xvarstart,xvarstop]);
    ax2.XAxis.FontSize = 8;
    ylabel('hk ','Interpreter','latex','fontsize',15);
    ytickformat('%.1f');
    xtickformat('%.1f');
    linkaxes([ax1, ax2], 'x');
    yyaxis(ax2, 'right');
    plot(X,-H,'--');
    y2fontsize=12;
    ylabel('h [m]','Interpreter','latex','fontsize',y2fontsize);
    try
    ax2.YAxis(2).TickValues=sort([min(-H),max(-H)]);
    ax2.YAxis(2).FontSize = y2fontsize;
    ax2.YAxis(2).Exponent=-2;
    end
    ytickformat('%.1f');
end
	