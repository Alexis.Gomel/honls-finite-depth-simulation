function f = plot_fourier_first_last_TL(K,H,Omega,omega0,u,omegaxis,X,B0,Lx,xvarstart,xvarstop,UdBth,cmap)

	g0=9.81;
	Fsz=15; %Fontsize
	s_color=[48,117,222]./255; %color at the start
    e_color=[223,78,192]./255; %color at the end
	
	spectrum= fftshift(ifft(u,[],1),1);

	hold on
	max_spec=max(abs(spectrum(:,1)));
	[ d, ix ] = min( abs( abs(spectrum(:,1)) - max_spec ) );%find the critical value index

	spectrum(ix,:)=0.5*spectrum(ix-1,:)+0.5*spectrum(ix+1,:);

	f=figure('name','Fourier first last','position',[20 20 500 500]);
	set_latex_plots(groot)

    subplot(4,1,[1 3]);
    hold on
%     UdBth = -15;
    Usp = 10*log10(abs(spectrum.')*sqrt(g0)*(1/(2*pi))/B0);
	Usp = Usp-max(Usp,[],"all");
    Usp = (Usp>UdBth).*(Usp-UdBth) +  UdBth;
	
	s2=pcolor(omegaxis*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi),X,Usp );
	set(s2, 'EdgeColor', 'black','LineWidth',1.,'MeshStyle','None');
	s2.FaceColor = 'interp';
    set(gca,'XDir','normal','FontSize',Fsz-1);

	ylim([min(X),max(X)]);
    xlabel('$f$ [Hz]','Interpreter','latex','fontsize',Fsz);
    ylabel('$x$ [m]','Interpreter','latex','fontsize',Fsz);
%     if K(ix)*H(ix)<(crit+0.02) & K(ix)*H(ix)>(crit-0.02);
%         plot(omegaxis*sqrt(g0)/(2*pi),X(ix).*ones(max(size(omegaxis*sqrt(g0)/(2*pi)))),'r','LineStyle',':')
%     end
   
% axis([-4.5*omega0*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi) 4.5*omega0*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi) min(X) max(X)   ]  )
    hold off;
	shading interp;
	colormap(cmap)
    hc=colorbar;
	
	for j=1:length(omegaxis)
		if Usp(end,j)>UdBth
			omega_min=j;
			break
		end
	end
	
	for j=1:length(omegaxis)
		if Usp(end,end-j)>UdBth
			omega_max=length(omegaxis)-j;
			break
		end
	end
	plotlim=[omegaxis(omega_min)*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi),omegaxis(omega_max)*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi)];
	xlim(plotlim);

% 	xlim([0.4*Omega*sqrt(g0)/(2*pi),2.5*Omega*sqrt(g0)/(2*pi)]);

	title(hc,'$|dB|$','Interpreter','latex','fontsize',Fsz-1);
    ax1 = gca;
% 	xlim([tp(1),tp(end)])
	xt = xticks;

	boxlim=0.75;
	
    ax1_pos = ax1.Position ;
	ax1_pos = [ ax1_pos(1)   ax1_pos(2) boxlim  ax1_pos(4)] 
	set(ax1,'Position',ax1_pos);
	
	set(hc,'Units','normalized');
	set(hc,'Position', [ boxlim+0.15  ax1_pos(2)  0.03 ax1_pos(4) ]);
    subplot(4,1,4);
    ax2 = gca;
    ax2_pos = ax2.Position;
    ax2_pos =  [ ax2_pos(1)  .75*ax2_pos(2)  boxlim  .7*ax2_pos(4)];
    set(ax2,'Position',ax2_pos);
	
	X_set=1;	
	[ d, ix ] = min( abs( X - X_set ) );%find the critical value index

	hold on
	plot(omegaxis*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi),Usp(ix,:),'-','color',s_color,'LineWidth',1.8,'Displayname',sprintf('%.2f m',X(ix)));
	plot(omegaxis*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi),Usp(end,:),'-','color',e_color,'LineWidth',1.5,'Displayname',sprintf('%.2f m',X(end)));
	hold off
	legend()
	xlim(plotlim);
% 	xlim([0.4*Omega*sqrt(g0)/(2*pi),2.5*Omega*sqrt(g0)/(2*pi)]);

% 	xlim([-4.5*omega0*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi) 4.5*omega0*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi)]);
	ylabel('$|dB|$ ','Interpreter','latex','fontsize',Fsz);
    xlabel('$f$ [Hz]','Interpreter','latex','fontsize',Fsz);

%     yline=H.*K;
%     yline(end) = NaN;
%     c = yline;
%     patch(X,yline,c,'EdgeColor','interp','LineWidth',1.2,'MarkerFaceColor','flat');  
%     set(gca,'FontSize',15,'Ylim',[0.85*min(H.*K), 1.15*max(H.*K)],'ytick',[min(H.*K)  max(H.*K)*1.0000001],'XLim',[0. Lx],'xtick',[xvarstart,xvarstop]);
%     ax2.XAxis.FontSize = 8;
%     ylabel('hk ','Interpreter','latex','fontsize',15);
%     ytickformat('%.1f');
%     xtickformat('%.1f');
%     linkaxes([ax1, ax2], 'x');
%     yyaxis(ax2, 'right');
%     plot(X,-H,'--');
%     y2fontsize=12;
%     ylabel('h [m]','Interpreter','latex','fontsize',y2fontsize);
%     try
%     ax2.YAxis(2).TickValues=sort([min(-H),max(-H)]);
%     ax2.YAxis(2).FontSize = y2fontsize;
%     ax2.YAxis(2).Exponent=-2;
%     end
%     ytickformat('%.1f');
end
