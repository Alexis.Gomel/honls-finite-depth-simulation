function f = plot_fourier_solitonN4_TL(K,H,Omega,omega0,uspectrum,omegaxis,X,B0,Lx,xvarstart,xvarstop,cmap)

	crit= 1.363;
	g0=9.81;

    f=figure('name','Fourier N4','position',[20 20 900 450]);

    pos1 = [0.05 0.3 0.9 0.65];
    UdBth = -30;
    Usp = 10*log10(abs(uspectrum.')*sqrt(g0)*(1/(2*pi))/B0);
	Usp = Usp-max(Usp,[],"all");
    Usp = (Usp>UdBth).*(Usp-UdBth) +  UdBth;


	
	X_set=1;	
	[ d, ix ] = min( abs( X - X_set ) );%find the critical value index

	hold on
	plot(omegaxis*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi),Usp(ix,:),'-r','LineWidth',1.5,'Displayname',sprintf('%.2f m',X(ix)));
	plot(omegaxis*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi),Usp(end,:),'-b','LineWidth',1.5,'Displayname',sprintf('%.2f m',X(end)));
	hold off
	legend()
   	xlim([2,5]);
	ylabel('Spectrum $|dB|$ ','Interpreter','latex','fontsize',13);
    xlabel('Frequency [Hz]','Interpreter','latex','fontsize',13);

end
