function f=plot_params_TL(X,K,H,Mu,Cp,Cg,Muintexp,Regime,Alpha3,Alpha4,Beta_21,Beta_22,Fsz)

ratio=20/9; r_fig=400; f=figure('position',[20 50 ratio*r_fig r_fig]);
	set_latex_plots(groot)
	subplot(221);
		yyaxis left
		plot(X,H,'LineWidth',1.5);
		ylabel('$h$[m]','FontSize',Fsz)
		yyaxis right
		plot(X,Mu,'LineWidth',1.5);
		ylabel('$\mu$[m]','FontSize',Fsz)
% 		legend({'$h$[m]','$\mu$[$m^{-1}$]'},'FontSize',Fsz-1)
		xlabel('$X$[m]','FontSize',Fsz); set(gca,'Fontsize',Fsz-1);
	subplot(222);
		yyaxis left
		plot(X,Cp./Cg,'LineWidth',1.5,'DisplayName','$c_p/c_g$');
		ylim([0.9.*min(Cp./Cg),1.1*max(Cp./Cg)])
		ylabel('$c_p/c_g$','FontSize',Fsz)
		set(gca,'Fontsize',Fsz-1);
		yyaxis right
		plot(X(2:end),Muintexp(2:end)./Muintexp(1:end-1),'LineWidth',1.5,'DisplayName','$e^{\int \mu}$');
		ylim([0.95.*min(Muintexp(2:end)./Muintexp(1:end-1)),1.01*max(Muintexp(2:end)./Muintexp(1:end-1))])
		ylabel('$e^{\int \mu}$','FontSize',Fsz)
		xlabel('$X$[m]','FontSize',Fsz); set(gca,'Fontsize',Fsz-1);
	subplot(223);
		yyaxis left
		plot(X,Regime,'LineWidth',1.5);
		ylabel('$\beta/ \alpha$','FontSize',Fsz)
% 		legend({'$\beta/ \alpha$'},'FontSize',Fsz-1)
		yyaxis right
		plot(X,K,'LineWidth',1.5,'DisplayName','$k$[$m^{-1}$]');
		ylabel('$k$ [$m^{-1}$]','FontSize',Fsz)
		xlabel('$X$[m]','FontSize',Fsz); set(gca,'Fontsize',Fsz-1);
	subplot(224);
		plot(X,Alpha3,X,Alpha4,X,Beta_21,X,Beta_22,'LineWidth',1.5);
		legend({'$\alpha_3$ (TOD)','$\alpha_4$ (FOT)','$\beta_{21}$','$\beta_{22}$'},'FontSize',Fsz-1)
		xlabel('$X$[m]','FontSize',Fsz); set(gca,'Fontsize',Fsz-1);

	axesHandles = get(gcf,'children');
	axesHandles = findall(0,'type','axes');
	% set(axesHandles,xlim,[0,Lx])
% 	axis(axesHandles,'tight')


end