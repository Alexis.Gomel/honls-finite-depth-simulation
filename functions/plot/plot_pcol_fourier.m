function [fig_four_pcolor]=plot_pcol_fourier(timeseries,X,dt,carrier_frequency,Tnl,log_plot,window)
g=9.81;
time = linspace(0,length(timeseries).*dt,length(timeseries));
[ freqs,modes,fmeans] = mean_w0_test(timeseries,carrier_frequency,time,0,1);
Tmod=1/abs(freqs(1)-freqs(2));
Fmod=abs(freqs(1)-freqs(2));
[omegaxis,fou]=Nice_fourier(timeseries,time,0,window);
fig_four_pcolor=figure();
if log_plot==1 
s=pcolor(X,omegaxis,log(abs(fou)./max(abs(fou(:,1))) ) );    %maybe don't normalize the log 
else
s=pcolor(X,omegaxis,abs(fou)./max(abs(fou(:,1)))  );  
end
set(s, 'EdgeColor', 'black','LineWidth',1.,'MeshStyle','column');
s.FaceColor = 'interp';
plot_thing=gca;
Omega_max_r=carrier_frequency+sqrt(2*g)./Tnl./(2*pi);
Omega_min_r=carrier_frequency+2*sqrt(g)./Tnl./(2*pi);
Omega_max_l=carrier_frequency-sqrt(2*g)./Tnl./(2*pi);
Omega_min_l=carrier_frequency-2*sqrt(g)./Tnl./(2*pi);
hold on
plot(X,Omega_max_r,'--w','linewidth',0.9);
plot(X,Omega_max_l,'--w','linewidth',0.9);
plot(X,Omega_min_r,'--y','linewidth',0.7);
plot(X,Omega_min_l,'--y','linewidth',0.7);
% plot(pos_wg,freqs(2)*ones(length(pos_wg)),'--g','linewidth',0.9);
% plot(pos_wg,freqs(3)*ones(length(pos_wg)),'--g','linewidth',0.9);
str = sprintf('$ \\frac{\\Omega_0}{\\Omega_{max}} =$ %.3f' ,1./Tmod./(Omega_max_r(1)-carrier_frequency));
txt=text(X(end)*3/4,freqs(1)+2*Fmod,str,'interpreter','latex','color','green','FontSize',13);
uistack(txt, 'top')
str = sprintf('$ \\frac{\\Omega_f}{\\Omega_{max}} =$ %.3f' ,1./Tmod./(Omega_max_r(end)-carrier_frequency));
txt=text(X(end)*3/4,freqs(1)+3*Fmod,str,'interpreter','latex','color','green','FontSize',13);
uistack(txt, 'top')
hold off
ylim([freqs(1)-4*Fmod,freqs(1)+4*Fmod]);
xlim([0,X(end)]);
% caxis([0. inf]);
xlabel('Distance to wavemaker ($m$)','interpreter','Latex');
ylabel('$Hz$','interpreter','Latex');
try
colormap(inferno);
end
colorbar;

% [d_5, ix_5 ] = min( abs( 5 - omegaxis ) );
% fig_four_pcolor=figure();
% if log_plot==1 
% s=pcolor(X,omegaxis(1:ix_5),log(abs(fou(1:ix_5,:))./max(abs(fou(1:ix_5,1))) ) );     
% else
% s=pcolor(X,omegaxis(1:ix_5),abs(fou(1:ix_5,:))./max(abs(fou(1:ix_5,1)))  );  
% end
