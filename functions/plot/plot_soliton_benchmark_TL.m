function f=plot_soliton_benchmark_TL(K,H,Omega,omega0,uspectrum,omegaxis,X,B0,Lx,xvarstart,xvarstop,cmap)

	crit= 1.363;
	g0=9.81;

    f=figure('name','Fourier','position',[20 20 500 500]);
    [ d, ix ] = min( abs( K.*H - crit ) );%find the critical value index

    pos1 = [0.05 0.5 0.9 0.3];
%     subplot(4,1,[1 3]);
    hold on
    UdBth = -15;
    Usp = 10*log10(abs(uspectrum.')*sqrt(g0)*(1/(2*pi))/B0);
	Usp = Usp-max(Usp,[],"all");
    Usp = (Usp>UdBth).*(Usp-UdBth) +  UdBth;
% 	s2=pcolor(X,omegaxis*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi),Usp' );
% 	caxis([2*mean(mean((abs_env_part(edge_plot_time:end-edge_plot_time,1:end))))-0.9*max(max(abs_env_part(edge_plot_time:end-edge_plot_time,1:end)))   max(max(abs_env_part(edge_plot_time:end-edge_plot_time,1:end))) ]);
	s2=pcolor(omegaxis*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi),X,Usp );

	set(s2, 'EdgeColor', 'black','LineWidth',1.,'MeshStyle','None');
	s2.FaceColor = 'interp';
%     imagesc(X,omegaxis*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi),Usp');
    set(gca,'XDir','normal','FontSize',14);
%     xlim([min(X),max(X)]);
    xlabel('Frequency (Hz)','Interpreter','latex','fontsize',13);
    ylabel('Distance (m)','Interpreter','latex','fontsize',13);
%     if K(ix)*H(ix)<(crit+0.02) & K(ix)*H(ix)>(crit-0.02);
%         plot(omegaxis*sqrt(g0)/(2*pi),X(ix).*ones(max(size(omegaxis*sqrt(g0)/(2*pi)))),'r','LineStyle',':')
%     end
%     axis([min(X) max(X) -4.5*omega0*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi) 4.5*omega0*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi)  ]  )
    hold off;
	shading interp;
	colormap(cmap)
    hc=colorbar;
%    	ylim([0.4*Omega*sqrt(g0)/(2*pi),2.5*Omega*sqrt(g0)/(2*pi)]);
	title(hc,'|dB|');
    ax1 = gca;
    ax1_pos = ax1.Position ;
	ylim([3.5,10]);
	xlim([1,3.5]);

end