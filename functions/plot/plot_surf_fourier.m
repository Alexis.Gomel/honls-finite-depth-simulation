function [fig_four_surf]=plot_surf_fourier(timeseries,X,dt,log_plot)
fou=fftshift(ifft(timeseries,[],1),1);
L=length(fou(:,1));
omegaxis = linspace(-1/dt/2,1/dt/2,L+1); 
omegaxis = omegaxis(1:end-1);                   %Frequency axis, in real frequency units.
[d_5, ix_5 ] = min( abs( 5 - omegaxis ) );
fig_four_surf=figure();
s=surf(X,omegaxis(L/2+1:ix_5),abs(fou(L/2+1:ix_5,:))./max(abs(fou(L/2+1:ix_5,1)))  ),  
set(s, 'EdgeColor', 'black','LineWidth',1.,'MeshStyle','column');
s.FaceColor = 'interp';
plot_thing=gca;
ylim([0,5]);
caxis([0. inf]);
if log_plot==1 
set(plot_thing,'zscale','log')
zlim([0.001,1]);
end
try
    colormap(Inferno)
end
xlabel('Distance to wavemaker ($m$)','interpreter','Latex');
ylabel('$Hz$','interpreter','Latex');
colorbar;