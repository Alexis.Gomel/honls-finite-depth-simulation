function f=plot_tank_scheme_TL(X,H,xvarstart,xvarstop,Exportinit,pos_gau,Lx,Fsz)

	f=figure('name','bathymetry','position',[20 20 600 200]);
	hold on
		ax=gca;
		plot(X,-H);
		plot(X,-1*ones(size(X)));    
		plot(X,-H(1)*ones(size(X)),':k');
		plot(X,0*ones(size(X)),'b');
		ang=atan((-H(length(H)-1)+H(1))/(xvarstop-xvarstart));
		angles=linspace(0,ang,25);
		amp=(xvarstop-xvarstart)*0.5;
		if max(X)>30
		   plot([30 30],[0 -1],':k')
		   if Exportinit=='yes'
			   for j=1:length(pos_gau)
			   plot([pos_gau(j) pos_gau(j)],[0 -min(H)],'-.k')
			   end
			end
		end
		plot(amp*cos(angles)+xvarstart,amp*sin(angles)-H(1),'r' )
		hold off
		try
		if max(X)>30
		   set(ax,'XLim',[0. Lx],'xtick',sort([0,xvarstart,xvarstop,30,Lx]) ) ;
		else
			set(ax,'XLim',[0. Lx],'xtick',sort([0,xvarstart,xvarstop,Lx]) ) ;
		end
		end
		ylabel('$h$ [m]','fontsize',Fsz);
		xlabel('$x$ [m]','fontsize',Fsz);
		angle=num2str(rad2deg(ang));
		legend({strcat('angle= ',angle) },'Location','southwest') ;
		try
		ax.YAxis.TickValues=sort([-1,min(-H),max(-H)]);
		end
		ax.YAxis.Exponent=-2;
		
	end