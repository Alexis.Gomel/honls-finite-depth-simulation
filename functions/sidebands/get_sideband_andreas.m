function utrunc=get_sideband_andreas(omegaxis,omega0,uspectrum,nt)

	IAS = find(omegaxis<=omega0+.1 & omegaxis>=omega0-.1); IAS = IAS(round(length(IAS)/2));
	IS = find(omegaxis<=-omega0+.1 & omegaxis>=-omega0-.1); IS = IS(round(length(IS)/2));
	IAS2 = find(omegaxis<=2*omega0+.1 & omegaxis>=2*omega0-.1); IAS2 = IAS2(round(length(IAS2)/2));
	IS2 = find(omegaxis<=-2*omega0+.1 & omegaxis>=-2*omega0-.1); IS2 = IS2(round(length(IS2)/2));
	IAS3 = find(omegaxis<=3*omega0+.1 & omegaxis>=3*omega0-.1); IAS3 = IAS3(round(length(IAS2)/2));
	IS3 = find(omegaxis<=-3*omega0+.1 & omegaxis>=-3*omega0-.1); IS3 = IS3(round(length(IS2)/2));


	utrunc = uspectrum([nt/2+1,IS,IAS,IS2,IAS2,IS3,IAS3],:);
	
end