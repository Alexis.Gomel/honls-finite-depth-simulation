function [ModesAmplitudes]=getsidebands_static_theoretical(data_series,central_frequency,var,Debug)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This code searches for the first and second sidebands of a data series. (array)
% Inputs: 
%         - data is the data array for the data_series.
%         - data series array, is the array to extract the sideband
%         information from
%         - Aproximate central frequency
%         - Debug option: 1 to print debug plot, 0 if not.
%
% Outputs:
%         - ModesAmplitudes: Is an array with the complex value of the fft
%         main modes, up to the 2nd sidebands
%         - ModesPhases: Is an array with the Phases of the fft
%         main modes, up to the 2nd sidebands
%
% A.Gomel 15/01/2020
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dsl=length(data_series);

foudata=fftshift(fft(data_series,[],1),1)./dsl;
hvar=var(2)-var(1);
new_omegaxis=2*pi.*linspace(-1/hvar/2,1/hvar/2,dsl);


	if Debug==1
		figure()
		subplot(111)
		plot(new_omegaxis,abs(foudata(:,1)))
	end
	
	[psor,lsor] = findpeaks(abs(foudata(:,1)),'SortStr','descend');%sorts the data
	ind1=lsor(1);
	ind2=lsor(2);
	ind3=lsor(3);
	max1=psor(1);
	max2=psor(2);
	max3=psor(3);

	if max1>=max2  %%%% this is to refer to the highest first sideband, just in case.
		ind_dist=abs(ind2-ind1);
		fmod= abs(central_frequency-new_omegaxis(ind2)); %modulation frequency

	elseif max1<max2
		ind_dist=abs(ind3-ind1);
		fmod= abs(central_frequency-new_omegaxis(ind3)); %modulation frequency
	end

	Tmod=1./fmod;  %aproximate modulation period
	if Debug==1
		figure()
			subplot(311)
			plot(new_omegaxis,real(foudata))
			hold on
			plot(new_omegaxis(ind1),max1,'bx')
			plot(new_omegaxis(ind2),max2,'bx')
			plot(new_omegaxis(ind3),max3,'bx')
			hold off
			xlim([0,4]);
			subplot(312)
			plot(new_omegaxis,abs(foudata))
			hold on
			plot(new_omegaxis(ind1),abs(foudata(ind1)),'gx')
			plot(new_omegaxis(ind1+ind_dist),abs(foudata(ind1+ind_dist)),'gx')
			plot(new_omegaxis(ind1-ind_dist),abs(foudata(ind1-ind_dist)),'gx')
			plot(new_omegaxis(ind1+2*ind_dist),abs(foudata(ind1+2*ind_dist)),'gx')
			plot(new_omegaxis(ind1-2*ind_dist),abs(foudata(ind1-2*ind_dist)),'gx')
			hold off
			xlim([0,4]);
			subplot(313)
			plot(new_omegaxis,mod(unwrap(angle(foudata)),2*pi))    
			hold on
			plot(new_omegaxis(ind1),mod(unwrap(angle(foudata(ind1))),2*pi),'gx')    
			plot(new_omegaxis(ind1+ind_dist),mod(unwrap(angle(foudata(ind1+ind_dist))),2*pi),'gx')    
			plot(new_omegaxis(ind1-ind_dist),mod(unwrap(angle(foudata(ind1-ind_dist))),2*pi),'gx')    
			hold off
			xlim([0,4]);
	end 

	L=length(foudata);                                          %data series lenght
	finalfoudata=foudata;   %/max(foudata(L/2+1:end));
	ModesAmplitudes=zeros(5,length(data_series(1,:)));                                %empty output matrix

	%%%%%%%%%%%% set ourput array to  a matrix %%%%%%%%%%
	ModesAmplitudes(1,:) = finalfoudata(ind1,:); %central mode
	ModesAmplitudes(2,:) = finalfoudata(ind1+ind_dist,:);   %right first sideband
	ModesAmplitudes(3,:) = finalfoudata(ind1-ind_dist,:);   %left first sideband
	ModesAmplitudes(4,:) = finalfoudata(ind1+2*ind_dist,:); %right second sideband
	ModesAmplitudes(5,:) = finalfoudata(ind1-2*ind_dist,:); %left second sideband