function print_input_file2(filename,Input_function,input_freq,initial_depth,wm_factor,M)
	fileID = fopen(filename,'w');

	format shortg;
	datetime = clock;
	fprintf(fileID,'#EDL\tDate\t%4d.%2d.%2d\tLocal Time\t%2d:%2d:%.3f\tDN_001\n',datetime);
	fprintf(fileID,'Rate(Hz)\tinput_function\tinitial_depth_in_m\twm_factor\n');

	fprintf(fileID,'%.1f\t%',input_freq);
	fprintf(fileID,Input_function);
	fprintf(fileID,'\t%.2f\t%',initial_depth);
	fprintf(fileID,'%.2f\n%',wm_factor);

	fprintf(fileID,'Time\tInput_values\n');
	fprintf(fileID,'s\tm\n');

	% #EDL	Date	17.10.2018	Local Time	13:46:24	DN_001	
	% Rate (Hz)	paddles			Main array	
	fprintf(fileID,'%5d\t%5d\n',M');
	fclose(fileID);
end
