function print_params_file(filename,Input_function,input_freq, save_params,save_params_names);%TO BE USED TO SAVE PARAMETERS TO FILE. 

	fileID = fopen(filename,'w');

	format shortg;
	datetime = clock;
	fprintf(fileID,'#EDL\tDate\t%4d.%2d.%2d\tLocal Time\t%2d:%2d:%.3f\tparams\n',datetime);
	fprintf(fileID,'Rate(Hz)\tinput_function\tinput_frequency\n');
	fprintf(fileID,'%.1f\t%\n',input_freq);
	fprintf(fileID,'\n')

	for j=1:length(save_params_names)
		fprintf(fileID,'%s\t',string(save_params_names(j)));
	end
	fprintf(fileID,'\n')
	for j=1:length(save_params)
		fprintf(fileID,'%s\t',string(save_params(j)));
	end
	fprintf(fileID,'\n')
	fclose(fileID)
end


